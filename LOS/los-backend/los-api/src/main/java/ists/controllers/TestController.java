package ists.controllers;

import ists.models.viewModels.ProductAudViewModel;
import ists.utility.SequenceUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 10/16/2014.
 */
@RestController
public class TestController {

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public String test(@RequestBody ProductAudViewModel productAudEntity) {
        System.out.println("productname: " + productAudEntity.getProductName());
        System.out.println("parent product name: " + productAudEntity.getProductByParentId().getProductName());
        if (productAudEntity == null) {
            System.out.println("Null 1");
            return "Null 1";
        }
//        if (productPropsAudEntityList == null) {System.out.println("Null 2"); return "Null 2";}
//        else if (productPropsAudEntityList.length < 1) {System.out.println("Zero"); return "Zero";}
        return "K co loi";
    }

    @RequestMapping(value = "/exception", method = RequestMethod.GET)
    public Exception exception() throws Exception {
        throw new Exception("Test loi");
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public Long test() {
        return SequenceUtil.getNextPId();
    }
}
