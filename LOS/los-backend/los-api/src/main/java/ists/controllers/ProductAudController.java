package ists.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import ists.models.viewModels.ProductAudLite;
import ists.utility.Common;
import ists.utility.EnumUtil;
import ists.utility.ObjectUtil;
import ists.models.*;
import ists.models.viewModels.ProductLite;
import ists.utility.HibernateUtil;
import ists.models.viewModels.ProductAudViewModel;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Administrator on 10/3/2014.
 */
@RestController
public class ProductAudController {

    /**
     * USE FOR TEST (Get all product with no right)
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public List<ProductLite> getProductsTree() throws Exception {
        Session session = null;
        List<ProductLite> listProductLite = new ArrayList();
        try {
//            Integer.parseInt(productId);
            session = HibernateUtil.getSessionFactory().openSession();
            List<ProductEntity> list = session.createQuery("from ProductEntity").list();
            for (ProductEntity p : list) {
                listProductLite.add(new ProductLite(p));
            }
            return listProductLite;
        } catch (Exception e) {
            System.out.println("=====> Loi: ---- " + e.toString());
            throw e;
        }
    }

    /**
     * Get main properties of product
     * @param productId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/product/{productId}", method = RequestMethod.GET)
    public ProductEntity getProduct(@PathVariable String productId) throws Exception {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from ProductEntity where productId=:id");
            query = query.setString("id",productId);
            List<ProductEntity> list = query.list();
            System.out.println("blah balh");
            if (list.size() < 1){
                System.out.println("Loi null");
                throw new Exception("Khong ton tai productId");
            }

//            System.out.println("Ten cha: " + list.get(0).getProductByParentId().getProductId());
            System.out.println("Ten cha: " + list.get(0).getParentId());

//            ObjectMapper mapper = new ObjectMapper();

//            Hibernate4Module hbm = new Hibernate4Module();
//            hbm.configure(Hibernate4Module.Feature.FORCE_LAZY_LOADING, true);
//            mapper.registerModule(hbm);
//            ObjectWriter w = mapper.writer();
//            String result = w.writeValueAsString(list.get(0));
//            System.out.println(result);
//
//            return result;

            return list.get(0);
        } catch (Exception e) {
            System.out.println("=====> Loi: ---- " + e.toString());
            throw e;
        }
    }

    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public boolean saveProductAud(ProductAudEntity productAudEntity) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            //TODO: Kiem tra productName bi trung
//            List products = session.createQuery("from ProductEntity where productId = " + productEntity.getProductId()).list();
//            if (products.size() > 0) {
//                throw new HibernateException("Da ton tai Product_Name");
//            }

//            private String productId;
//            private String productName;
//            private String productSlogan;
//            private String parentId;
//            private Long productOwner;
//            private String allowSub;
//            private String allowCont;
//            private String inheritCondition;
//            private String inheritDocument;
//            private String linkCbs;
//            private Timestamp effective;
//            private Timestamp expiry;
//            private Long pid;
//            private Long inheritMenuAcl;
//            private Long inheritParentAcl;
//            private Long lastSysTranId;
//            private Long updSeq;


//            ProductAudEntity productAudEntity = new ProductAudEntity();

//            productAudEntity.setProductAudId(null);
            //TODO: Thay the gia tri SysTranId
            productAudEntity.setSysTranId(null);
            productAudEntity.setAuditDate(new Timestamp(System.currentTimeMillis()));

            ObjectUtil<ProductAudEntity> objectUtil = new ObjectUtil<ProductAudEntity>();//Xac dinh cac truong bi thay doi, all
            productAudEntity.setAuditFields(objectUtil.getAllAnnotationNameString(productAudEntity));

            productAudEntity.setAuditOperation(EnumUtil.AuditOperation.INSERT.toString());
//            productAudEntity.setAuditOperation("I");
            productAudEntity.setApprovalStatus(EnumUtil.ApprovalStatus.INPUT.toString());
//            productAudEntity.setApprovalStatus("I");
            //TODO: Sua lai gia tri mac dinh
            productAudEntity.setPrevSysTranId(new Long(0));
            productAudEntity.setUpdSeq(new Long(0));

//            productAudEntity.setProductId(productAudEntity.getProductId());
//            productAudEntity.setProductName(productAudEntity.getProductName());
//            productAudEntity.setProductSlogan(productAudEntity.getProductSlogan());
            //TODO: kiem tra Parent Id
//            productAudEntity.setParentId(productAudEntity.getParentId());
            //TODO: kiem tra Product Owner
//            productAudEntity.setProductOwner(productAudEntity.getProductOwner());
//            productAudEntity.setAllowSub(productAudEntity.getAllowSub());
//            productAudEntity.setAllowCont(productAudEntity.getAllowCont());
//            productAudEntity.setInheritCondition(productAudEntity.getInheritCondition());
//            productAudEntity.setInheritDocument(productAudEntity.getInheritDocument());
//            productAudEntity.setLinkCbs(productAudEntity.getLinkCbs());
//            productAudEntity.setEffective(productAudEntity.getEffective());

            session.save(productAudEntity);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            throw ex;
        }

        return true;
    }

       @RequestMapping(value = "/product/", method = RequestMethod.PUT)
    public String updateProductAud(ProductAudEntity productAudEntity) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            session.beginTransaction();

            ProductEntity productEntity = new ProductEntity();
            SysTransactionEntity sysTransactionEntity = new SysTransactionEntity();

            //ProductAudEntity newProductAudEntity = new ProductAudEntity();
            //SysTransactionEntity sysTransactionEntity = new SysTransactionEntity();

//            productAudEntity.setSysTranId(sysTransactionEntity.getSysTranId());
            productAudEntity.setAuditDate(new Timestamp(System.currentTimeMillis()));
            productAudEntity.setAuditFields("field 1, field 2, ....");
            productAudEntity.setAuditOperation("U");
            if (productAudEntity.getApprovalStatus() == EnumUtil.ApprovalStatus.APPROVED.toString()) {
                productAudEntity.setApprovalStatus(EnumUtil.ApprovalStatus.INPUT.toString());
                productAudEntity.setUpdSeq(productEntity.getUpdSeq() + 1);
            } else {
                productAudEntity.setApprovalStatus(EnumUtil.ApprovalStatus.REVIEW.toString());
            }
//            productAudEntity.setUpdSeq(productAudEntity.getUpdSeq());
//            productAudEntity.setParentId(productEntity.getParentId());
//            productAudEntity.setProductOwner(productEntity.getProductOwner());
//            productAudEntity.setAllowSub(productEntity.getAllowSub());
//            productAudEntity.setAllowCont(productEntity.getAllowCont());
//            productAudEntity.setInheritCondition(productEntity.getInheritCondition());
//            productAudEntity.setInheritDocument(productEntity.getInheritDocument());
//            productAudEntity.setLinkCbs(productEntity.getLinkCbs());
//            productAudEntity.setExpiry(productAudEntity.getExpiry());
//            productAudEntity.setEffective(productAudEntity.getEffective());

            session.update(productAudEntity);
            session.getTransaction().commit();

        } catch (Exception ex) {
            System.out.println(ex.toString());
            throw ex;
        }
        return "OK";
    }


    //===================================================================//
    //===================================================================//
    //===================================================================//

    /**
     * Get detail product audit for edit draft or approve
     * @param productAudId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/productAud/{productAudId}", method = RequestMethod.GET)
    public ProductAudEntity getProductAud(@PathVariable Long productAudId) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            Query query = session.createQuery("from ProductAudEntity where productAudId=:id");
            query.setLong("id",productAudId);
            List<ProductAudEntity> list = query.list();
            if (list.size() < 1){
                System.out.println("Loi null");
                throw new Exception("Khong ton tai productAudId");
            }

            return list.get(0);
        } catch (Exception ex) {
            throw ex;
        }
    }

    /**
     * Get list product audit waiting for approval
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/productAud", method = RequestMethod.GET)
    public List<ProductAudEntity> getProductAud() throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            //TODO: kiem tra thoi gian expiry
//            Query query = session.createQuery("select ProductAudEntity from ProductAudEntity " +
//                    "left join ProductEntity on ProductAudEntity.updSeq = ProductEntity.updSeq " +
//                    "inner join SysTransactionEntity on ProductAudEntity.sysTranId = SysTransactionEntity.sysTranId " +
//                    "where ProductAudEntity.approvalStatus = 'I' and SysTransactionEntity.tranStatus = 'A'");

            Query query = session.createSQLQuery("select pa.PRODUCT_AUD_ID, pa.AUDIT_DATE, pa.PRODUCT_NAME, pa.APPROVAL_STATUS, s.USER_ID from PRODUCT_AUD pa " +
                    "left join PRODUCT p on pa.UPD_SEQ = p.UPD_SEQ " +
                    "inner join SYS_TRANSACTION s on pa.SYS_TRAN_ID = s.SYS_TRAN_ID " +
                    "WHERE pa.APPROVAL_STATUS NOT IN (:a, :j, :e) and s.TRAN_STATUS = :s").addEntity(ProductAudLite.class);
            query.setString("a", EnumUtil.ApprovalStatus.APPROVED.toString());
            query.setString("j", EnumUtil.ApprovalStatus.REJECT.toString());
            query.setString("e", EnumUtil.ApprovalStatus.ERROR.toString());
            query.setString("s", Common.TRAN_STATUS.ACTIVE.toString());
            List<ProductAudEntity> list = query.list();

            System.out.println("========================== " + list.size() + " ============");

            return list;
        } catch (Exception ex) {
            throw ex;
        }
    }


    @RequestMapping(value = "/productAud/Reject", method = RequestMethod.POST)
    public String rejectProductAud(String productAudId) throws Exception {

        return "OK";
    }
}
