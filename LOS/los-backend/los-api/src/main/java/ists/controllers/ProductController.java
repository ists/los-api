package ists.controllers;

/**
 * Created by Trung on 8/25/2014.
 */
import ists.blls.Product;
import ists.blls.Test;
import ists.models.viewModels.ProductDetailRight;
import ists.models.viewModels.ProductLite;
import ists.models.viewModels.ProductRight;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {
    //    private ProductBLL objBll;
//    public ProductController() {
//        objBll = new ProductBLL();
//    }
//    @RequestMapping(value = "/GetTest1/{isAccepted}", method = RequestMethod.GET)
//    public boolean getTest1(@PathVariable int isAccepted) {
//        Product productController = new Product();
//        ProductEntity product = new ProductEntity();
//        product.setProductId("abc");
//        productController.add(product);
//        if (isAccepted == 0)
//            return false;
//        else
//            return true;
//    }
//
//    @RequestMapping(value = "/GetTest2/{isAccepted}", method = RequestMethod.GET)
//    public SysUserEntity getTest2(@PathVariable int isAccepted) {
//        return new Product().get("demo.inputuser1");
//    }

    /**
     * Get tree by user's right
     * @param menuId
     * @param userId
     * @return
     */
    @RequestMapping(value = "/GetProductTreeByUserId/{menuId}/{userId:.+}", method = RequestMethod.GET)
    public List<ProductLite> getProductTreeByUserId(@PathVariable Long menuId, @PathVariable String userId) {
        return new Product().getTreeByUserId(menuId, userId);
    }

    /**
     * Get product right by user for display and hide buttonbar
     * @param menuId
     * @param productId
     * @param userId
     * @return
     */
    @RequestMapping(value = "/GetProductRight/{menuId}/{productId}/{userId:.+}", method = RequestMethod.GET)
    public List<ProductRight> getProductRight(@PathVariable Long menuId, @PathVariable String productId, @PathVariable String userId) {
        return new Product().getProductRight(menuId, productId, userId);
    }

    /**
     * Get list user, group with right of product
     * @param productId
     * @return
     */
    @RequestMapping(value = "/GetProductDetailRight/{productId}", method = RequestMethod.GET)
    public List<ProductDetailRight> getProductDetailRight(@PathVariable String productId) {
        return new Product().getProductDetailRight(productId);
    }

    @RequestMapping(value = "/Test", method = RequestMethod.GET)
    public String Test() {
        return new Test().Test3();
    }

//    @RequestMapping(value = "/bpmnumbertest/{isAccepted}", method = RequestMethod.GET)
//    public int bpmNumberTest(@PathVariable int isAccepted) {
//        if (isAccepted == 0)
//            return 10000;
//        else
//            return 1;
//    }
//    @RequestMapping(value = "/product", method = RequestMethod.GET)
//    public List<DBProduct> productList() {
//        return objBll.getProductList();
//    }

    /*@RequestMapping(value = "/product/{productId}", method = RequestMethod.GET)
     public DBProduct getProduct(@PathVariable String productId) {
     if (productId == null)
     throw new NullPointerException("Product id is null");

     Session session = HibernateUtil.getSessionFactory().openSession();
     DBProduct product = (DBProduct) session.byId(DBProduct.class).load(Integer.valueOf(productId));
     session.close();

     return product;
     }

     @RequestMapping(value = "/product/add", method = RequestMethod.POST)
     public void addProduct(@Valid @RequestBody DBProduct product) {
     Session session = HibernateUtil.getSessionFactory().openSession();
     session.beginTransaction();

     System.out.println("Product code: " + product.getProductCode());
     saveProduct(product, session);
     }

     @RequestMapping(value = "/product/edit", method = RequestMethod.POST)
     public void editProduct(@Valid @RequestBody DBProduct product) {
     Session session = HibernateUtil.getSessionFactory().openSession();
     session.beginTransaction();

     saveProduct(product, session);
     }

     @RequestMapping(value = "/product/{productId}", method = RequestMethod.DELETE)
     public void deleteProduct(@PathVariable String productId) {
     if (productId == null)
     throw new NullPointerException("Product id is null");

     Session session = HibernateUtil.getSessionFactory().openSession();
     session.beginTransaction();
     DBProduct product = (DBProduct) session.byId(DBProduct.class).load(Integer.valueOf(productId));
     session.delete(product);

     session.getTransaction().commit();
     }

     private void saveProduct(DBProduct product, Session session) {
     if (product == null)
     throw new NullPointerException("The product need to be saved is null");

     DBProduct model = new DBProduct();
     if (product.getProductID() > 0) {
     model = (DBProduct) session.byId(DBProduct.class).load(product.getProductID());
     }

     model.setProductName(product.getProductName());
     model.setProductCode(product.getProductCode());
     model.setCreatedDate(new Date());

     session.save(model);
     session.getTransaction().commit();
     }*/
}
