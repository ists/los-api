package ists.controllers;

import ists.blls.BaseClass;
import ists.dao.ProductAudEntityDAO;
import ists.dao.ProductAudEntityDAOimplms;
import ists.dao.ProductEntityDAO;
import ists.dao.ProductEntityDAOimplms;
import ists.models.viewModels.FullProduct;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Chien Leader on 14-10-14.
 */

@RestController
public class ProductAudEntityController_Chien extends BaseClass {

    ProductAudEntityDAO productAudEntityDAO = new ProductAudEntityDAOimplms();

    ProductEntityDAO productEntityDAO = new ProductEntityDAOimplms();

    @RequestMapping(value = "/product/saveProduct", method = RequestMethod.POST)
    public void saveProduct(@RequestBody FullProduct fullProduct) throws Exception {
        try {
            productAudEntityDAO.save(fullProduct);
        } catch (Exception ex) {
           throw ex;
        }

    }

    @RequestMapping(value = "/product/approve", method = RequestMethod.POST)
    public void approve_productAud(long productAudId) throws Exception {
        try {
            productEntityDAO.approve(productAudId);
        } catch (Exception ex) {
           throw ex;
        }
    }

    @RequestMapping(value = "/product/deleteProduct", method = RequestMethod.DELETE)
    public void delete_product(String productId) throws Exception {
        try {
            productEntityDAO.deleteProduct(productId);
        } catch (Exception ex) {
           throw ex;
        }

    }
}