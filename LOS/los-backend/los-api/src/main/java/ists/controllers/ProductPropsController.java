package ists.controllers;

import ists.models.ProductPropsEntity;
import ists.models.UdtmFieldsEntity;
import ists.utility.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/16/2014.
 */
@RestController
public class ProductPropsController {

    /**
     * Get UDF for product
     * @param productId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/productprops/{productId}", method = RequestMethod.GET)
    public List<ProductPropsEntity> getProductProps(@PathVariable String productId) throws Exception {
        Session session = null;
        try {
//            Integer.parseInt(productId);
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from ProductPropsEntity where productId=?");
            query = query.setString(0, productId.toUpperCase());
            List<ProductPropsEntity> list = query.list();
            System.out.println("blah balh");

            if (list == null || list.size() < 1) {
                list = new ArrayList<ProductPropsEntity>();

                query = session.createQuery("from UdtmFieldsEntity where fieldGroupId='PRODUCT'");
                List<UdtmFieldsEntity> lstFields = query.list();
                if (lstFields != null && lstFields.size() > 0)
                    for (UdtmFieldsEntity field: lstFields) {
                        ProductPropsEntity entity = new ProductPropsEntity();
                        entity.setUdtmFieldsByUdfId(field);

                        list.add(entity);
                    }
            }

            return list;
        } catch (Exception e) {
            System.out.println("=====> Loi: ---- " + e.toString());
            throw e;
        }
    }
}
