package ists.blls;

import ists.models.ProductAudEntity;
import ists.models.SysRightEntity;
import ists.models.viewModels.ProductDetailRight;
import ists.utility.HibernateUtil;

import java.util.List;

/**
 * Created by Administrator on 10/15/2014.
 */
public class ProductAud extends BaseClass {
    /**
     * Method to update right list of Product Audit
     * @param productAud
     * @param rightList
     */
    public void updateRight(ProductAudEntity productAud, List<ProductDetailRight> rightList) {
        try {
            //Remove all right list of product
            HibernateUtil.getSessionFactory().getCurrentSession().createQuery("delete from SysRightEntity where pid = :pid")
                    .setLong("pid", productAud.getPid())
                    .executeUpdate();
            //Add right list for product
            for(ProductDetailRight r : rightList) {
                SysRightEntity right = new SysRightEntity();
                right.setSid(r.getSid());
                right.setPid(productAud.getPid());
                right.setAcl(r.getAcl());

                HibernateUtil.getSessionFactory().getCurrentSession().save(right);
            }
        }
        catch(RuntimeException e) {
            throw e;
        }
    }

    public ProductAudEntity getByProductAudIdWithTran(Long productAudId) {
        try {
            beginTransaction(null);

            ProductAudEntity proAud = (ProductAudEntity) HibernateUtil.getSessionFactory().getCurrentSession().get(ProductAudEntity.class, productAudId);

            commitTransaction(null);

            return proAud;
        }
        catch(RuntimeException e) {
            rollbackTransaction(null);
            throw e;
        }
    }
}
