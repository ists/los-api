package ists.blls;

import ists.models.ProductEntity;
import ists.utility.Common;
import ists.utility.HibernateUtil;

import java.util.List;

/**
 * Created by Administrator on 10/13/2014.
 */
public class SysUser extends BaseClass {
    public String getACLByUserIdWithTran(Long pid, String userId) {
        try {
            beginTransaction(null, null);

            //Add product, get productId to use.
            String acl = getACLByUserId(pid, userId);

            commitTransaction(null);

            return acl;
        } catch (RuntimeException e) {
            rollbackTransaction(null);
            throw e; // or display error message
        }
    }

    public String getACLByUserId(Long pid, String userID) {
        try {
            Long userSid = (Long) HibernateUtil.getSessionFactory().getCurrentSession().createQuery("select sid from SysUserEntity as su where su.userId = ?")
                    .setString(0, userID).uniqueResult();
            List<Long> list;
            list = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("select groupSid from SysGroupMemberEntity as sg where sg.memberSid = ?")
                    .setLong(0, userSid).list();
            list.add(userSid);
            StringBuilder acl = new StringBuilder();
            for (Long sid : list) {
                Object result = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("select acl from SysRightEntity as sr where sr.sid = ? and sr.pid = ?")
                        .setLong(0, sid)
                        .setLong(1, pid)
                        .uniqueResult();
                if(result != null)
                    acl.append(result.toString());
            }
            return acl.toString();
        } catch (RuntimeException e) {
            throw e; // or display error message
        }

    }
}
