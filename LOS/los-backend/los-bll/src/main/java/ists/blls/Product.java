package ists.blls;

import ists.models.*;
import ists.models.viewModels.ProductDetailRight;
import ists.models.viewModels.ProductLite;
import ists.models.viewModels.ProductRight;
import ists.utility.Common;
import ists.utility.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by Administrator on 10/6/2014.
 */
public class Product extends BaseClass {

    /**
     * Method to get list user, group with right of product
     * @param productId
     * @return
     */
    public List<ProductDetailRight> getProductDetailRight(String productId) {
        try {
            beginTransaction(null, null);
            List<ProductDetailRight> listRight = new ArrayList<ProductDetailRight>();
            ProductEntity product = (ProductEntity) HibernateUtil.getSessionFactory().getCurrentSession().get(ProductEntity.class, productId);
            if (product != null) {
                //Find list of sid with right of product
                List<SysRightEntity> list = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from SysRightEntity as sr where sr.pid = ?")
                                            .setLong(0, product.getPid()).list();
                    for (SysRightEntity r : list) {
                        //Check if user or group to return
                        SysUserEntity user = (SysUserEntity) HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from SysUserEntity as su where su.sid = ?")
                                            .setLong(0, r.getSid()).uniqueResult();
                        if(user != null)
                        {
                            listRight.add(new ProductDetailRight(r.getSid(), user.getUserName(), 1, r.getAcl()));
                        }
                        else {
                            SysGroupEntity group = (SysGroupEntity) HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from SysGroupEntity as sg where sg.sid = ?")
                                    .setLong(0, r.getSid()).uniqueResult();
                            if(group != null)
                            {
                                listRight.add(new ProductDetailRight(r.getSid(), group.getGroupName(), 2 , r.getAcl()));
                            }
                        }
                    }
            }

            commitTransaction(null);
            return listRight;
        } catch (RuntimeException e) {
            rollbackTransaction(null);
            throw e;
        }
    }

    /**
     * Method to get right (display action button) of product by userId
     * @param menuId
     * @param productId
     * @param userId
     * @return
     */
    public List<ProductRight> getProductRight(Long menuId, String productId, String userId) {
        try {
            List<ProductRight> list = new ArrayList<ProductRight>();
            beginTransaction(null, null);

            SysMenuEntity menu = (SysMenuEntity) HibernateUtil.getSessionFactory().getCurrentSession().get(SysMenuEntity.class, menuId);
            String menuAcl = new SysUser().getACLByUserId(menu.getPid(), userId);
            ProductEntity product = (ProductEntity) HibernateUtil.getSessionFactory().getCurrentSession().get(ProductEntity.class, productId);
            String acl = getACLOfProduct(product, menuAcl, userId);

            commitTransaction(null);

            //Return JSON of BUTTON RIGHT
            if (acl != null && !acl.isEmpty()) {
                if (acl.contains(Common.SYS_RIGHT_DEF.UPDATE.toString())) {
                    list.add(new ProductRight(Common.SYS_RIGHT_DEF.UPDATE.toString()));
                }
                if (acl.contains(Common.SYS_RIGHT_DEF.DELETE.toString())) {
                    list.add(new ProductRight(Common.SYS_RIGHT_DEF.DELETE.toString()));
                }
                //Only with INPUT RIGHT, we need INPUT RIGHT of menu and ALLOW_SUB of product == TRUE
                if (menuAcl.contains(Common.SYS_RIGHT_DEF.INPUT.toString()) && product.getAllowSub().equals(Common.BOOLEAN_CHAR.YES.toString())) {
                    list.add(new ProductRight(Common.SYS_RIGHT_DEF.INPUT.toString()));
                }
            }

            return list;
        } catch (RuntimeException e) {
            rollbackTransaction(null);
            throw e;
        }
    }

    /**
     * Method to recursive get ACL of product by userId
     * @param product
     * @param menuAcl
     * @param userId
     * @return
     */
    public String getACLOfProduct(ProductEntity product, String menuAcl, String userId) {
        try {
            String acl = null;
            if (product != null) {
                if (product.getInheritMenuAcl() == 1) {
                    //Inherit Right from Product menu
                    acl = menuAcl;
                } else if (product.getInheritParentAcl() == 1) {
                    //Inherit Right from parent product
                    if (product.getParentId() != null && !product.getParentId().isEmpty()) {
                        ProductEntity parentProduct = (ProductEntity) HibernateUtil.getSessionFactory().getCurrentSession().get(ProductEntity.class, product.getParentId());
                        acl = getACLOfProduct(parentProduct, menuAcl, userId);
                    }
                } else {
                    //Get Right of product
                    acl = new SysUser().getACLByUserId(product.getPid(), userId);
                }
            }
            return acl;
        } catch (RuntimeException e) {
            throw e;
        }
    }

    /**
     * Method to get Tree product by userID
     * @param menuId
     * @param userId
     * @return
     */
    public List<ProductLite> getTreeByUserId(Long menuId, String userId) {
        try {
            List<ProductLite> listProductLite = new ArrayList<ProductLite>();

            beginTransaction(null, null);
            //Get pid and heritable of menu for display tree
            SysMenuEntity menu = (SysMenuEntity) HibernateUtil.getSessionFactory().getCurrentSession().get(SysMenuEntity.class, menuId);
            String menuAcl = new SysUser().getACLByUserId(menu.getPid(), userId);
            if (menu.getHeritableAcl() == 1) {
                //When heritable == 1, all product record must follow "Product menu" right.
                if (menuAcl.contains(Common.SYS_RIGHT_DEF.VIEW.toString())) {
                    List<ProductEntity> list = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from ProductEntity").list();
                    for (ProductEntity p : list) {
                        listProductLite.add(new ProductLite(p));
                    }
                }
            } else {
                //When heritable == 0, follow record's right
                //When we have small table (<300 records), load all and use java to manipulate
                //When we have large table (>300 records), don't load all, get each part from database
                List<ProductEntity> list = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from ProductEntity").list();
                listProductLite = getProductByRight(list, null, menuAcl, null, userId);
            }
            commitTransaction(null);

            return listProductLite;
        } catch (RuntimeException e) {
            rollbackTransaction(null);
            throw e;
        }
    }

    public ProductEntity getByProductId(String productId) {
        try {
            return (ProductEntity) HibernateUtil.getSessionFactory().getCurrentSession().get(ProductEntity.class, productId);
        }
        catch (RuntimeException e) {
            throw e;
        }
    }

    /**
     * Method to recursive get all product by right for viewing
     * @param list
     * @param parentId
     * @param menuAcl
     * @param parentAcl
     * @param userId
     * @return
     */
    private List<ProductLite> getProductByRight(List<ProductEntity> list, final String parentId, String menuAcl, String parentAcl, String userId) {
        try {
            List<ProductLite> listProductLite = new ArrayList();

            if (parentId == null) {
                //Get Root node
                ProductEntity rootNode = list.stream().filter(new Predicate<ProductEntity>() {
                    @Override
                    public boolean test(ProductEntity productEntity) {
                        return productEntity.getParentId() == null || productEntity.getParentId().isEmpty();
                    }
                }).findFirst().get();

                //Check if Root node can be viewed by User
                String rootAcl = new SysUser().getACLByUserId(rootNode.getPid(), userId);
                if ((rootNode.getInheritMenuAcl() == 1 && menuAcl.contains(Common.SYS_RIGHT_DEF.VIEW.toString()))
                        || (rootNode.getInheritMenuAcl() == 0 && rootAcl.contains(Common.SYS_RIGHT_DEF.VIEW.toString()))) {
                    //Inherit from menu and have View right with Product Menu
                    //Or not inherit from menu and have View right with Root node
                    String eNodeAcl;
                    if (rootNode.getInheritMenuAcl() == 1) {
                        eNodeAcl = menuAcl;
                    } else {
                        eNodeAcl = rootAcl;
                    }
                    listProductLite.add(new ProductLite(rootNode));
                    //Recursive all leaf of root node.
                    List<ProductLite> leafList = getProductByRight(list, rootNode.getProductId(), menuAcl, eNodeAcl, userId);
                    listProductLite.addAll(leafList);
                }
            } else {
                //Get Leaf node
                for (ProductEntity node : list) {
                    if (node.getParentId() != null && node.getParentId().equals(parentId)) {
                        //Check if each node can be viewed by User
                        String nodeAcl = new SysUser().getACLByUserId(node.getPid(), userId);
                        if ((node.getInheritMenuAcl() == 1 && menuAcl.contains(Common.SYS_RIGHT_DEF.VIEW.toString()))
                                || (node.getInheritMenuAcl() == 0 && node.getInheritParentAcl() == 1 && parentAcl.contains(Common.SYS_RIGHT_DEF.VIEW.toString()))
                                || (node.getInheritMenuAcl() == 0 && node.getInheritParentAcl() == 0 && nodeAcl.contains(Common.SYS_RIGHT_DEF.VIEW.toString()))) {
                            //Inherit from menu and have View right with Product menu
                            //Or not inherit from menu, inherit from parent and have View right with parent node
                            //Or not inherit from anywhere and have View right with node
                            listProductLite.add(new ProductLite(node));

                            //Get exactly ACL of this node
                            String eNodeAcl;
                            if (node.getInheritMenuAcl() == 1) {
                                eNodeAcl = menuAcl;
                            } else if (node.getInheritMenuAcl() == 0 && node.getInheritParentAcl() == 1) {
                                eNodeAcl = parentAcl;
                            } else {
                                eNodeAcl = nodeAcl;
                            }
                            //Recursive all leaf of this node.
                            List<ProductLite> leafList = getProductByRight(list, node.getProductId(), menuAcl, eNodeAcl, userId);
                            listProductLite.addAll(leafList);
                        }
                    }
                }
            }

            return listProductLite;
        } catch (RuntimeException e) {
            throw e;
        }
    }

}