package ists.blls;

import ists.models.SysTransactionEntity;
import ists.utility.Common;
import ists.utility.HibernateUtil;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Administrator on 10/8/2014.
 */
public class BaseClass {
    protected SysTransactionEntity transaction;
    protected Long sysTranId;
    protected Long prevTranId;
    protected String TRAN_ACTION;

    protected void beginTransaction(String _TRAN_ACTION, Long _prevTranId) {
        if (_prevTranId != null)
            prevTranId = _prevTranId;

        beginTransaction(_TRAN_ACTION);
    }

    protected void beginTransaction(String _TRAN_ACTION) {
        try {
            if (_TRAN_ACTION != null && !_TRAN_ACTION.isEmpty())
                TRAN_ACTION = _TRAN_ACTION;
            else
                TRAN_ACTION = Common.TRAN_ACTION.INPUT.toString();

            sysTranId = createNewTransaction();
            //Begin transaction
            HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
        } catch (RuntimeException e) {
            throw e; // or display error message
        }
    }

    protected void commitTransaction(String _TRAN_COMMENT, Long _prevTranId) {
        if (_prevTranId != null)
            prevTranId = _prevTranId;

        commitTransaction(_TRAN_COMMENT);
    }

    protected void commitTransaction(String _TRAN_COMMENT) {
        try {
            //Commit transaction
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();

            //Get remain information of SysTransaction and save
            //When approve succeed, insert new Sys_Transaction with TRAN_STATUS = CLOSE
            if (TRAN_ACTION.equals(Common.TRAN_ACTION.APPROVE.toString()))
                transaction.setTranStatus(Common.TRAN_STATUS.CLOSE.toString());
            else
                transaction.setTranStatus(Common.TRAN_STATUS.ACTIVE.toString());

            if (_TRAN_COMMENT != null && !_TRAN_COMMENT.isEmpty())
                transaction.setComments(_TRAN_COMMENT);
            else
                transaction.setComments(Common.TRAN_COMMENT.SUCCEED.toString());

            updateTransaction();

            //After insert Transaction succeed, update previous Transaction
            updatePreviousTransaction();
        } catch (RuntimeException e) {
            throw e; // or display error message
        }
    }

    protected void rollbackTransaction(String _TRAN_COMMENT) {
        try {
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().rollback();

            //Add SysTransaction record for failed transaction.
            transaction.setTranStatus(Common.TRAN_STATUS.ERROR.toString());
            if (_TRAN_COMMENT != null && !_TRAN_COMMENT.isEmpty())
                transaction.setComments(_TRAN_COMMENT);
            else
                transaction.setComments(Common.TRAN_COMMENT.ERROR.toString());
            updateTransaction();
        } catch (RuntimeException e) {
            throw e; // or display error message
        }
    }

    private Long createNewTransaction() {
        try {
            //Add default information for SysTransaction
            HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
            //Get information of SysTransaction
            transaction = new SysTransactionEntity();
            transaction.setTranAction(TRAN_ACTION);
            if (prevTranId != null)
                transaction.setPrevSysTranId(prevTranId);
            transaction.setUserId(Common.getUserID());
//            transaction.setUserSessionId(Common.getUserSessionID());
            transaction.setTranStart(new Timestamp(new Date().getTime()));
            transaction.setDbSessionId(Common.getDBSessionID());
            Long id = new Long(String.valueOf(HibernateUtil.getSessionFactory().getCurrentSession().save(transaction)));
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();

            return id;
        } catch (RuntimeException e) {
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().rollback();
            throw e; // or display error message
        }
    }

    private void updateTransaction() {
        try {
            //Add default information for SysTransaction
            HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();

            transaction.setTranEnd(new Timestamp(new Date().getTime()));
            HibernateUtil.getSessionFactory().getCurrentSession().update(transaction);

            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();

        } catch (RuntimeException e) {
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().rollback();
            throw e; // or display error message
        }
    }

    protected void updatePreviousTransaction() {
        try {
            //When TRAN_ACTION = INPUT or PREVIOUS Sys_Transaction_ID == null, no need to update Previous Sys Transaction
            if (TRAN_ACTION.equals(Common.TRAN_ACTION.INPUT.toString()) || prevTranId == null)
                return;

            HibernateUtil.getSessionFactory().getCurrentSession().beginTransaction();
            SysTransactionEntity prevTran = (SysTransactionEntity) HibernateUtil.getSessionFactory().getCurrentSession().get(SysTransactionEntity.class, prevTranId);

            if (TRAN_ACTION.equals(Common.TRAN_ACTION.APPROVE.toString()))
                prevTran.setTranStatus(Common.TRAN_STATUS.APPROVED.toString());
            else if (TRAN_ACTION.equals(Common.TRAN_ACTION.REJECT.toString()))
                prevTran.setTranStatus(Common.TRAN_STATUS.REJECT.toString());
            else
                prevTran.setTranStatus(Common.TRAN_STATUS.CLOSE.toString());

            HibernateUtil.getSessionFactory().getCurrentSession().update(prevTran);

            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().commit();
        } catch (RuntimeException e) {
            HibernateUtil.getSessionFactory().getCurrentSession().getTransaction().rollback();
            throw e; // or display error message
        }
    }
}