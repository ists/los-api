package ists.utility;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;

/**
 * Created by Administrator on 10/16/2014.
 */
public class ObjectUtil<T extends Object> {

    public List<String> compareMethods(T obj1, T obj2) {
        List<String> list = new ArrayList<String>();
        try {
            Method[] methods = obj1.getClass().getMethods();

            for (Method method : methods) {
                method.setAccessible(true);

                Column column = method.getAnnotation(Column.class);
                if (column == null) {
                    continue;
                }

                if (!method.invoke(obj1, null).equals(method.invoke(obj2, null))) {
                    list.add(column.name());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list;
    }

    public String compareMethodsString(T obj1, T obj2) {
        String list = "";
        try {
            Method[] methods = obj1.getClass().getMethods();

            for (Method method : methods) {
                method.setAccessible(true);

                Column column = method.getAnnotation(Column.class);
                if (column == null) {
                    continue;
                }

                if (!method.invoke(obj1, null).equals(method.invoke(obj2, null))) {
                    list += column.name() + " ";
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return list.trim().replace(" ",";");
    }

    public List<String> getAllAnnotationName(T obj) {
        List<String> list = new ArrayList<String>();

        try {
            Method[] methods = obj.getClass().getMethods();

            for (Method method : methods) {
                method.setAccessible(true);
                Column column = method.getAnnotation(Column.class);
                if (column == null) {
                    continue;
                }
                list.add(column.name());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getAllAnnotationNameString(T obj) {
        String list = "";

        try {
            Method[] methods = obj.getClass().getMethods();

            for (Method method : methods) {
                method.setAccessible(true);
                Column column = method.getAnnotation(Column.class);
                if (column == null) {
                    continue;
                }
                list += column.name() + " ";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return list.trim().replace(" ",";");
    }
}

