package ists.utility;

//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;
//import javax.persistence.Query;

import org.hibernate.Query;
import org.hibernate.Session;

import java.math.BigDecimal;

/**
 * Created by Administrator on 10/17/2014.
 */
public class SequenceUtil {

//    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPAService");
//
//    private static EntityManager entityManager = emf.createEntityManager();

    public static Long getNextPId()
    {
//        Query query = entityManager.createNativeQuery("SELECT SQ_PID.NEXTVAL from Dual",
//                "NextSequenceVal");
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createSQLQuery("SELECT SQ_PID.NEXTVAL from Dual");
            return ((BigDecimal) query.uniqueResult()).longValue();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            ex.printStackTrace();
        } finally {
//            session.close();
        }
        return new Long(-1);
    }
}
