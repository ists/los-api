package ists.utility;

import org.hibernate.SQLQuery;

import java.util.List;

/**
 * Created by Administrator on 10/7/2014.
 */
public class Common {
    public static String getUserID() {
        //TODO: Need get userId here
        String userId = "demo.approve";

        return userId;
    }

    public static String getDBSessionID() {
        try {
            StringBuilder querySQL = new StringBuilder();
            querySQL.append("select\n" +
                            "sys_context('USERENV','SID') AS SID\n" +
                            "FROM dual"
            );
            SQLQuery query = HibernateUtil.getSessionFactory().getCurrentSession().createSQLQuery(querySQL.toString());
            List results = query.list();
            if (!results.isEmpty())
                return results.get(0).toString();
            else
                throw new RuntimeException();
        } catch (RuntimeException e) {
            throw e; // or display error message
        }
    }

    public static Long getUserSessionID() {
        //TODO: Need get userSessionId here
        Long userSessionId = Long.valueOf(1);

        return userSessionId;
    }

    public static enum TRAN_COMMENT {
        SUCCEED("Giao dịch thành công."),
        ERROR("Giao dịch thất bại.");

        private String value;

        private TRAN_COMMENT(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static enum TRAN_ACTION {
        INPUT("I"),
        EDIT_DRAFT("E"),
        REVIEW("R"),
        APPROVE("A"),
        REJECT("J"),
        INQUIRY("Q");

        private String value;

        private TRAN_ACTION(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static enum TRAN_STATUS {
        ACTIVE("A"),
        CLOSE("C"),
        ERROR("E"),
        REJECT("J"),
        APPROVED("P");

        private String value;

        private TRAN_STATUS(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static enum SYS_RIGHT_DEF {
        VIEW("V"),
        INPUT("I"),
        UPDATE("U"),
        DELETE("D"),
        CHANGE_PARENT("E"),
        APPROVE("A");

        private String value;

        private SYS_RIGHT_DEF(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static enum BOOLEAN_CHAR {
        YES("Y"),
        NO("N");

        private String value;

        private BOOLEAN_CHAR(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

}
