package ists.utility;

/**
 * Created by Administrator on 10/7/2014.
 */
public class EnumUtil {

    public static enum AuditOperation {
        INSERT,UPDATE,DELETE, EDIT_DRAFT;

        public String toString() {
            switch (this) {
                case INSERT:
                    return "I";
                case UPDATE:
                    return "U";
                case DELETE:
                    return "D";
                case EDIT_DRAFT:
                    return "E";
            }
            return super.toString();
        }

        public String getValue() {
            return toString();
        }
    }

    public static enum ApprovalStatus {
        INPUT,REVIEW,APPROVED,ERROR, REJECT;

        public String toString() {
            switch (this) {
                case INPUT:
                    return "I";
                case REVIEW:
                    return "U";
                case APPROVED:
                    return "A";
                case ERROR:
                    return "E";
                case REJECT:
                    return "J";
            }
            return super.toString();
        }

        public String getValue() {
            return toString();
        }
    }

    public static enum ACLEnum {
        VIEW,EDIT,DELETE,APPROVE,CREATE,SUB_TREE,USE;

        public String toString() {
            switch (this) {
                case VIEW:
                    return "V";
                case EDIT:
                    return "E";
                case DELETE:
                    return "D";
                case APPROVE:
                    return "A";
                case CREATE:
                    return "C";
                case SUB_TREE:
                    return "S";
                case USE:
                    return "U";
            }
            return super.toString();
        }

        public String getValue() {
            return toString();
        }
    }

    public static enum BooleanEnum {
        Y,N;

        public String toString() {
            switch (this) {
                case Y:
                    return "Y";
                case N:
                    return "N";
            }
            return super.toString();
        }

        public boolean getValue() {
            switch (this) {
                case Y:
                    return true;
                default:
                    return false;
            }
        }
    }
}
