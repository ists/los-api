package ists.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_TRANSACTION", schema = "ISTS", catalog = "")
public class SysTransactionEntity {
    private Long sysTranId;
    private String userId;
    private String dbSessionId;
    private Timestamp tranStart;
    private Timestamp tranEnd;
    private String tranCode;
    private String errCode;
    private String errDesc;
//    private Long userSessionId;
    private String tranAction;
    private Long prevSysTranId;
    private String tranStatus;
    private String comments;
    private SysUserSessionEntity sysUserSessionByUserSessionId;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SysTransactionSequence")
    @SequenceGenerator(name = "SysTransactionSequence", sequenceName = "SQ_SYS_TRAN_ID", allocationSize = 1)
    @Column(name = "SYS_TRAN_ID", nullable = false, insertable = true, updatable = true, precision = 0)
    public Long getSysTranId() {
        return sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    @Basic
    @Column(name = "USER_ID", nullable = false, insertable = true, updatable = true, length = 20)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "DB_SESSION_ID", nullable = true, insertable = true, updatable = true, length = 20)
    public String getDbSessionId() {
        return dbSessionId;
    }

    public void setDbSessionId(String dbSessionId) {
        this.dbSessionId = dbSessionId;
    }

    @Basic
    @Column(name = "TRAN_START", nullable = true, insertable = true, updatable = true)
    public Timestamp getTranStart() {
        return tranStart;
    }

    public void setTranStart(Timestamp tranStart) {
        this.tranStart = tranStart;
    }

    @Basic
    @Column(name = "TRAN_END", nullable = true, insertable = true, updatable = true)
    public Timestamp getTranEnd() {
        return tranEnd;
    }

    public void setTranEnd(Timestamp tranEnd) {
        this.tranEnd = tranEnd;
    }

    @Basic
    @Column(name = "TRAN_CODE", nullable = true, insertable = true, updatable = true, length = 30)
    public String getTranCode() {
        return tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    @Basic
    @Column(name = "ERR_CODE", nullable = true, insertable = true, updatable = true, length = 10)
    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    @Basic
    @Column(name = "ERR_DESC", nullable = true, insertable = true, updatable = true, length = 4000)
    public String getErrDesc() {
        return errDesc;
    }

    public void setErrDesc(String errDesc) {
        this.errDesc = errDesc;
    }

//    @Basic
//    @Column(name = "USER_SESSION_ID", nullable = true, insertable = true, updatable = true, precision = -127)
//    public Long getUserSessionId() {
//        return userSessionId;
//    }
//
//    public void setUserSessionId(Long userSessionId) {
//        this.userSessionId = userSessionId;
//    }

    @Basic
    @Column(name = "TRAN_ACTION", nullable = true, insertable = true, updatable = true, length = 1)
    public String getTranAction() {
        return tranAction;
    }

    public void setTranAction(String tranAction) {
        this.tranAction = tranAction;
    }

    @Basic
    @Column(name = "PREV_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPrevSysTranId() {
        return prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    @Basic
    @Column(name = "TRAN_STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getTranStatus() {
        return tranStatus;
    }

    public void setTranStatus(String tranStatus) {
        this.tranStatus = tranStatus;
    }

    @Basic
    @Column(name = "COMMENTS", nullable = true, insertable = true, updatable = true, length = 256)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysTransactionEntity that = (SysTransactionEntity) o;

        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (dbSessionId != null ? !dbSessionId.equals(that.dbSessionId) : that.dbSessionId != null) return false;
        if (errCode != null ? !errCode.equals(that.errCode) : that.errCode != null) return false;
        if (errDesc != null ? !errDesc.equals(that.errDesc) : that.errDesc != null) return false;
        if (prevSysTranId != null ? !prevSysTranId.equals(that.prevSysTranId) : that.prevSysTranId != null)
            return false;
        if (sysTranId != null ? !sysTranId.equals(that.sysTranId) : that.sysTranId != null) return false;
        if (tranAction != null ? !tranAction.equals(that.tranAction) : that.tranAction != null) return false;
        if (tranCode != null ? !tranCode.equals(that.tranCode) : that.tranCode != null) return false;
        if (tranEnd != null ? !tranEnd.equals(that.tranEnd) : that.tranEnd != null) return false;
        if (tranStart != null ? !tranStart.equals(that.tranStart) : that.tranStart != null) return false;
        if (tranStatus != null ? !tranStatus.equals(that.tranStatus) : that.tranStatus != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
//        if (userSessionId != null ? !userSessionId.equals(that.userSessionId) : that.userSessionId != null)
//            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sysTranId != null ? sysTranId.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (dbSessionId != null ? dbSessionId.hashCode() : 0);
        result = 31 * result + (tranStart != null ? tranStart.hashCode() : 0);
        result = 31 * result + (tranEnd != null ? tranEnd.hashCode() : 0);
        result = 31 * result + (tranCode != null ? tranCode.hashCode() : 0);
        result = 31 * result + (errCode != null ? errCode.hashCode() : 0);
        result = 31 * result + (errDesc != null ? errDesc.hashCode() : 0);
//        result = 31 * result + (userSessionId != null ? userSessionId.hashCode() : 0);
        result = 31 * result + (tranAction != null ? tranAction.hashCode() : 0);
        result = 31 * result + (prevSysTranId != null ? prevSysTranId.hashCode() : 0);
        result = 31 * result + (tranStatus != null ? tranStatus.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "USER_SESSION_ID", referencedColumnName = "USER_SESSION_ID")
    public SysUserSessionEntity getSysUserSessionByUserSessionId() {
        return sysUserSessionByUserSessionId;
    }

    public void setSysUserSessionByUserSessionId(SysUserSessionEntity sysUserSessionByUserSessionId) {
        this.sysUserSessionByUserSessionId = sysUserSessionByUserSessionId;
    }
}
