package ists.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_GROUP_MEMBER_AUD", schema = "ISTS", catalog = "")
public class SysGroupMemberAudEntity {
    private Long sysGroupMemberAudId;
    private Long sysTranId;
    private Timestamp auditDate;
    private String auditFields;
    private String auditOperation;
    private String approvalStatus;
    private Long prevSysTranId;
    private Long updSeq;
    private Long groupSid;
    private Long memberSid;
    private String acl;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SysGroupMemberAudSequence")
    @SequenceGenerator(name = "SysGroupMemberAudSequence", sequenceName = "SQ_SYS_GROUP_MEMBER_AUD_ID", allocationSize = 1)
    @Column(name = "SYS_GROUP_MEMBER_AUD_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getSysGroupMemberAudId() {
        return sysGroupMemberAudId;
    }

    public void setSysGroupMemberAudId(Long sysGroupMemberAudId) {
        this.sysGroupMemberAudId = sysGroupMemberAudId;
    }

    @Basic
    @Column(name = "SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getSysTranId() {
        return sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    @Basic
    @Column(name = "AUDIT_DATE", nullable = true, insertable = true, updatable = true)
    public Timestamp getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    @Basic
    @Column(name = "AUDIT_FIELDS", nullable = true, insertable = true, updatable = true, length = 25)
    public String getAuditFields() {
        return auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    @Basic
    @Column(name = "AUDIT_OPERATION", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAuditOperation() {
        return auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    @Basic
    @Column(name = "APPROVAL_STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Basic
    @Column(name = "PREV_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPrevSysTranId() {
        return prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Basic
    @Column(name = "GROUP_SID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getGroupSid() {
        return groupSid;
    }

    public void setGroupSid(Long groupSid) {
        this.groupSid = groupSid;
    }

    @Basic
    @Column(name = "MEMBER_SID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getMemberSid() {
        return memberSid;
    }

    public void setMemberSid(Long memberSid) {
        this.memberSid = memberSid;
    }

    @Basic
    @Column(name = "ACL", nullable = true, insertable = true, updatable = true, length = 200)
    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysGroupMemberAudEntity that = (SysGroupMemberAudEntity) o;

        if (acl != null ? !acl.equals(that.acl) : that.acl != null) return false;
        if (approvalStatus != null ? !approvalStatus.equals(that.approvalStatus) : that.approvalStatus != null)
            return false;
        if (auditDate != null ? !auditDate.equals(that.auditDate) : that.auditDate != null) return false;
        if (auditFields != null ? !auditFields.equals(that.auditFields) : that.auditFields != null) return false;
        if (auditOperation != null ? !auditOperation.equals(that.auditOperation) : that.auditOperation != null)
            return false;
        if (groupSid != null ? !groupSid.equals(that.groupSid) : that.groupSid != null) return false;
        if (memberSid != null ? !memberSid.equals(that.memberSid) : that.memberSid != null) return false;
        if (prevSysTranId != null ? !prevSysTranId.equals(that.prevSysTranId) : that.prevSysTranId != null)
            return false;
        if (sysGroupMemberAudId != null ? !sysGroupMemberAudId.equals(that.sysGroupMemberAudId) : that.sysGroupMemberAudId != null)
            return false;
        if (sysTranId != null ? !sysTranId.equals(that.sysTranId) : that.sysTranId != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sysGroupMemberAudId != null ? sysGroupMemberAudId.hashCode() : 0;
        result = 31 * result + (sysTranId != null ? sysTranId.hashCode() : 0);
        result = 31 * result + (auditDate != null ? auditDate.hashCode() : 0);
        result = 31 * result + (auditFields != null ? auditFields.hashCode() : 0);
        result = 31 * result + (auditOperation != null ? auditOperation.hashCode() : 0);
        result = 31 * result + (approvalStatus != null ? approvalStatus.hashCode() : 0);
        result = 31 * result + (prevSysTranId != null ? prevSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (groupSid != null ? groupSid.hashCode() : 0);
        result = 31 * result + (memberSid != null ? memberSid.hashCode() : 0);
        result = 31 * result + (acl != null ? acl.hashCode() : 0);
        return result;
    }
}
