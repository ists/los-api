package ists.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "UDTM_FIELDS_AUD", schema = "ISTS", catalog = "")
public class UdtmFieldsAudEntity {
    private Long udtmFieldsAudId;
    private Long sysTranId;
    private Timestamp auditDate;
    private String auditFields;
    private String auditOperation;
    private String approvalStatus;
    private Long prevSysTranId;
    private Long updSeq;
    private Long udfId;
    private String fieldGroupId;
    private String fieldName;
    private String fieldDescription;
    private String fieldType;
    private String valType;
    private String incExcFlag;
    private String mandatory;
    private Long minVal;
    private Long maxVal;
    private String mask;
    private Long minLength;
    private Long maxLength;
    private String entityName;
    private String entityDescription;
    private String entityTable;
    private String entityWhereClause;
    private String recordStat;
    private String authStat;
    private String uniqueField;
    private String defaultValue;
    private String drvRuleType;
    private String drvRule;
    private String valRuleType;
    private String valRule;
    private String updateAllowed;
    private String acl;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UdtmFieldsAudSequence")
    @SequenceGenerator(name = "UdtmFieldsAudSequence", sequenceName = "SQ_UDTM_FIELDS_AUD_ID", allocationSize = 1)
    @Column(name = "UDTM_FIELDS_AUD_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getUdtmFieldsAudId() {
        return udtmFieldsAudId;
    }

    public void setUdtmFieldsAudId(Long udtmFieldsAudId) {
        this.udtmFieldsAudId = udtmFieldsAudId;
    }

    @Basic
    @Column(name = "SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getSysTranId() {
        return sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    @Basic
    @Column(name = "AUDIT_DATE", nullable = true, insertable = true, updatable = true)
    public Timestamp getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    @Basic
    @Column(name = "AUDIT_FIELDS", nullable = true, insertable = true, updatable = true, length = 315)
    public String getAuditFields() {
        return auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    @Basic
    @Column(name = "AUDIT_OPERATION", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAuditOperation() {
        return auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    @Basic
    @Column(name = "APPROVAL_STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Basic
    @Column(name = "PREV_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPrevSysTranId() {
        return prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Basic
    @Column(name = "UDF_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getUdfId() {
        return udfId;
    }

    public void setUdfId(Long udfId) {
        this.udfId = udfId;
    }

    @Basic
    @Column(name = "FIELD_GROUP_ID", nullable = true, insertable = true, updatable = true, length = 105)
    public String getFieldGroupId() {
        return fieldGroupId;
    }

    public void setFieldGroupId(String fieldGroupId) {
        this.fieldGroupId = fieldGroupId;
    }

    @Basic
    @Column(name = "FIELD_NAME", nullable = true, insertable = true, updatable = true, length = 105)
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    @Basic
    @Column(name = "FIELD_DESCRIPTION", nullable = true, insertable = true, updatable = true, length = 500)
    public String getFieldDescription() {
        return fieldDescription;
    }

    public void setFieldDescription(String fieldDescription) {
        this.fieldDescription = fieldDescription;
    }

    @Basic
    @Column(name = "FIELD_TYPE", nullable = true, insertable = true, updatable = true, length = 1)
    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    @Basic
    @Column(name = "VAL_TYPE", nullable = true, insertable = true, updatable = true, length = 3)
    public String getValType() {
        return valType;
    }

    public void setValType(String valType) {
        this.valType = valType;
    }

    @Basic
    @Column(name = "INC_EXC_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    public String getIncExcFlag() {
        return incExcFlag;
    }

    public void setIncExcFlag(String incExcFlag) {
        this.incExcFlag = incExcFlag;
    }

    @Basic
    @Column(name = "MANDATORY", nullable = true, insertable = true, updatable = true, length = 1)
    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    @Basic
    @Column(name = "MIN_VAL", nullable = true, insertable = true, updatable = true, precision = 3)
    public Long getMinVal() {
        return minVal;
    }

    public void setMinVal(Long minVal) {
        this.minVal = minVal;
    }

    @Basic
    @Column(name = "MAX_VAL", nullable = true, insertable = true, updatable = true, precision = 3)
    public Long getMaxVal() {
        return maxVal;
    }

    public void setMaxVal(Long maxVal) {
        this.maxVal = maxVal;
    }

    @Basic
    @Column(name = "MASK", nullable = true, insertable = true, updatable = true, length = 25)
    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    @Basic
    @Column(name = "MIN_LENGTH", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getMinLength() {
        return minLength;
    }

    public void setMinLength(Long minLength) {
        this.minLength = minLength;
    }

    @Basic
    @Column(name = "MAX_LENGTH", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Long maxLength) {
        this.maxLength = maxLength;
    }

    @Basic
    @Column(name = "ENTITY_NAME", nullable = true, insertable = true, updatable = true, length = 30)
    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    @Basic
    @Column(name = "ENTITY_DESCRIPTION", nullable = true, insertable = true, updatable = true, length = 30)
    public String getEntityDescription() {
        return entityDescription;
    }

    public void setEntityDescription(String entityDescription) {
        this.entityDescription = entityDescription;
    }

    @Basic
    @Column(name = "ENTITY_TABLE", nullable = true, insertable = true, updatable = true, length = 30)
    public String getEntityTable() {
        return entityTable;
    }

    public void setEntityTable(String entityTable) {
        this.entityTable = entityTable;
    }

    @Basic
    @Column(name = "ENTITY_WHERE_CLAUSE", nullable = true, insertable = true, updatable = true, length = 1000)
    public String getEntityWhereClause() {
        return entityWhereClause;
    }

    public void setEntityWhereClause(String entityWhereClause) {
        this.entityWhereClause = entityWhereClause;
    }

    @Basic
    @Column(name = "RECORD_STAT", nullable = true, insertable = true, updatable = true, length = 1)
    public String getRecordStat() {
        return recordStat;
    }

    public void setRecordStat(String recordStat) {
        this.recordStat = recordStat;
    }

    @Basic
    @Column(name = "AUTH_STAT", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAuthStat() {
        return authStat;
    }

    public void setAuthStat(String authStat) {
        this.authStat = authStat;
    }

    @Basic
    @Column(name = "UNIQUE_FIELD", nullable = true, insertable = true, updatable = true, length = 1)
    public String getUniqueField() {
        return uniqueField;
    }

    public void setUniqueField(String uniqueField) {
        this.uniqueField = uniqueField;
    }

    @Basic
    @Column(name = "DEFAULT_VALUE", nullable = true, insertable = true, updatable = true, length = 150)
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Basic
    @Column(name = "DRV_RULE_TYPE", nullable = true, insertable = true, updatable = true, length = 4)
    public String getDrvRuleType() {
        return drvRuleType;
    }

    public void setDrvRuleType(String drvRuleType) {
        this.drvRuleType = drvRuleType;
    }

    @Basic
    @Column(name = "DRV_RULE", nullable = true, insertable = true, updatable = true, length = 4000)
    public String getDrvRule() {
        return drvRule;
    }

    public void setDrvRule(String drvRule) {
        this.drvRule = drvRule;
    }

    @Basic
    @Column(name = "VAL_RULE_TYPE", nullable = true, insertable = true, updatable = true, length = 1)
    public String getValRuleType() {
        return valRuleType;
    }

    public void setValRuleType(String valRuleType) {
        this.valRuleType = valRuleType;
    }

    @Basic
    @Column(name = "VAL_RULE", nullable = true, insertable = true, updatable = true, length = 4000)
    public String getValRule() {
        return valRule;
    }

    public void setValRule(String valRule) {
        this.valRule = valRule;
    }

    @Basic
    @Column(name = "UPDATE_ALLOWED", nullable = true, insertable = true, updatable = true, length = 1)
    public String getUpdateAllowed() {
        return updateAllowed;
    }

    public void setUpdateAllowed(String updateAllowed) {
        this.updateAllowed = updateAllowed;
    }

    @Basic
    @Column(name = "ACL", nullable = true, insertable = true, updatable = true, length = 50)
    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UdtmFieldsAudEntity that = (UdtmFieldsAudEntity) o;

        if (acl != null ? !acl.equals(that.acl) : that.acl != null) return false;
        if (approvalStatus != null ? !approvalStatus.equals(that.approvalStatus) : that.approvalStatus != null)
            return false;
        if (auditDate != null ? !auditDate.equals(that.auditDate) : that.auditDate != null) return false;
        if (auditFields != null ? !auditFields.equals(that.auditFields) : that.auditFields != null) return false;
        if (auditOperation != null ? !auditOperation.equals(that.auditOperation) : that.auditOperation != null)
            return false;
        if (authStat != null ? !authStat.equals(that.authStat) : that.authStat != null) return false;
        if (defaultValue != null ? !defaultValue.equals(that.defaultValue) : that.defaultValue != null) return false;
        if (drvRule != null ? !drvRule.equals(that.drvRule) : that.drvRule != null) return false;
        if (drvRuleType != null ? !drvRuleType.equals(that.drvRuleType) : that.drvRuleType != null) return false;
        if (entityDescription != null ? !entityDescription.equals(that.entityDescription) : that.entityDescription != null)
            return false;
        if (entityName != null ? !entityName.equals(that.entityName) : that.entityName != null) return false;
        if (entityTable != null ? !entityTable.equals(that.entityTable) : that.entityTable != null) return false;
        if (entityWhereClause != null ? !entityWhereClause.equals(that.entityWhereClause) : that.entityWhereClause != null)
            return false;
        if (fieldDescription != null ? !fieldDescription.equals(that.fieldDescription) : that.fieldDescription != null)
            return false;
        if (fieldGroupId != null ? !fieldGroupId.equals(that.fieldGroupId) : that.fieldGroupId != null) return false;
        if (fieldName != null ? !fieldName.equals(that.fieldName) : that.fieldName != null) return false;
        if (fieldType != null ? !fieldType.equals(that.fieldType) : that.fieldType != null) return false;
        if (incExcFlag != null ? !incExcFlag.equals(that.incExcFlag) : that.incExcFlag != null) return false;
        if (mandatory != null ? !mandatory.equals(that.mandatory) : that.mandatory != null) return false;
        if (mask != null ? !mask.equals(that.mask) : that.mask != null) return false;
        if (maxLength != null ? !maxLength.equals(that.maxLength) : that.maxLength != null) return false;
        if (maxVal != null ? !maxVal.equals(that.maxVal) : that.maxVal != null) return false;
        if (minLength != null ? !minLength.equals(that.minLength) : that.minLength != null) return false;
        if (minVal != null ? !minVal.equals(that.minVal) : that.minVal != null) return false;
        if (prevSysTranId != null ? !prevSysTranId.equals(that.prevSysTranId) : that.prevSysTranId != null)
            return false;
        if (recordStat != null ? !recordStat.equals(that.recordStat) : that.recordStat != null) return false;
        if (sysTranId != null ? !sysTranId.equals(that.sysTranId) : that.sysTranId != null) return false;
        if (udfId != null ? !udfId.equals(that.udfId) : that.udfId != null) return false;
        if (udtmFieldsAudId != null ? !udtmFieldsAudId.equals(that.udtmFieldsAudId) : that.udtmFieldsAudId != null)
            return false;
        if (uniqueField != null ? !uniqueField.equals(that.uniqueField) : that.uniqueField != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;
        if (updateAllowed != null ? !updateAllowed.equals(that.updateAllowed) : that.updateAllowed != null)
            return false;
        if (valRule != null ? !valRule.equals(that.valRule) : that.valRule != null) return false;
        if (valRuleType != null ? !valRuleType.equals(that.valRuleType) : that.valRuleType != null) return false;
        if (valType != null ? !valType.equals(that.valType) : that.valType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = udtmFieldsAudId != null ? udtmFieldsAudId.hashCode() : 0;
        result = 31 * result + (sysTranId != null ? sysTranId.hashCode() : 0);
        result = 31 * result + (auditDate != null ? auditDate.hashCode() : 0);
        result = 31 * result + (auditFields != null ? auditFields.hashCode() : 0);
        result = 31 * result + (auditOperation != null ? auditOperation.hashCode() : 0);
        result = 31 * result + (approvalStatus != null ? approvalStatus.hashCode() : 0);
        result = 31 * result + (prevSysTranId != null ? prevSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (udfId != null ? udfId.hashCode() : 0);
        result = 31 * result + (fieldGroupId != null ? fieldGroupId.hashCode() : 0);
        result = 31 * result + (fieldName != null ? fieldName.hashCode() : 0);
        result = 31 * result + (fieldDescription != null ? fieldDescription.hashCode() : 0);
        result = 31 * result + (fieldType != null ? fieldType.hashCode() : 0);
        result = 31 * result + (valType != null ? valType.hashCode() : 0);
        result = 31 * result + (incExcFlag != null ? incExcFlag.hashCode() : 0);
        result = 31 * result + (mandatory != null ? mandatory.hashCode() : 0);
        result = 31 * result + (minVal != null ? minVal.hashCode() : 0);
        result = 31 * result + (maxVal != null ? maxVal.hashCode() : 0);
        result = 31 * result + (mask != null ? mask.hashCode() : 0);
        result = 31 * result + (minLength != null ? minLength.hashCode() : 0);
        result = 31 * result + (maxLength != null ? maxLength.hashCode() : 0);
        result = 31 * result + (entityName != null ? entityName.hashCode() : 0);
        result = 31 * result + (entityDescription != null ? entityDescription.hashCode() : 0);
        result = 31 * result + (entityTable != null ? entityTable.hashCode() : 0);
        result = 31 * result + (entityWhereClause != null ? entityWhereClause.hashCode() : 0);
        result = 31 * result + (recordStat != null ? recordStat.hashCode() : 0);
        result = 31 * result + (authStat != null ? authStat.hashCode() : 0);
        result = 31 * result + (uniqueField != null ? uniqueField.hashCode() : 0);
        result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
        result = 31 * result + (drvRuleType != null ? drvRuleType.hashCode() : 0);
        result = 31 * result + (drvRule != null ? drvRule.hashCode() : 0);
        result = 31 * result + (valRuleType != null ? valRuleType.hashCode() : 0);
        result = 31 * result + (valRule != null ? valRule.hashCode() : 0);
        result = 31 * result + (updateAllowed != null ? updateAllowed.hashCode() : 0);
        result = 31 * result + (acl != null ? acl.hashCode() : 0);
        return result;
    }
}
