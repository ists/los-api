package ists.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Administrator on 10/10/2014.
 */
public class ProductPropsEntityPK implements Serializable {
    private String productId;
    private Long udfId;

    @Column(name = "PRODUCT_ID", nullable = false, insertable = true, updatable = true, length = 20)
    @Id
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Column(name = "UDF_ID", nullable = false, insertable = true, updatable = true, precision = 0)
    @Id
    public Long getUdfId() {
        return udfId;
    }

    public void setUdfId(Long udfId) {
        this.udfId = udfId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductPropsEntityPK that = (ProductPropsEntityPK) o;

        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (udfId != null ? !udfId.equals(that.udfId) : that.udfId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productId != null ? productId.hashCode() : 0;
        result = 31 * result + (udfId != null ? udfId.hashCode() : 0);
        return result;
    }
}
