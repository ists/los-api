/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ists.models.viewModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Administrator
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDetailRight {

    private Long sid;
    private String name;
    private int type;
    private String acl;

    public ProductDetailRight() {}

    public ProductDetailRight(Long sid, String name, int type, String acl) {
        this.sid = sid;
        this.name = name;
        this.type = type;
        this.acl = acl;
    }

    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }
}
