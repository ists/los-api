package ists.models;

import javax.persistence.*;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_RIGHT_DEF", schema = "ISTS", catalog = "")
public class SysRightDefEntity {
    private String rightCode;
    private String rightDesc;

    @Id
    @Column(name = "RIGHT_CODE", nullable = false, insertable = true, updatable = true, length = 1)
    public String getRightCode() {
        return rightCode;
    }

    public void setRightCode(String rightCode) {
        this.rightCode = rightCode;
    }

    @Basic
    @Column(name = "RIGHT_DESC", nullable = true, insertable = true, updatable = true, length = 50)
    public String getRightDesc() {
        return rightDesc;
    }

    public void setRightDesc(String rightDesc) {
        this.rightDesc = rightDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRightDefEntity that = (SysRightDefEntity) o;

        if (rightCode != null ? !rightCode.equals(that.rightCode) : that.rightCode != null) return false;
        if (rightDesc != null ? !rightDesc.equals(that.rightDesc) : that.rightDesc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rightCode != null ? rightCode.hashCode() : 0;
        result = 31 * result + (rightDesc != null ? rightDesc.hashCode() : 0);
        return result;
    }
}
