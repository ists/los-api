package ists.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "PRODUCT_PROPS", schema = "ISTS", catalog = "")
@IdClass(ProductPropsEntityPK.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPropsEntity {
    private String productId;
    private Long udfId;
    private String udfValue;
    private Long lastSysTranId;
    private Long updSeq;
//    private ProductEntity productByProductId;
    private UdtmFieldsEntity udtmFieldsByUdfId;

    @Id
    @Column(name = "PRODUCT_ID", nullable = false, insertable = true, updatable = true, length = 20)
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Id
    @Column(name = "UDF_ID", nullable = false, insertable = true, updatable = true, precision = 0)
    public Long getUdfId() {
        return udfId;
    }

    public void setUdfId(Long udfId) {
        this.udfId = udfId;
    }

    @Basic
    @Column(name = "UDF_VALUE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getUdfValue() {
        return udfValue;
    }

    public void setUdfValue(String udfValue) {
        this.udfValue = udfValue;
    }

    @Basic
    @Column(name = "LAST_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getLastSysTranId() {
        return lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductPropsEntity that = (ProductPropsEntity) o;

        if (lastSysTranId != null ? !lastSysTranId.equals(that.lastSysTranId) : that.lastSysTranId != null)
            return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (udfId != null ? !udfId.equals(that.udfId) : that.udfId != null) return false;
        if (udfValue != null ? !udfValue.equals(that.udfValue) : that.udfValue != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productId != null ? productId.hashCode() : 0;
        result = 31 * result + (udfId != null ? udfId.hashCode() : 0);
        result = 31 * result + (udfValue != null ? udfValue.hashCode() : 0);
        result = 31 * result + (lastSysTranId != null ? lastSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        return result;
    }

//    @ManyToOne
//    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID", nullable = false)
//    public ProductEntity getProductByProductId() {
//        return productByProductId;
//    }
//
//    public void setProductByProductId(ProductEntity productByProductId) {
//        this.productByProductId = productByProductId;
//    }

    @ManyToOne
    @JoinColumn(name = "UDF_ID", referencedColumnName = "UDF_ID", nullable = false)
    public UdtmFieldsEntity getUdtmFieldsByUdfId() {
        return udtmFieldsByUdfId;
    }

    public void setUdtmFieldsByUdfId(UdtmFieldsEntity udtmFieldsByUdfId) {
        this.udtmFieldsByUdfId = udtmFieldsByUdfId;
    }

}
