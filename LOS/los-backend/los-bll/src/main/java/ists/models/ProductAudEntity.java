package ists.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@javax.persistence.Table(name = "PRODUCT_AUD", schema = "ISTS", catalog = "")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductAudEntity {
    private Long productAudId;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProductAudSequence")
    @SequenceGenerator(name = "ProductAudSequence", sequenceName = "SQ_PRODUCT_AUD_ID", allocationSize = 1)
    @javax.persistence.Column(name = "PRODUCT_AUD_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getProductAudId() {
        return productAudId;
    }

    public void setProductAudId(Long productAudId) {
        this.productAudId = productAudId;
    }

    private Long sysTranId;

    @Basic
    @javax.persistence.Column(name = "SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getSysTranId() {
        return sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    private Timestamp auditDate;

    @Basic
    @javax.persistence.Column(name = "AUDIT_DATE", nullable = true, insertable = true, updatable = true)
    public Timestamp getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    private String auditFields;

    @Basic
    @javax.persistence.Column(name = "AUDIT_FIELDS", nullable = true, insertable = true, updatable = true, length = 185)
    public String getAuditFields() {
        return auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    private String auditOperation;

    @Basic
    @javax.persistence.Column(name = "AUDIT_OPERATION", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAuditOperation() {
        return auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    private String approvalStatus;

    @Basic
    @javax.persistence.Column(name = "APPROVAL_STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    private Long prevSysTranId;

    @Basic
    @javax.persistence.Column(name = "PREV_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPrevSysTranId() {
        return prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    private Long updSeq;

    @Basic
    @javax.persistence.Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    private String productId;

    @Basic
    @javax.persistence.Column(name = "PRODUCT_ID", nullable = true, insertable = true, updatable = true, length = 20)
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    private String productName;

    @Basic
    @javax.persistence.Column(name = "PRODUCT_NAME", nullable = true, insertable = true, updatable = true, length = 200)
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    private String productSlogan;

    @Basic
    @javax.persistence.Column(name = "PRODUCT_SLOGAN", nullable = true, insertable = true, updatable = true, length = 400)
    public String getProductSlogan() {
        return productSlogan;
    }

    public void setProductSlogan(String productSlogan) {
        this.productSlogan = productSlogan;
    }

    private String parentId;

    @Basic
    @javax.persistence.Column(name = "PARENT_ID", nullable = true, insertable = true, updatable = true, length = 20)
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    private Long productOwner;

    @Basic
    @javax.persistence.Column(name = "PRODUCT_OWNER", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getProductOwner() {
        return productOwner;
    }

    public void setProductOwner(Long productOwner) {
        this.productOwner = productOwner;
    }

    private String allowSub;

    @Basic
    @javax.persistence.Column(name = "ALLOW_SUB", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAllowSub() {
        return allowSub;
    }

    public void setAllowSub(String allowSub) {
        this.allowSub = allowSub;
    }

    private String allowCont;

    @Basic
    @javax.persistence.Column(name = "ALLOW_CONT", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAllowCont() {
        return allowCont;
    }

    public void setAllowCont(String allowCont) {
        this.allowCont = allowCont;
    }

    private String inheritCondition;

    @Basic
    @javax.persistence.Column(name = "INHERIT_CONDITION", nullable = true, insertable = true, updatable = true, length = 1)
    public String getInheritCondition() {
        return inheritCondition;
    }

    public void setInheritCondition(String inheritCondition) {
        this.inheritCondition = inheritCondition;
    }

    private String inheritDocument;

    @Basic
    @javax.persistence.Column(name = "INHERIT_DOCUMENT", nullable = true, insertable = true, updatable = true, length = 1)
    public String getInheritDocument() {
        return inheritDocument;
    }

    public void setInheritDocument(String inheritDocument) {
        this.inheritDocument = inheritDocument;
    }

    private String linkCbs;

    @Basic
    @javax.persistence.Column(name = "LINK_CBS", nullable = true, insertable = true, updatable = true, length = 120)
    public String getLinkCbs() {
        return linkCbs;
    }

    public void setLinkCbs(String linkCbs) {
        this.linkCbs = linkCbs;
    }

    private Timestamp effective;

    @Basic
    @javax.persistence.Column(name = "EFFECTIVE", nullable = true, insertable = true, updatable = true)
    public Timestamp getEffective() {
        return effective;
    }

    public void setEffective(Timestamp effective) {
        this.effective = effective;
    }

    private Timestamp expiry;

    @Basic
    @javax.persistence.Column(name = "EXPIRY", nullable = true, insertable = true, updatable = true)
    public Timestamp getExpiry() {
        return expiry;
    }

    public void setExpiry(Timestamp expiry) {
        this.expiry = expiry;
    }

    private Long pid;

    @Basic
    @javax.persistence.Column(name = "PID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    private Long inheritMenuAcl;

    @Basic
    @javax.persistence.Column(name = "INHERIT_MENU_ACL", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getInheritMenuAcl() {
        return inheritMenuAcl;
    }

    public void setInheritMenuAcl(Long inheritMenuAcl) {
        this.inheritMenuAcl = inheritMenuAcl;
    }

    private Long inheritParentAcl;

    @Basic
    @javax.persistence.Column(name = "INHERIT_PARENT_ACL", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getInheritParentAcl() {
        return inheritParentAcl;
    }

    public void setInheritParentAcl(Long inheritParentAcl) {
        this.inheritParentAcl = inheritParentAcl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductAudEntity that = (ProductAudEntity) o;

        if (allowCont != null ? !allowCont.equals(that.allowCont) : that.allowCont != null) return false;
        if (allowSub != null ? !allowSub.equals(that.allowSub) : that.allowSub != null) return false;
        if (approvalStatus != null ? !approvalStatus.equals(that.approvalStatus) : that.approvalStatus != null)
            return false;
        if (auditDate != null ? !auditDate.equals(that.auditDate) : that.auditDate != null) return false;
        if (auditFields != null ? !auditFields.equals(that.auditFields) : that.auditFields != null) return false;
        if (auditOperation != null ? !auditOperation.equals(that.auditOperation) : that.auditOperation != null)
            return false;
        if (effective != null ? !effective.equals(that.effective) : that.effective != null) return false;
        if (expiry != null ? !expiry.equals(that.expiry) : that.expiry != null) return false;
        if (inheritCondition != null ? !inheritCondition.equals(that.inheritCondition) : that.inheritCondition != null)
            return false;
        if (inheritDocument != null ? !inheritDocument.equals(that.inheritDocument) : that.inheritDocument != null)
            return false;
        if (inheritMenuAcl != null ? !inheritMenuAcl.equals(that.inheritMenuAcl) : that.inheritMenuAcl != null)
            return false;
        if (inheritParentAcl != null ? !inheritParentAcl.equals(that.inheritParentAcl) : that.inheritParentAcl != null)
            return false;
        if (linkCbs != null ? !linkCbs.equals(that.linkCbs) : that.linkCbs != null) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (pid != null ? !pid.equals(that.pid) : that.pid != null) return false;
        if (prevSysTranId != null ? !prevSysTranId.equals(that.prevSysTranId) : that.prevSysTranId != null)
            return false;
        if (productAudId != null ? !productAudId.equals(that.productAudId) : that.productAudId != null) return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;
        if (productOwner != null ? !productOwner.equals(that.productOwner) : that.productOwner != null) return false;
        if (productSlogan != null ? !productSlogan.equals(that.productSlogan) : that.productSlogan != null)
            return false;
        if (sysTranId != null ? !sysTranId.equals(that.sysTranId) : that.sysTranId != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productAudId != null ? productAudId.hashCode() : 0;
        result = 31 * result + (sysTranId != null ? sysTranId.hashCode() : 0);
        result = 31 * result + (auditDate != null ? auditDate.hashCode() : 0);
        result = 31 * result + (auditFields != null ? auditFields.hashCode() : 0);
        result = 31 * result + (auditOperation != null ? auditOperation.hashCode() : 0);
        result = 31 * result + (approvalStatus != null ? approvalStatus.hashCode() : 0);
        result = 31 * result + (prevSysTranId != null ? prevSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (productSlogan != null ? productSlogan.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (productOwner != null ? productOwner.hashCode() : 0);
        result = 31 * result + (allowSub != null ? allowSub.hashCode() : 0);
        result = 31 * result + (allowCont != null ? allowCont.hashCode() : 0);
        result = 31 * result + (inheritCondition != null ? inheritCondition.hashCode() : 0);
        result = 31 * result + (inheritDocument != null ? inheritDocument.hashCode() : 0);
        result = 31 * result + (linkCbs != null ? linkCbs.hashCode() : 0);
        result = 31 * result + (effective != null ? effective.hashCode() : 0);
        result = 31 * result + (expiry != null ? expiry.hashCode() : 0);
        result = 31 * result + (pid != null ? pid.hashCode() : 0);
        result = 31 * result + (inheritMenuAcl != null ? inheritMenuAcl.hashCode() : 0);
        result = 31 * result + (inheritParentAcl != null ? inheritParentAcl.hashCode() : 0);
        return result;
    }
}
