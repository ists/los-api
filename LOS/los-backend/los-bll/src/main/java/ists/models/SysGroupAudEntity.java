package ists.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_GROUP_AUD", schema = "ISTS", catalog = "")
public class SysGroupAudEntity {
    private Long sysGroupAudId;
    private Long sysTranId;
    private Timestamp auditDate;
    private String auditFields;
    private String auditOperation;
    private String approvalStatus;
    private Long prevSysTranId;
    private Long updSeq;
    private String acl;
    private String groupId;
    private String groupName;
    private String ldapId;
    private Long sid;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SysGroupAudSequence")
    @SequenceGenerator(name = "SysGroupAudSequence", sequenceName = "SQ_SYS_GROUP_AUD_ID", allocationSize = 1)
    @Column(name = "SYS_GROUP_AUD_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getSysGroupAudId() {
        return sysGroupAudId;
    }

    public void setSysGroupAudId(Long sysGroupAudId) {
        this.sysGroupAudId = sysGroupAudId;
    }

    @Basic
    @Column(name = "SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getSysTranId() {
        return sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    @Basic
    @Column(name = "AUDIT_DATE", nullable = true, insertable = true, updatable = true)
    public Timestamp getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    @Basic
    @Column(name = "AUDIT_FIELDS", nullable = true, insertable = true, updatable = true, length = 36)
    public String getAuditFields() {
        return auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    @Basic
    @Column(name = "AUDIT_OPERATION", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAuditOperation() {
        return auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    @Basic
    @Column(name = "APPROVAL_STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Basic
    @Column(name = "PREV_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPrevSysTranId() {
        return prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Basic
    @Column(name = "ACL", nullable = true, insertable = true, updatable = true, length = 50)
    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Basic
    @Column(name = "GROUP_ID", nullable = true, insertable = true, updatable = true, length = 20)
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "GROUP_NAME", nullable = true, insertable = true, updatable = true, length = 400)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Basic
    @Column(name = "LDAP_ID", nullable = true, insertable = true, updatable = true, length = 400)
    public String getLdapId() {
        return ldapId;
    }

    public void setLdapId(String ldapId) {
        this.ldapId = ldapId;
    }

    @Basic
    @Column(name = "SID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysGroupAudEntity that = (SysGroupAudEntity) o;

        if (acl != null ? !acl.equals(that.acl) : that.acl != null) return false;
        if (approvalStatus != null ? !approvalStatus.equals(that.approvalStatus) : that.approvalStatus != null)
            return false;
        if (auditDate != null ? !auditDate.equals(that.auditDate) : that.auditDate != null) return false;
        if (auditFields != null ? !auditFields.equals(that.auditFields) : that.auditFields != null) return false;
        if (auditOperation != null ? !auditOperation.equals(that.auditOperation) : that.auditOperation != null)
            return false;
        if (groupId != null ? !groupId.equals(that.groupId) : that.groupId != null) return false;
        if (groupName != null ? !groupName.equals(that.groupName) : that.groupName != null) return false;
        if (ldapId != null ? !ldapId.equals(that.ldapId) : that.ldapId != null) return false;
        if (prevSysTranId != null ? !prevSysTranId.equals(that.prevSysTranId) : that.prevSysTranId != null)
            return false;
        if (sid != null ? !sid.equals(that.sid) : that.sid != null) return false;
        if (sysGroupAudId != null ? !sysGroupAudId.equals(that.sysGroupAudId) : that.sysGroupAudId != null)
            return false;
        if (sysTranId != null ? !sysTranId.equals(that.sysTranId) : that.sysTranId != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = sysGroupAudId != null ? sysGroupAudId.hashCode() : 0;
        result = 31 * result + (sysTranId != null ? sysTranId.hashCode() : 0);
        result = 31 * result + (auditDate != null ? auditDate.hashCode() : 0);
        result = 31 * result + (auditFields != null ? auditFields.hashCode() : 0);
        result = 31 * result + (auditOperation != null ? auditOperation.hashCode() : 0);
        result = 31 * result + (approvalStatus != null ? approvalStatus.hashCode() : 0);
        result = 31 * result + (prevSysTranId != null ? prevSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (acl != null ? acl.hashCode() : 0);
        result = 31 * result + (groupId != null ? groupId.hashCode() : 0);
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        result = 31 * result + (ldapId != null ? ldapId.hashCode() : 0);
        result = 31 * result + (sid != null ? sid.hashCode() : 0);
        return result;
    }
}
