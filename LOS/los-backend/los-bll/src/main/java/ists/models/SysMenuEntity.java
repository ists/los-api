package ists.models;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_MENU", schema = "ISTS", catalog = "")
public class SysMenuEntity {
    private Long menuId;
    private String menuName;
    private Long parentId;
    private String url;
    private String menuRight;
    private Long pid;
    private Long heritableAcl;
    private Long lastSysTranId;
    private Long updSeq;
//    private SysMenuEntity sysMenuByParentId;
    private Collection<SysMenuEntity> sysMenusByMenuId;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SysMenuSequence")
    @SequenceGenerator(name = "SysMenuSequence", sequenceName = "SQ_MENU_ID", allocationSize = 1)
    @Column(name = "MENU_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    @Basic
    @Column(name = "MENU_NAME", nullable = true, insertable = true, updatable = true, length = 50)
    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Basic
    @Column(name = "PARENT_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "URL", nullable = true, insertable = true, updatable = true, length = 255)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "MENU_RIGHT", nullable = true, insertable = true, updatable = true, length = 50)
    public String getMenuRight() {
        return menuRight;
    }

    public void setMenuRight(String menuRight) {
        this.menuRight = menuRight;
    }

    @Basic
    @Column(name = "PID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    @Basic
    @Column(name = "HERITABLE_ACL", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getHeritableAcl() {
        return heritableAcl;
    }

    public void setHeritableAcl(Long heritableAcl) {
        this.heritableAcl = heritableAcl;
    }

    @Basic
    @Column(name = "LAST_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getLastSysTranId() {
        return lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysMenuEntity that = (SysMenuEntity) o;

        if (heritableAcl != null ? !heritableAcl.equals(that.heritableAcl) : that.heritableAcl != null) return false;
        if (lastSysTranId != null ? !lastSysTranId.equals(that.lastSysTranId) : that.lastSysTranId != null)
            return false;
        if (menuId != null ? !menuId.equals(that.menuId) : that.menuId != null) return false;
        if (menuName != null ? !menuName.equals(that.menuName) : that.menuName != null) return false;
        if (menuRight != null ? !menuRight.equals(that.menuRight) : that.menuRight != null) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (pid != null ? !pid.equals(that.pid) : that.pid != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = menuId != null ? menuId.hashCode() : 0;
        result = 31 * result + (menuName != null ? menuName.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (menuRight != null ? menuRight.hashCode() : 0);
        result = 31 * result + (pid != null ? pid.hashCode() : 0);
        result = 31 * result + (heritableAcl != null ? heritableAcl.hashCode() : 0);
        result = 31 * result + (lastSysTranId != null ? lastSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        return result;
    }

//    @ManyToOne
//    @JoinColumn(name = "PARENT_ID", referencedColumnName = "MENU_ID")
//    public SysMenuEntity getSysMenuByParentId() {
//        return sysMenuByParentId;
//    }
//
//    public void setSysMenuByParentId(SysMenuEntity sysMenuByParentId) {
//        this.sysMenuByParentId = sysMenuByParentId;
//    }

    @OneToMany(mappedBy = "sysMenuByParentId")
    public Collection<SysMenuEntity> getSysMenusByMenuId() {
        return sysMenusByMenuId;
    }

    public void setSysMenusByMenuId(Collection<SysMenuEntity> sysMenusByMenuId) {
        this.sysMenusByMenuId = sysMenusByMenuId;
    }
}
