package ists.models;

import javax.persistence.*;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_GROUP_MEMBER", schema = "ISTS", catalog = "")
@IdClass(SysGroupMemberEntityPK.class)
public class SysGroupMemberEntity {
    private Long groupSid;
    private Long memberSid;
    private Long updSeq;
    private Long lastSysTranId;
    private String acl;

    @Id
    @Column(name = "GROUP_SID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getGroupSid() {
        return groupSid;
    }

    public void setGroupSid(Long groupSid) {
        this.groupSid = groupSid;
    }

    @Id
    @Column(name = "MEMBER_SID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getMemberSid() {
        return memberSid;
    }

    public void setMemberSid(Long memberSid) {
        this.memberSid = memberSid;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Basic
    @Column(name = "LAST_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getLastSysTranId() {
        return lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    @Basic
    @Column(name = "ACL", nullable = true, insertable = true, updatable = true, length = 50)
    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysGroupMemberEntity that = (SysGroupMemberEntity) o;

        if (acl != null ? !acl.equals(that.acl) : that.acl != null) return false;
        if (groupSid != null ? !groupSid.equals(that.groupSid) : that.groupSid != null) return false;
        if (lastSysTranId != null ? !lastSysTranId.equals(that.lastSysTranId) : that.lastSysTranId != null)
            return false;
        if (memberSid != null ? !memberSid.equals(that.memberSid) : that.memberSid != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = groupSid != null ? groupSid.hashCode() : 0;
        result = 31 * result + (memberSid != null ? memberSid.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (lastSysTranId != null ? lastSysTranId.hashCode() : 0);
        result = 31 * result + (acl != null ? acl.hashCode() : 0);
        return result;
    }
}
