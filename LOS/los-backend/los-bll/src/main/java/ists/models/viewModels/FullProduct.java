package ists.models.viewModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import ists.models.ProductAudEntity;
import ists.models.ProductPropsEntity;

import java.util.Collection;

/**
 * Created by Administrator on 10/15/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FullProduct {
    public ProductAudEntity getProduct() {
        return product;
    }

    public void setProduct(ProductAudEntity product) {
        this.product = product;
    }

    public Collection<ProductPropsEntity> getProductUdfs() {
        return productUdfs;
    }

    public void setProductUdfs(Collection<ProductPropsEntity> productUdfs) {
        this.productUdfs = productUdfs;
    }

    public Collection<ProductDetailRight> getProductRights() {
        return productRights;
    }

    public void setProductRights(Collection<ProductDetailRight> productRights) {
        this.productRights = productRights;
    }

    private ProductAudEntity product;
    private Collection<ProductPropsEntity> productUdfs;
    private Collection<ProductDetailRight> productRights;
}
