package ists.models.viewModels;

/**
 * Created by Administrator on 10/14/2014.
 */
public class ProductRight {
    private String rightName;

    public ProductRight(String _rightName) {
        this.rightName = _rightName;
    }

    public String getRightName() {
        return rightName;
    }

    public void setRightName(String _rightName) {
        this.rightName = _rightName;
    }
}
