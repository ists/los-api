package ists.models.viewModels;


import ists.models.ProductEntity;

/**
 * Created by Administrator on 10/13/2014.
 */
public class ProductLite {

    private String productId;
    private String parentId;
    private String productName;

    public ProductLite(String productId, String parentId, String productName) {
        this.productId = productId;
        this.parentId = parentId;
        this.productName = productName;
    }

    public ProductLite(ProductEntity product) {
        this.productId = product.getProductId();
//        ProductEntity parent = product.getProductByParentId();
//        this.parentId = (parent==null)?null:parent.getProductId();
        this.parentId = (product.getParentId()==null)?null:product.getParentId();
        this.productName = product.getProductName();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
