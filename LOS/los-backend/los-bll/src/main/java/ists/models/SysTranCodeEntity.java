package ists.models;

import javax.persistence.*;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_TRAN_CODE", schema = "ISTS", catalog = "")
public class SysTranCodeEntity {
    private String tranCode;
    private String tranDesc;
    private Long menuId;

    @Id
    @Column(name = "TRAN_CODE", nullable = false, insertable = true, updatable = true, length = 30)
    public String getTranCode() {
        return tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    @Basic
    @Column(name = "TRAN_DESC", nullable = true, insertable = true, updatable = true, length = 100)
    public String getTranDesc() {
        return tranDesc;
    }

    public void setTranDesc(String tranDesc) {
        this.tranDesc = tranDesc;
    }

    @Basic
    @Column(name = "MENU_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysTranCodeEntity that = (SysTranCodeEntity) o;

        if (menuId != null ? !menuId.equals(that.menuId) : that.menuId != null) return false;
        if (tranCode != null ? !tranCode.equals(that.tranCode) : that.tranCode != null) return false;
        if (tranDesc != null ? !tranDesc.equals(that.tranDesc) : that.tranDesc != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tranCode != null ? tranCode.hashCode() : 0;
        result = 31 * result + (tranDesc != null ? tranDesc.hashCode() : 0);
        result = 31 * result + (menuId != null ? menuId.hashCode() : 0);
        return result;
    }
}
