package ists.models;

import javax.persistence.*;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_RIGHT", schema = "ISTS", catalog = "")
public class SysRightEntity {
    private Long rightId;
    private Long sid;
    private Long pid;
    private String acl;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SysRightSequence")
    @SequenceGenerator(name = "SysRightSequence", sequenceName = "SQ_SYS_RIGHT_ID", allocationSize = 1)
    @Column(name = "RIGHT_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getRightId() {
        return rightId;
    }

    public void setRightId(Long rightId) {
        this.rightId = rightId;
    }

    @Basic
    @Column(name = "SID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    @Basic
    @Column(name = "PID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    @Basic
    @Column(name = "ACL", nullable = true, insertable = true, updatable = true, length = 50)
    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRightEntity that = (SysRightEntity) o;

        if (acl != null ? !acl.equals(that.acl) : that.acl != null) return false;
        if (pid != null ? !pid.equals(that.pid) : that.pid != null) return false;
        if (rightId != null ? !rightId.equals(that.rightId) : that.rightId != null) return false;
        if (sid != null ? !sid.equals(that.sid) : that.sid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rightId != null ? rightId.hashCode() : 0;
        result = 31 * result + (sid != null ? sid.hashCode() : 0);
        result = 31 * result + (pid != null ? pid.hashCode() : 0);
        result = 31 * result + (acl != null ? acl.hashCode() : 0);
        return result;
    }
}
