package ists.models;

import javax.persistence.*;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_GROUP", schema = "ISTS", catalog = "")
public class SysGroupEntity {
    private String groupId;
    private String groupName;
    private Long updSeq;
    private Long lastSysTranId;
    private String ldapId;
    private Long sid;
    private String acl;

    @Id
    @Column(name = "GROUP_ID", nullable = false, insertable = true, updatable = true, length = 20)
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "GROUP_NAME", nullable = true, insertable = true, updatable = true, length = 100)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Basic
    @Column(name = "LAST_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getLastSysTranId() {
        return lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    @Basic
    @Column(name = "LDAP_ID", nullable = true, insertable = true, updatable = true, length = 100)
    public String getLdapId() {
        return ldapId;
    }

    public void setLdapId(String ldapId) {
        this.ldapId = ldapId;
    }

    @Basic
    @Column(name = "SID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    @Basic
    @Column(name = "ACL", nullable = true, insertable = true, updatable = true, length = 50)
    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysGroupEntity that = (SysGroupEntity) o;

        if (acl != null ? !acl.equals(that.acl) : that.acl != null) return false;
        if (groupId != null ? !groupId.equals(that.groupId) : that.groupId != null) return false;
        if (groupName != null ? !groupName.equals(that.groupName) : that.groupName != null) return false;
        if (lastSysTranId != null ? !lastSysTranId.equals(that.lastSysTranId) : that.lastSysTranId != null)
            return false;
        if (ldapId != null ? !ldapId.equals(that.ldapId) : that.ldapId != null) return false;
        if (sid != null ? !sid.equals(that.sid) : that.sid != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = groupId != null ? groupId.hashCode() : 0;
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (lastSysTranId != null ? lastSysTranId.hashCode() : 0);
        result = 31 * result + (ldapId != null ? ldapId.hashCode() : 0);
        result = 31 * result + (sid != null ? sid.hashCode() : 0);
        result = 31 * result + (acl != null ? acl.hashCode() : 0);
        return result;
    }
}
