package ists.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_USER", schema = "ISTS", catalog = "")
public class SysUserEntity {
    private String userId;
    private String userName;
    private String deptId;
    private String passCode;
    private String nodeId;
    private String status;
    private Timestamp effective;
    private Timestamp lastLogin;
    private Timestamp expiry;
    private Long tryCount;
    private Long updSeq;
    private Long lastSysTranId;
    private Long maxConnections;
    private Long userLevel;
    private Long sid;
    private String ldapId;
    private String acl;
    private Long autoAuthorized;

    @Id
    @Column(name = "USER_ID", nullable = false, insertable = true, updatable = true, length = 20)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "USER_NAME", nullable = true, insertable = true, updatable = true, length = 50)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "DEPT_ID", nullable = true, insertable = true, updatable = true, length = 5)
    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    @Basic
    @Column(name = "PASS_CODE", nullable = true, insertable = true, updatable = true, length = 50)
    public String getPassCode() {
        return passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    @Basic
    @Column(name = "NODE_ID", nullable = true, insertable = true, updatable = true, length = 20)
    public String getNodeId() {
        return nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "EFFECTIVE", nullable = true, insertable = true, updatable = true)
    public Timestamp getEffective() {
        return effective;
    }

    public void setEffective(Timestamp effective) {
        this.effective = effective;
    }

    @Basic
    @Column(name = "LAST_LOGIN", nullable = true, insertable = true, updatable = true)
    public Timestamp getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Timestamp lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Basic
    @Column(name = "EXPIRY", nullable = true, insertable = true, updatable = true)
    public Timestamp getExpiry() {
        return expiry;
    }

    public void setExpiry(Timestamp expiry) {
        this.expiry = expiry;
    }

    @Basic
    @Column(name = "TRY_COUNT", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getTryCount() {
        return tryCount;
    }

    public void setTryCount(Long tryCount) {
        this.tryCount = tryCount;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Basic
    @Column(name = "LAST_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getLastSysTranId() {
        return lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    @Basic
    @Column(name = "MAX_CONNECTIONS", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getMaxConnections() {
        return maxConnections;
    }

    public void setMaxConnections(Long maxConnections) {
        this.maxConnections = maxConnections;
    }

    @Basic
    @Column(name = "USER_LEVEL", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Long userLevel) {
        this.userLevel = userLevel;
    }

    @Basic
    @Column(name = "SID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getSid() {
        return sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    @Basic
    @Column(name = "LDAP_ID", nullable = true, insertable = true, updatable = true, length = 100)
    public String getLdapId() {
        return ldapId;
    }

    public void setLdapId(String ldapId) {
        this.ldapId = ldapId;
    }

    @Basic
    @Column(name = "ACL", nullable = true, insertable = true, updatable = true, length = 50)
    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Basic
    @Column(name = "AUTO_AUTHORIZED", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getAutoAuthorized() {
        return autoAuthorized;
    }

    public void setAutoAuthorized(Long autoAuthorized) {
        this.autoAuthorized = autoAuthorized;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUserEntity that = (SysUserEntity) o;

        if (acl != null ? !acl.equals(that.acl) : that.acl != null) return false;
        if (autoAuthorized != null ? !autoAuthorized.equals(that.autoAuthorized) : that.autoAuthorized != null)
            return false;
        if (deptId != null ? !deptId.equals(that.deptId) : that.deptId != null) return false;
        if (effective != null ? !effective.equals(that.effective) : that.effective != null) return false;
        if (expiry != null ? !expiry.equals(that.expiry) : that.expiry != null) return false;
        if (lastLogin != null ? !lastLogin.equals(that.lastLogin) : that.lastLogin != null) return false;
        if (lastSysTranId != null ? !lastSysTranId.equals(that.lastSysTranId) : that.lastSysTranId != null)
            return false;
        if (ldapId != null ? !ldapId.equals(that.ldapId) : that.ldapId != null) return false;
        if (maxConnections != null ? !maxConnections.equals(that.maxConnections) : that.maxConnections != null)
            return false;
        if (nodeId != null ? !nodeId.equals(that.nodeId) : that.nodeId != null) return false;
        if (passCode != null ? !passCode.equals(that.passCode) : that.passCode != null) return false;
        if (sid != null ? !sid.equals(that.sid) : that.sid != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (tryCount != null ? !tryCount.equals(that.tryCount) : that.tryCount != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (userLevel != null ? !userLevel.equals(that.userLevel) : that.userLevel != null) return false;
        if (userName != null ? !userName.equals(that.userName) : that.userName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId != null ? userId.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (deptId != null ? deptId.hashCode() : 0);
        result = 31 * result + (passCode != null ? passCode.hashCode() : 0);
        result = 31 * result + (nodeId != null ? nodeId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (effective != null ? effective.hashCode() : 0);
        result = 31 * result + (lastLogin != null ? lastLogin.hashCode() : 0);
        result = 31 * result + (expiry != null ? expiry.hashCode() : 0);
        result = 31 * result + (tryCount != null ? tryCount.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (lastSysTranId != null ? lastSysTranId.hashCode() : 0);
        result = 31 * result + (maxConnections != null ? maxConnections.hashCode() : 0);
        result = 31 * result + (userLevel != null ? userLevel.hashCode() : 0);
        result = 31 * result + (sid != null ? sid.hashCode() : 0);
        result = 31 * result + (ldapId != null ? ldapId.hashCode() : 0);
        result = 31 * result + (acl != null ? acl.hashCode() : 0);
        result = 31 * result + (autoAuthorized != null ? autoAuthorized.hashCode() : 0);
        return result;
    }
}
