package ists.models.viewModels;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/17/2014.
 */
public class ProductAudLite {

    private Long productAudId;
    private Timestamp auditDate;
    private String productName;
    private String approvalStatus;
    private String userId;

    public ProductAudLite() {
    }

    public ProductAudLite(Long productAudId, Timestamp auditDate, String productName, String approvalStatus, String userId) {
        this.productAudId = productAudId;
        this.auditDate = auditDate;
        this.productName = productName;
        this.approvalStatus = approvalStatus;
        this.userId = userId;
    }

    @Id
    @javax.persistence.Column(name = "PRODUCT_AUD_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getProductAudId() {
        return productAudId;
    }

    public void setProductAudId(Long productAudId) {
        this.productAudId = productAudId;
    }

    @Basic
    @javax.persistence.Column(name = "AUDIT_DATE", nullable = true, insertable = true, updatable = true)
    public Timestamp getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    @Basic
    @javax.persistence.Column(name = "PRODUCT_NAME", nullable = true, insertable = true, updatable = true, length = 200)
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Basic
    @javax.persistence.Column(name = "APPROVAL_STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Basic
    @Column(name = "USER_ID", nullable = false, insertable = true, updatable = true, length = 20)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
