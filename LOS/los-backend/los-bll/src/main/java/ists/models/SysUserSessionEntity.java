package ists.models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "SYS_USER_SESSION", schema = "ISTS", catalog = "")
public class SysUserSessionEntity {
    private Long userSessionId;
    private Timestamp logonTime;
    private Timestamp logoffTime;
    private String userId;
    private String sessionKey;
    private String langId;
    private String status;
//    private Collection<SysTransactionEntity> sysTransactionsByUserSessionId;

    @Id
    @Column(name = "USER_SESSION_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(Long userSessionId) {
        this.userSessionId = userSessionId;
    }

    @Basic
    @Column(name = "LOGON_TIME", nullable = true, insertable = true, updatable = true)
    public Timestamp getLogonTime() {
        return logonTime;
    }

    public void setLogonTime(Timestamp logonTime) {
        this.logonTime = logonTime;
    }

    @Basic
    @Column(name = "LOGOFF_TIME", nullable = true, insertable = true, updatable = true)
    public Timestamp getLogoffTime() {
        return logoffTime;
    }

    public void setLogoffTime(Timestamp logoffTime) {
        this.logoffTime = logoffTime;
    }

    @Basic
    @Column(name = "USER_ID", nullable = true, insertable = true, updatable = true, length = 20)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "SESSION_KEY", nullable = true, insertable = true, updatable = true, length = 50)
    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    @Basic
    @Column(name = "LANG_ID", nullable = true, insertable = true, updatable = true, length = 5)
    public String getLangId() {
        return langId;
    }

    public void setLangId(String langId) {
        this.langId = langId;
    }

    @Basic
    @Column(name = "STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUserSessionEntity that = (SysUserSessionEntity) o;

        if (langId != null ? !langId.equals(that.langId) : that.langId != null) return false;
        if (logoffTime != null ? !logoffTime.equals(that.logoffTime) : that.logoffTime != null) return false;
        if (logonTime != null ? !logonTime.equals(that.logonTime) : that.logonTime != null) return false;
        if (sessionKey != null ? !sessionKey.equals(that.sessionKey) : that.sessionKey != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (userSessionId != null ? !userSessionId.equals(that.userSessionId) : that.userSessionId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userSessionId != null ? userSessionId.hashCode() : 0;
        result = 31 * result + (logonTime != null ? logonTime.hashCode() : 0);
        result = 31 * result + (logoffTime != null ? logoffTime.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (sessionKey != null ? sessionKey.hashCode() : 0);
        result = 31 * result + (langId != null ? langId.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "sysUserSessionByUserSessionId")
//    public Collection<SysTransactionEntity> getSysTransactionsByUserSessionId() {
//        return sysTransactionsByUserSessionId;
//    }
//
//    public void setSysTransactionsByUserSessionId(Collection<SysTransactionEntity> sysTransactionsByUserSessionId) {
//        this.sysTransactionsByUserSessionId = sysTransactionsByUserSessionId;
//    }
}
