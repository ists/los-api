package ists.models.viewModels;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductAudViewModel {
    private Long productAudId;

    public Long getProductAudId() {
        return productAudId;
    }
    public void setProductAudId(Long productAudId) {
        this.productAudId = productAudId;
    }
    private Long sysTranId;
    public Long getSysTranId() {
        return sysTranId;
    }
    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }
    private Timestamp auditDate;
    public Timestamp getAuditDate() {
        return auditDate;
    }
    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }
    private String auditFields;
    public String getAuditFields() {
        return auditFields;
    }
    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }
    private String auditOperation;
    public String getAuditOperation() {
        return auditOperation;
    }
    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }
    private String approvalStatus;
    public String getApprovalStatus() {
        return approvalStatus;
    }
    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }
    private Long prevSysTranId;
    public Long getPrevSysTranId() {
        return prevSysTranId;
    }
    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }
    private Long updSeq;
    public Long getUpdSeq() {
        return updSeq;
    }
    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }
    private String productId;
    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }
    private String productName;
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    private String productSlogan;
    public String getProductSlogan() {
        return productSlogan;
    }
    public void setProductSlogan(String productSlogan) {
        this.productSlogan = productSlogan;
    }
    private String parentId;
    public String getParentId() {
        return parentId;
    }
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    private Long productOwner;
    public Long getProductOwner() {
        return productOwner;
    }
    public void setProductOwner(Long productOwner) {
        this.productOwner = productOwner;
    }
    private String allowSub;
    public String getAllowSub() {
        return allowSub;
    }
    public void setAllowSub(String allowSub) {
        this.allowSub = allowSub;
    }
    private String allowCont;
    public String getAllowCont() {
        return allowCont;
    }
    public void setAllowCont(String allowCont) {
        this.allowCont = allowCont;
    }
    private String inheritCondition;
    public String getInheritCondition() {
        return inheritCondition;
    }
    public void setInheritCondition(String inheritCondition) {
        this.inheritCondition = inheritCondition;
    }
    private String inheritDocument;
    public String getInheritDocument() {
        return inheritDocument;
    }
    public void setInheritDocument(String inheritDocument) {
        this.inheritDocument = inheritDocument;
    }
    private String linkCbs;
    public String getLinkCbs() {
        return linkCbs;
    }
    public void setLinkCbs(String linkCbs) {
        this.linkCbs = linkCbs;
    }
    private Long inheritMenuAcl;
    public Long getInheritMenuAcl() {
        return inheritMenuAcl;
    }
    public void setInheritMenuAcl(Long inheritMenuAcl) {
        this.inheritMenuAcl = inheritMenuAcl;
    }
    private Long inheritParentAcl;
    public Long getInheritParentAcl() {
        return inheritParentAcl;
    }
    public void setInheritParentAcl(Long inheritParentAcl) {
        this.inheritParentAcl = inheritParentAcl;
    }

    private ProductAudViewModel productByParentId;
    public ProductAudViewModel getProductByParentId() {
        return productByParentId;
    }

    public void setProductByParentId(ProductAudViewModel productByParentId) {
        this.productByParentId = productByParentId;
    }
}
