package ists.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@javax.persistence.Table(name = "UDTM_FIELDS", schema = "ISTS", catalog = "")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdtmFieldsEntity {
    private Long udfId;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UdtmFieldsSequence")
    @SequenceGenerator(name = "UdtmFieldsSequence", sequenceName = "SQ_UDF_ID", allocationSize = 1)
    @javax.persistence.Column(name = "UDF_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getUdfId() {
        return udfId;
    }

    public void setUdfId(Long udfId) {
        this.udfId = udfId;
    }

    private String fieldGroupId;

    @Basic
    @javax.persistence.Column(name = "FIELD_GROUP_ID", nullable = true, insertable = true, updatable = true, length = 105)
    public String getFieldGroupId() {
        return fieldGroupId;
    }

    public void setFieldGroupId(String fieldGroupId) {
        this.fieldGroupId = fieldGroupId;
    }

    private String fieldName;

    @Basic
    @javax.persistence.Column(name = "FIELD_NAME", nullable = true, insertable = true, updatable = true, length = 105)
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    private String fieldDescription;

    @Basic
    @javax.persistence.Column(name = "FIELD_DESCRIPTION", nullable = true, insertable = true, updatable = true, length = 500)
    public String getFieldDescription() {
        return fieldDescription;
    }

    public void setFieldDescription(String fieldDescription) {
        this.fieldDescription = fieldDescription;
    }

    private String fieldType;

    @Basic
    @javax.persistence.Column(name = "FIELD_TYPE", nullable = true, insertable = true, updatable = true, length = 1)
    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    private String valType;

    @Basic
    @javax.persistence.Column(name = "VAL_TYPE", nullable = true, insertable = true, updatable = true, length = 3)
    public String getValType() {
        return valType;
    }

    public void setValType(String valType) {
        this.valType = valType;
    }

    private String incExcFlag;

    @Basic
    @javax.persistence.Column(name = "INC_EXC_FLAG", nullable = true, insertable = true, updatable = true, length = 1)
    public String getIncExcFlag() {
        return incExcFlag;
    }

    public void setIncExcFlag(String incExcFlag) {
        this.incExcFlag = incExcFlag;
    }

    private String mandatory;

    @Basic
    @javax.persistence.Column(name = "MANDATORY", nullable = true, insertable = true, updatable = true, length = 1)
    public String getMandatory() {
        return mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    private Long minVal;

    @Basic
    @javax.persistence.Column(name = "MIN_VAL", nullable = true, insertable = true, updatable = true, precision = 3)
    public Long getMinVal() {
        return minVal;
    }

    public void setMinVal(Long minVal) {
        this.minVal = minVal;
    }

    private Long maxVal;

    @Basic
    @javax.persistence.Column(name = "MAX_VAL", nullable = true, insertable = true, updatable = true, precision = 3)
    public Long getMaxVal() {
        return maxVal;
    }

    public void setMaxVal(Long maxVal) {
        this.maxVal = maxVal;
    }

    private String mask;

    @Basic
    @javax.persistence.Column(name = "MASK", nullable = true, insertable = true, updatable = true, length = 25)
    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    private Long minLength;

    @Basic
    @javax.persistence.Column(name = "MIN_LENGTH", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getMinLength() {
        return minLength;
    }

    public void setMinLength(Long minLength) {
        this.minLength = minLength;
    }

    private Long maxLength;

    @Basic
    @javax.persistence.Column(name = "MAX_LENGTH", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Long maxLength) {
        this.maxLength = maxLength;
    }

    private String entityName;

    @Basic
    @javax.persistence.Column(name = "ENTITY_NAME", nullable = true, insertable = true, updatable = true, length = 30)
    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    private String entityDescription;

    @Basic
    @javax.persistence.Column(name = "ENTITY_DESCRIPTION", nullable = true, insertable = true, updatable = true, length = 30)
    public String getEntityDescription() {
        return entityDescription;
    }

    public void setEntityDescription(String entityDescription) {
        this.entityDescription = entityDescription;
    }

    private String entityTable;

    @Basic
    @javax.persistence.Column(name = "ENTITY_TABLE", nullable = true, insertable = true, updatable = true, length = 30)
    public String getEntityTable() {
        return entityTable;
    }

    public void setEntityTable(String entityTable) {
        this.entityTable = entityTable;
    }

    private String entityWhereClause;

    @Basic
    @javax.persistence.Column(name = "ENTITY_WHERE_CLAUSE", nullable = true, insertable = true, updatable = true, length = 1000)
    public String getEntityWhereClause() {
        return entityWhereClause;
    }

    public void setEntityWhereClause(String entityWhereClause) {
        this.entityWhereClause = entityWhereClause;
    }

    private String recordStat;

    @Basic
    @javax.persistence.Column(name = "RECORD_STAT", nullable = true, insertable = true, updatable = true, length = 1)
    public String getRecordStat() {
        return recordStat;
    }

    public void setRecordStat(String recordStat) {
        this.recordStat = recordStat;
    }

    private String authStat;

    @Basic
    @javax.persistence.Column(name = "AUTH_STAT", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAuthStat() {
        return authStat;
    }

    public void setAuthStat(String authStat) {
        this.authStat = authStat;
    }

    private String uniqueField;

    @Basic
    @javax.persistence.Column(name = "UNIQUE_FIELD", nullable = true, insertable = true, updatable = true, length = 1)
    public String getUniqueField() {
        return uniqueField;
    }

    public void setUniqueField(String uniqueField) {
        this.uniqueField = uniqueField;
    }

    private String defaultValue;

    @Basic
    @javax.persistence.Column(name = "DEFAULT_VALUE", nullable = true, insertable = true, updatable = true, length = 150)
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    private String drvRuleType;

    @Basic
    @javax.persistence.Column(name = "DRV_RULE_TYPE", nullable = true, insertable = true, updatable = true, length = 4)
    public String getDrvRuleType() {
        return drvRuleType;
    }

    public void setDrvRuleType(String drvRuleType) {
        this.drvRuleType = drvRuleType;
    }

    private String drvRule;

    @Basic
    @javax.persistence.Column(name = "DRV_RULE", nullable = true, insertable = true, updatable = true, length = 4000)
    public String getDrvRule() {
        return drvRule;
    }

    public void setDrvRule(String drvRule) {
        this.drvRule = drvRule;
    }

    private String valRuleType;

    @Basic
    @javax.persistence.Column(name = "VAL_RULE_TYPE", nullable = true, insertable = true, updatable = true, length = 1)
    public String getValRuleType() {
        return valRuleType;
    }

    public void setValRuleType(String valRuleType) {
        this.valRuleType = valRuleType;
    }

    private String valRule;

    @Basic
    @javax.persistence.Column(name = "VAL_RULE", nullable = true, insertable = true, updatable = true, length = 4000)
    public String getValRule() {
        return valRule;
    }

    public void setValRule(String valRule) {
        this.valRule = valRule;
    }

    private String updateAllowed;

    @Basic
    @javax.persistence.Column(name = "UPDATE_ALLOWED", nullable = true, insertable = true, updatable = true, length = 1)
    public String getUpdateAllowed() {
        return updateAllowed;
    }

    public void setUpdateAllowed(String updateAllowed) {
        this.updateAllowed = updateAllowed;
    }

    private Long lastSysTranId;

    @Basic
    @javax.persistence.Column(name = "LAST_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getLastSysTranId() {
        return lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    private Long updSeq;

    @Basic
    @javax.persistence.Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    private String acl;

    @Basic
    @javax.persistence.Column(name = "ACL", nullable = true, insertable = true, updatable = true, length = 50)
    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UdtmFieldsEntity that = (UdtmFieldsEntity) o;

        if (acl != null ? !acl.equals(that.acl) : that.acl != null) return false;
        if (authStat != null ? !authStat.equals(that.authStat) : that.authStat != null) return false;
        if (defaultValue != null ? !defaultValue.equals(that.defaultValue) : that.defaultValue != null) return false;
        if (drvRule != null ? !drvRule.equals(that.drvRule) : that.drvRule != null) return false;
        if (drvRuleType != null ? !drvRuleType.equals(that.drvRuleType) : that.drvRuleType != null) return false;
        if (entityDescription != null ? !entityDescription.equals(that.entityDescription) : that.entityDescription != null)
            return false;
        if (entityName != null ? !entityName.equals(that.entityName) : that.entityName != null) return false;
        if (entityTable != null ? !entityTable.equals(that.entityTable) : that.entityTable != null) return false;
        if (entityWhereClause != null ? !entityWhereClause.equals(that.entityWhereClause) : that.entityWhereClause != null)
            return false;
        if (fieldDescription != null ? !fieldDescription.equals(that.fieldDescription) : that.fieldDescription != null)
            return false;
        if (fieldGroupId != null ? !fieldGroupId.equals(that.fieldGroupId) : that.fieldGroupId != null) return false;
        if (fieldName != null ? !fieldName.equals(that.fieldName) : that.fieldName != null) return false;
        if (fieldType != null ? !fieldType.equals(that.fieldType) : that.fieldType != null) return false;
        if (incExcFlag != null ? !incExcFlag.equals(that.incExcFlag) : that.incExcFlag != null) return false;
        if (lastSysTranId != null ? !lastSysTranId.equals(that.lastSysTranId) : that.lastSysTranId != null)
            return false;
        if (mandatory != null ? !mandatory.equals(that.mandatory) : that.mandatory != null) return false;
        if (mask != null ? !mask.equals(that.mask) : that.mask != null) return false;
        if (maxLength != null ? !maxLength.equals(that.maxLength) : that.maxLength != null) return false;
        if (maxVal != null ? !maxVal.equals(that.maxVal) : that.maxVal != null) return false;
        if (minLength != null ? !minLength.equals(that.minLength) : that.minLength != null) return false;
        if (minVal != null ? !minVal.equals(that.minVal) : that.minVal != null) return false;
        if (recordStat != null ? !recordStat.equals(that.recordStat) : that.recordStat != null) return false;
        if (udfId != null ? !udfId.equals(that.udfId) : that.udfId != null) return false;
        if (uniqueField != null ? !uniqueField.equals(that.uniqueField) : that.uniqueField != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;
        if (updateAllowed != null ? !updateAllowed.equals(that.updateAllowed) : that.updateAllowed != null)
            return false;
        if (valRule != null ? !valRule.equals(that.valRule) : that.valRule != null) return false;
        if (valRuleType != null ? !valRuleType.equals(that.valRuleType) : that.valRuleType != null) return false;
        if (valType != null ? !valType.equals(that.valType) : that.valType != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = udfId != null ? udfId.hashCode() : 0;
        result = 31 * result + (fieldGroupId != null ? fieldGroupId.hashCode() : 0);
        result = 31 * result + (fieldName != null ? fieldName.hashCode() : 0);
        result = 31 * result + (fieldDescription != null ? fieldDescription.hashCode() : 0);
        result = 31 * result + (fieldType != null ? fieldType.hashCode() : 0);
        result = 31 * result + (valType != null ? valType.hashCode() : 0);
        result = 31 * result + (incExcFlag != null ? incExcFlag.hashCode() : 0);
        result = 31 * result + (mandatory != null ? mandatory.hashCode() : 0);
        result = 31 * result + (minVal != null ? minVal.hashCode() : 0);
        result = 31 * result + (maxVal != null ? maxVal.hashCode() : 0);
        result = 31 * result + (mask != null ? mask.hashCode() : 0);
        result = 31 * result + (minLength != null ? minLength.hashCode() : 0);
        result = 31 * result + (maxLength != null ? maxLength.hashCode() : 0);
        result = 31 * result + (entityName != null ? entityName.hashCode() : 0);
        result = 31 * result + (entityDescription != null ? entityDescription.hashCode() : 0);
        result = 31 * result + (entityTable != null ? entityTable.hashCode() : 0);
        result = 31 * result + (entityWhereClause != null ? entityWhereClause.hashCode() : 0);
        result = 31 * result + (recordStat != null ? recordStat.hashCode() : 0);
        result = 31 * result + (authStat != null ? authStat.hashCode() : 0);
        result = 31 * result + (uniqueField != null ? uniqueField.hashCode() : 0);
        result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
        result = 31 * result + (drvRuleType != null ? drvRuleType.hashCode() : 0);
        result = 31 * result + (drvRule != null ? drvRule.hashCode() : 0);
        result = 31 * result + (valRuleType != null ? valRuleType.hashCode() : 0);
        result = 31 * result + (valRule != null ? valRule.hashCode() : 0);
        result = 31 * result + (updateAllowed != null ? updateAllowed.hashCode() : 0);
        result = 31 * result + (lastSysTranId != null ? lastSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (acl != null ? acl.hashCode() : 0);
        return result;
    }
}
