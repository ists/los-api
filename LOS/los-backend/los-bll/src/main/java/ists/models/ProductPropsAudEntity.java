package ists.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "PRODUCT_PROPS_AUD", schema = "ISTS", catalog = "")
public class ProductPropsAudEntity {
    private Long productPropsAudId;
    private Long sysTranId;
    private Timestamp auditDate;
    private String auditFields;
    private String auditOperation;
    private String approvalStatus;
    private Long prevSysTranId;
    private Long updSeq;
    private String productId;
    private Long udfId;
    private String udfValue;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ProductPropsAudSequence")
    @SequenceGenerator(name = "ProductPropsAudSequence", sequenceName = "SQ_PRODUCT_PROPS_AUD_ID", allocationSize = 1)
    @Column(name = "PRODUCT_PROPS_AUD_ID", nullable = false, insertable = true, updatable = true, precision = -127)
    public Long getProductPropsAudId() {
        return productPropsAudId;
    }

    public void setProductPropsAudId(Long productPropsAudId) {
        this.productPropsAudId = productPropsAudId;
    }

    @Basic
    @Column(name = "SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getSysTranId() {
        return sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    @Basic
    @Column(name = "AUDIT_DATE", nullable = true, insertable = true, updatable = true)
    public Timestamp getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    @Basic
    @Column(name = "AUDIT_FIELDS", nullable = true, insertable = true, updatable = true, length = 28)
    public String getAuditFields() {
        return auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    @Basic
    @Column(name = "AUDIT_OPERATION", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAuditOperation() {
        return auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    @Basic
    @Column(name = "APPROVAL_STATUS", nullable = true, insertable = true, updatable = true, length = 1)
    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Basic
    @Column(name = "PREV_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPrevSysTranId() {
        return prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Basic
    @Column(name = "PRODUCT_ID", nullable = true, insertable = true, updatable = true, length = 20)
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "UDF_ID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUdfId() {
        return udfId;
    }

    public void setUdfId(Long udfId) {
        this.udfId = udfId;
    }

    @Basic
    @Column(name = "UDF_VALUE", nullable = true, insertable = true, updatable = true, length = 400)
    public String getUdfValue() {
        return udfValue;
    }

    public void setUdfValue(String udfValue) {
        this.udfValue = udfValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductPropsAudEntity that = (ProductPropsAudEntity) o;

        if (approvalStatus != null ? !approvalStatus.equals(that.approvalStatus) : that.approvalStatus != null)
            return false;
        if (auditDate != null ? !auditDate.equals(that.auditDate) : that.auditDate != null) return false;
        if (auditFields != null ? !auditFields.equals(that.auditFields) : that.auditFields != null) return false;
        if (auditOperation != null ? !auditOperation.equals(that.auditOperation) : that.auditOperation != null)
            return false;
        if (prevSysTranId != null ? !prevSysTranId.equals(that.prevSysTranId) : that.prevSysTranId != null)
            return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (productPropsAudId != null ? !productPropsAudId.equals(that.productPropsAudId) : that.productPropsAudId != null)
            return false;
        if (sysTranId != null ? !sysTranId.equals(that.sysTranId) : that.sysTranId != null) return false;
        if (udfId != null ? !udfId.equals(that.udfId) : that.udfId != null) return false;
        if (udfValue != null ? !udfValue.equals(that.udfValue) : that.udfValue != null) return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productPropsAudId != null ? productPropsAudId.hashCode() : 0;
        result = 31 * result + (sysTranId != null ? sysTranId.hashCode() : 0);
        result = 31 * result + (auditDate != null ? auditDate.hashCode() : 0);
        result = 31 * result + (auditFields != null ? auditFields.hashCode() : 0);
        result = 31 * result + (auditOperation != null ? auditOperation.hashCode() : 0);
        result = 31 * result + (approvalStatus != null ? approvalStatus.hashCode() : 0);
        result = 31 * result + (prevSysTranId != null ? prevSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (udfId != null ? udfId.hashCode() : 0);
        result = 31 * result + (udfValue != null ? udfValue.hashCode() : 0);
        return result;
    }
}
