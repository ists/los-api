package ists.models;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by Administrator on 10/10/2014.
 */
@Entity
@Table(name = "PRODUCT", schema = "ISTS", catalog = "")
public class ProductEntity {
    private String productId;
    private String productName;
    private String productSlogan;
    private String parentId;
    private Long productOwner;
    private String allowSub;
    private String allowCont;
    private String inheritCondition;
    private String inheritDocument;
    private String linkCbs;
    private Timestamp effective;
    private Timestamp expiry;
    private Long pid;
    private Long inheritMenuAcl;
    private Long inheritParentAcl;
    private Long lastSysTranId;
    private Long updSeq;
//    private ProductEntity productByParentId;
    private Collection<ProductEntity> productsByProductId;
    private Collection<ProductPropsEntity> productPropsesByProductId;

    @Id
    @Column(name = "PRODUCT_ID", nullable = false, insertable = true, updatable = true, length = 20)
    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Basic
    @Column(name = "PRODUCT_NAME", nullable = true, insertable = true, updatable = true, length = 50)
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Basic
    @Column(name = "PRODUCT_SLOGAN", nullable = true, insertable = true, updatable = true, length = 100)
    public String getProductSlogan() {
        return productSlogan;
    }

    public void setProductSlogan(String productSlogan) {
        this.productSlogan = productSlogan;
    }

    @Basic
    @Column(name = "PARENT_ID", nullable = true, insertable = true, updatable = true, length = 20)
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "PRODUCT_OWNER", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getProductOwner() {
        return productOwner;
    }

    public void setProductOwner(Long productOwner) {
        this.productOwner = productOwner;
    }

    @Basic
    @Column(name = "ALLOW_SUB", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAllowSub() {
        return allowSub;
    }

    public void setAllowSub(String allowSub) {
        this.allowSub = allowSub;
    }

    @Basic
    @Column(name = "ALLOW_CONT", nullable = true, insertable = true, updatable = true, length = 1)
    public String getAllowCont() {
        return allowCont;
    }

    public void setAllowCont(String allowCont) {
        this.allowCont = allowCont;
    }

    @Basic
    @Column(name = "INHERIT_CONDITION", nullable = true, insertable = true, updatable = true, length = 1)
    public String getInheritCondition() {
        return inheritCondition;
    }

    public void setInheritCondition(String inheritCondition) {
        this.inheritCondition = inheritCondition;
    }

    @Basic
    @Column(name = "INHERIT_DOCUMENT", nullable = true, insertable = true, updatable = true, length = 1)
    public String getInheritDocument() {
        return inheritDocument;
    }

    public void setInheritDocument(String inheritDocument) {
        this.inheritDocument = inheritDocument;
    }

    @Basic
    @Column(name = "LINK_CBS", nullable = true, insertable = true, updatable = true, length = 30)
    public String getLinkCbs() {
        return linkCbs;
    }

    public void setLinkCbs(String linkCbs) {
        this.linkCbs = linkCbs;
    }

    @Basic
    @javax.persistence.Column(name = "EFFECTIVE", nullable = true, insertable = true, updatable = true)
    public Timestamp getEffective() {
        return effective;
    }

    public void setEffective(Timestamp effective) {
        this.effective = effective;
    }

    @Basic
    @javax.persistence.Column(name = "EXPIRY", nullable = true, insertable = true, updatable = true)
    public Timestamp getExpiry() {
        return expiry;
    }

    public void setExpiry(Timestamp expiry) {
        this.expiry = expiry;
    }

    @Basic
    @Column(name = "PID", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    @Basic
    @Column(name = "INHERIT_MENU_ACL", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getInheritMenuAcl() {
        return inheritMenuAcl;
    }

    public void setInheritMenuAcl(Long inheritMenuAcl) {
        this.inheritMenuAcl = inheritMenuAcl;
    }

    @Basic
    @Column(name = "INHERIT_PARENT_ACL", nullable = true, insertable = true, updatable = true, precision = -127)
    public Long getInheritParentAcl() {
        return inheritParentAcl;
    }

    public void setInheritParentAcl(Long inheritParentAcl) {
        this.inheritParentAcl = inheritParentAcl;
    }

    @Basic
    @Column(name = "LAST_SYS_TRAN_ID", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getLastSysTranId() {
        return lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    @Basic
    @Column(name = "UPD_SEQ", nullable = true, insertable = true, updatable = true, precision = 0)
    public Long getUpdSeq() {
        return updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductEntity that = (ProductEntity) o;

        if (allowCont != null ? !allowCont.equals(that.allowCont) : that.allowCont != null) return false;
        if (allowSub != null ? !allowSub.equals(that.allowSub) : that.allowSub != null) return false;
        if (effective != null ? !effective.equals(that.effective) : that.effective != null) return false;
        if (expiry != null ? !expiry.equals(that.expiry) : that.expiry != null) return false;
        if (inheritCondition != null ? !inheritCondition.equals(that.inheritCondition) : that.inheritCondition != null)
            return false;
        if (inheritDocument != null ? !inheritDocument.equals(that.inheritDocument) : that.inheritDocument != null)
            return false;
        if (inheritMenuAcl != null ? !inheritMenuAcl.equals(that.inheritMenuAcl) : that.inheritMenuAcl != null)
            return false;
        if (inheritParentAcl != null ? !inheritParentAcl.equals(that.inheritParentAcl) : that.inheritParentAcl != null)
            return false;
        if (lastSysTranId != null ? !lastSysTranId.equals(that.lastSysTranId) : that.lastSysTranId != null)
            return false;
        if (linkCbs != null ? !linkCbs.equals(that.linkCbs) : that.linkCbs != null) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (pid != null ? !pid.equals(that.pid) : that.pid != null) return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;
        if (productName != null ? !productName.equals(that.productName) : that.productName != null) return false;
        if (productOwner != null ? !productOwner.equals(that.productOwner) : that.productOwner != null) return false;
        if (productSlogan != null ? !productSlogan.equals(that.productSlogan) : that.productSlogan != null)
            return false;
        if (updSeq != null ? !updSeq.equals(that.updSeq) : that.updSeq != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productId != null ? productId.hashCode() : 0;
        result = 31 * result + (productName != null ? productName.hashCode() : 0);
        result = 31 * result + (productSlogan != null ? productSlogan.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (productOwner != null ? productOwner.hashCode() : 0);
        result = 31 * result + (allowSub != null ? allowSub.hashCode() : 0);
        result = 31 * result + (allowCont != null ? allowCont.hashCode() : 0);
        result = 31 * result + (inheritCondition != null ? inheritCondition.hashCode() : 0);
        result = 31 * result + (inheritDocument != null ? inheritDocument.hashCode() : 0);
        result = 31 * result + (linkCbs != null ? linkCbs.hashCode() : 0);
        result = 31 * result + (effective != null ? effective.hashCode() : 0);
        result = 31 * result + (expiry != null ? expiry.hashCode() : 0);
        result = 31 * result + (pid != null ? pid.hashCode() : 0);
        result = 31 * result + (inheritMenuAcl != null ? inheritMenuAcl.hashCode() : 0);
        result = 31 * result + (inheritParentAcl != null ? inheritParentAcl.hashCode() : 0);
        result = 31 * result + (lastSysTranId != null ? lastSysTranId.hashCode() : 0);
        result = 31 * result + (updSeq != null ? updSeq.hashCode() : 0);
        return result;
    }

//    @ManyToOne
//    @JoinColumn(name = "PARENT_ID", referencedColumnName = "PRODUCT_ID")
//    public ProductEntity getProductByParentId() {
//        return productByParentId;
//    }
//
//    public void setProductByParentId(ProductEntity productByParentId) {
//        this.productByParentId = productByParentId;
//    }

    @OneToMany(mappedBy = "productByParentId")
    public Collection<ProductEntity> getProductsByProductId() {
        return productsByProductId;
    }

    public void setProductsByProductId(Collection<ProductEntity> productsByProductId) {
        this.productsByProductId = productsByProductId;
    }

    @OneToMany(mappedBy = "productByProductId")
    public Collection<ProductPropsEntity> getProductPropsesByProductId() {
        return productPropsesByProductId;
    }

    public void setProductPropsesByProductId(Collection<ProductPropsEntity> productPropsesByProductId) {
        this.productPropsesByProductId = productPropsesByProductId;
    }
}
