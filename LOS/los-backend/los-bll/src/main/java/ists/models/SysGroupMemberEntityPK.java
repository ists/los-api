package ists.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Administrator on 10/10/2014.
 */
public class SysGroupMemberEntityPK implements Serializable {
    private Long groupSid;
    private Long memberSid;

    @Column(name = "GROUP_SID", nullable = false, insertable = true, updatable = true, precision = -127)
    @Id
    public Long getGroupSid() {
        return groupSid;
    }

    public void setGroupSid(Long groupSid) {
        this.groupSid = groupSid;
    }

    @Column(name = "MEMBER_SID", nullable = false, insertable = true, updatable = true, precision = -127)
    @Id
    public Long getMemberSid() {
        return memberSid;
    }

    public void setMemberSid(Long memberSid) {
        this.memberSid = memberSid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysGroupMemberEntityPK that = (SysGroupMemberEntityPK) o;

        if (groupSid != null ? !groupSid.equals(that.groupSid) : that.groupSid != null) return false;
        if (memberSid != null ? !memberSid.equals(that.memberSid) : that.memberSid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = groupSid != null ? groupSid.hashCode() : 0;
        result = 31 * result + (memberSid != null ? memberSid.hashCode() : 0);
        return result;
    }
}
