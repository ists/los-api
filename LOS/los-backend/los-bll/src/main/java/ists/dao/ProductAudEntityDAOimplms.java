package ists.dao;


import ists.blls.Product;
import ists.blls.ProductAud;
import ists.models.*;
import ists.utility.EnumUtil;
import ists.blls.BaseClass;
import ists.models.viewModels.FullProduct;
import ists.models.viewModels.ProductDetailRight;
import ists.utility.Common;
import ists.utility.HibernateUtil;
import ists.utility.SequenceUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
        import org.hibernate.Query;
        import ists.utility.*;

import java.sql.Timestamp;


/**
 * Created by Chien Leader on 15-10-14.
 */

public class ProductAudEntityDAOimplms extends BaseClass implements ProductAudEntityDAO {


    public void delete(ProductAudEntity productEntity) {
        beginTransaction(Common.TRAN_ACTION.INPUT.toString(), null);

    }

    public void save(FullProduct fullProduct) throws Exception {

        try {
            ProductAudEntity pAudInput = fullProduct.getProduct();
            Long productAudId = pAudInput.getProductAudId();
            //Kiem tra neu da ton tai san pham (qua productId) thi la Update, chua ton tai la Save
            beginTransaction(((productAudId == null) ? Common.TRAN_ACTION.INPUT.toString() : Common.TRAN_ACTION.EDIT_DRAFT.toString()));

            ProductEntity product = new Product().getByProductId(pAudInput.getProductId());
            pAudInput.setAuditDate(new Timestamp(System.currentTimeMillis()));
            pAudInput.setSysTranId(sysTranId);
            if(productAudId != null) {
                //Edit draft
                pAudInput.setAuditOperation(EnumUtil.AuditOperation.EDIT_DRAFT.toString());
                pAudInput.setProductAudId(null);
            }
            else {
                pAudInput.setPid(SequenceUtil.getNextPId());

                pAudInput.setApprovalStatus(EnumUtil.ApprovalStatus.INPUT.toString());
                if (product == null) {
                    //Create sub
                    pAudInput.setAuditOperation(EnumUtil.AuditOperation.INSERT.toString());
                    pAudInput.setUpdSeq(Long.valueOf(1));
                } else {
                    //Edit
                    pAudInput.setAuditOperation(EnumUtil.AuditOperation.UPDATE.toString());
                    pAudInput.setUpdSeq(product.getUpdSeq());
                }
            }

            HibernateUtil.getSessionFactory().getCurrentSession().save(pAudInput);

            if(productAudId == null)
                commitTransaction(null);
            else {
                ProductAudEntity pAud = new ProductAud().getByProductAudIdWithTran(productAudId);
                commitTransaction(null, pAud.getSysTranId());
            }
        } catch (Exception ex) {
            rollbackTransaction(Common.TRAN_COMMENT.ERROR.toString());
            throw ex;
        }
    }

    public void approve(FullProduct fullProduct) {
        /*beginTransaction(Common.TRAN_ACTION.EDIT_DRAFT.toString(), null);
        //update to ProductAudEntity table
        ProductAudEntity productAudEntity = new ProductAudEntity();
        ProductAudEntity pae = fullProduct.getProduct();

        productAudEntity.setSysTranId(pae.getSysTranId());
        productAudEntity.setAuditDate(new Timestamp(System.currentTimeMillis()));
        productAudEntity.setAuditFields(pae.getAuditFields());
        productAudEntity.setAuditOperation(EnumUtil.AuditOperation.UPDATE.toString());
        productAudEntity.setUpdSeq(pae.getUpdSeq());
        productAudEntity.setParentId(pae.getParentId());
        productAudEntity.setProductOwner(pae.getProductOwner());
        productAudEntity.setAllowSub(pae.getAllowSub());
        productAudEntity.setAllowCont(pae.getAllowCont());
        productAudEntity.setInheritCondition(pae.getInheritCondition());
        productAudEntity.setInheritDocument(pae.getInheritDocument());
        productAudEntity.setLinkCbs(pae.getLinkCbs());
        productAudEntity.setExpiry(pae.getExpiry());
        productAudEntity.setEffective(pae.getEffective());


        // update to ProductPropsEntity table
        ProductPropsAudEntity ppaEntity = null;
        for (ProductPropsEntity ppEntity : fullProduct.getProductUdfs()) {
            ppaEntity = new ProductPropsAudEntity();
            ppaEntity.setUpdSeq(ppEntity.getUpdSeq());
            ppaEntity.setProductId(ppEntity.getProductId());
            ppaEntity.setUdfId(ppEntity.getUdfId());
            ppaEntity.setUdfValue(ppEntity.getUdfValue());
            ppaEntity.setAuditOperation(EnumUtil.AuditOperation.UPDATE.toString());
            ppaEntity.setApprovalStatus(EnumUtil.ApprovalStatus.REVIEW.toString());
            ppaEntity.setAuditDate(new Timestamp(System.currentTimeMillis()));
            ppaEntity.setAuditFields("All");

        }
        // update to ProductDetailRight
        ProductDetailRight ppRight = null;
        for (ProductDetailRight productDetailRight : fullProduct.getProductRights()) {
            ppRight = new ProductDetailRight(null, null, 0, null);
            ppRight.setAcl(productDetailRight.getAcl());
            ppRight.setName(productDetailRight.getName());
            ppRight.setSid(productDetailRight.getSid());
            ppRight.setType(productDetailRight.getType());

        }

        HibernateUtil.getSessionFactory().getCurrentSession().update(productAudEntity);
        HibernateUtil.getSessionFactory().getCurrentSession().update(ppaEntity);
        HibernateUtil.getSessionFactory().getCurrentSession().update(ppRight);
        commitTransaction(Common.TRAN_COMMENT.SUCCEED.toString());
*/
    }


    public ProductAudEntity findById(Long productAudId) {
        try {
            Query query = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from ProductAudEntity where productId=:id");
            query.setLong("id", productAudId);
            return (ProductAudEntity) query.uniqueResult();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return null;
        }
    }
    public ProductEntity findByProductId(String productId) {
        try {
            Query query = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from ProductEntity where productId=:id");
            query.setString("id", productId);
            return (ProductEntity) query.uniqueResult();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return null;
        }
    }

}
