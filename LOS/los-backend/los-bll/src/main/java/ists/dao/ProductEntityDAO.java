package ists.dao;

import ists.models.ProductAudEntity;
import ists.models.ProductEntity;

/**
 * Created by Administrator on 10/8/2014.
 */
public interface ProductEntityDAO {
    // Find all products
    //public List<ProductEntity> findAll();

    // Find all product with some properties
    //public List<ProductEntity> findAllWithDetail();

    // Find a product with details by id
    public ProductEntity findById(String id);

    // Insert a product
    public void deleteProduct(String productId) throws Exception;

    //Update a product
    public void save(Long productAudId);

    // Delete a product
    public void delete(String productId);

    public void approve (Long productAudId) throws Exception;

    public ProductAudEntity findById(Long productAudId);
}
//    public ProductEntity getProduct(BigDecimal productId) {
//
////        try {
////            Session session = HibernateUtil.getSessionFactory().openSession();
////            session.beginTransaction();
////
////            ProductEntity product = (ProductEntity) session.byId(ProductEntity.class).load(Integer.valueOf(productId));
////
////            return product;
////        } catch (Exception ex) {
////            throw ex;
////        }
//
//
//
//    }



