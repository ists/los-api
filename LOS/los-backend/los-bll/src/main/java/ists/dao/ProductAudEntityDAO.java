package ists.dao;

import ists.models.ProductAudEntity;
import ists.models.ProductEntity;
import ists.models.viewModels.FullProduct;

/**
 * Created by Administrator on 10/8/2014.
 */
public interface ProductAudEntityDAO {
    // Find all productAuds
   // public List<ProductAudEntity> findAll();

    // Find all productAud with some properties
    //public List<ProductAudEntity> findAllWithDetail();

    // Find a productAud with details by id
    public ProductAudEntity findById(Long id);

    // Insert a productAud
    public void save(FullProduct fullProduct) throws Exception;

    //Update a productAud
    public void approve(FullProduct fullProduct);

    // Delete a productAud
    public void delete(ProductAudEntity productAudEntity);

    public ProductEntity findByProductId(String productId);
}


