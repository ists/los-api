package ists.dao;

import ists.blls.BaseClass;
import ists.models.ProductAudEntity;
import ists.models.ProductEntity;
import ists.models.ProductPropsAudEntity;
import ists.models.ProductPropsEntity;
import ists.utility.Common;
import ists.utility.EnumUtil;
import ists.utility.HibernateUtil;
import org.hibernate.Query;

import java.sql.Timestamp;


/**
 * Created by Chien Leader on 15-10-14.
 */

public class ProductEntityDAOimplms extends BaseClass implements ProductEntityDAO {



    public void deleteProduct(String productId) throws Exception {
        try {
            beginTransaction(Common.TRAN_ACTION.INPUT.toString(), null);
            //Them 1 ban ghi co AuditOperation la DELETE vao ProductAudEntity
            ProductEntity productEntity = (ProductEntity) findById(productId);
            ProductAudEntity productAudEntity = new ProductAudEntity();

            productAudEntity.setProductName(productEntity.getProductName());
            productAudEntity.setProductSlogan((productEntity.getProductSlogan()));
            productAudEntity.setParentId((productEntity.getParentId()));
            productAudEntity.setProductOwner(productEntity.getProductOwner());
            productAudEntity.setAllowSub(productEntity.getAllowSub());
            productAudEntity.setAllowCont(productEntity.getAllowCont());
            productAudEntity.setInheritCondition(productEntity.getInheritCondition());
            productAudEntity.setInheritDocument(productEntity.getInheritDocument());
            productAudEntity.setLinkCbs(productEntity.getLinkCbs());
            productAudEntity.setPid(productEntity.getPid());
            productAudEntity.setInheritMenuAcl(productEntity.getInheritMenuAcl());
            productAudEntity.setInheritParentAcl(productEntity.getInheritParentAcl());
            productAudEntity.setEffective(productAudEntity.getEffective());
            productAudEntity.setExpiry(productAudEntity.getExpiry());
            productAudEntity.setAuditOperation(EnumUtil.AuditOperation.DELETE.toString());
            productAudEntity.setApprovalStatus(EnumUtil.ApprovalStatus.REVIEW.toString());
            productAudEntity.setAuditFields("All");
            productAudEntity.setAuditDate(new Timestamp(System.currentTimeMillis()));
            productAudEntity.setPrevSysTranId(productEntity.getLastSysTranId());

            //Them mot ban ghi vao ProducPropsAudEntity co trang thai DELETE cho approve
            ProductPropsEntity productPropsEntity = (ProductPropsEntity) HibernateUtil.getSessionFactory().getCurrentSession().byId(ProductPropsEntity.class).load(productId);
            ProductPropsAudEntity productPropsAudEntity = new ProductPropsAudEntity();

            productPropsAudEntity.setApprovalStatus(EnumUtil.ApprovalStatus.REVIEW.toString());
            productPropsAudEntity.setAuditFields("All");
            productPropsAudEntity.setAuditOperation(EnumUtil.AuditOperation.DELETE.toString());
            productPropsAudEntity.setAuditDate(new Timestamp(System.currentTimeMillis()));
            productPropsAudEntity.setPrevSysTranId(productPropsEntity.getLastSysTranId());
            productPropsAudEntity.setProductId(productPropsEntity.getProductId());
            productPropsAudEntity.setUdfId(productPropsEntity.getUdfId());
            productPropsAudEntity.setUdfValue(productPropsEntity.getUdfValue());
            productPropsAudEntity.setUpdSeq(productPropsEntity.getUpdSeq());

            HibernateUtil.getSessionFactory().getCurrentSession().save(productAudEntity);
            HibernateUtil.getSessionFactory().getCurrentSession().save(productPropsAudEntity);
        } catch (Exception ex) {
            rollbackTransaction(Common.TRAN_COMMENT.ERROR.toString());
            throw ex;
        }

    }

    public void save(Long productAudId) {
       // beginTransaction(Common.TRAN_ACTION.INPUT.toString(), null);
        //Them 1 ban ghi moi vao ProductEntity
        ProductAudEntity productAudEntity =findById(productAudId);
        ProductEntity productEntity = new ProductEntity();

        productEntity.setProductId(productAudEntity.getProductId());
        productEntity.setProductName(productAudEntity.getProductName());
        productEntity.setProductSlogan((productAudEntity.getProductSlogan()));
        productEntity.setParentId((productAudEntity.getParentId()));
        productEntity.setProductOwner(productAudEntity.getProductOwner());
        productEntity.setAllowSub(productAudEntity.getAllowSub());
        productEntity.setAllowCont(productAudEntity.getAllowCont());
        productEntity.setInheritCondition(productAudEntity.getInheritCondition());
        productEntity.setInheritDocument(productAudEntity.getInheritDocument());
        productEntity.setLinkCbs(productAudEntity.getLinkCbs());
        productEntity.setPid(productAudEntity.getPid());
        productEntity.setInheritMenuAcl(productAudEntity.getInheritMenuAcl());
        productEntity.setInheritParentAcl(productAudEntity.getInheritParentAcl());
        productEntity.setEffective(productAudEntity.getEffective());
        productEntity.setExpiry(productAudEntity.getExpiry());
        productEntity.setLastSysTranId(productEntity.getLastSysTranId());
        productEntity.setUpdSeq(productAudEntity.getUpdSeq() + 1);

        HibernateUtil.getSessionFactory().getCurrentSession().save(productEntity);

        //Them ban ghi moi vao ProducPropsEntity

        /*ProductPropsAudEntity productPropsAudEntity = (ProductPropsAudEntity)  HibernateUtil.getSessionFactory().getCurrentSession().byId(ProductPropsAudEntity.class).load();
        ProductPropsEntity productPropsEntity = new ProductPropsEntity();

        productPropsEntity.setUpdSeq(productPropsAudEntity.getUpdSeq() + 1);
        productPropsEntity.setUdfValue(productPropsAudEntity.getUdfValue());
        productPropsEntity.setUdfId(productPropsEntity.getUdfId());
        productPropsEntity.setProductId(productPropsAudEntity.getProductId());
        productPropsEntity.setLastSysTranId(productPropsAudEntity.getSysTranId());


        HibernateUtil.getSessionFactory().getCurrentSession().save(productEntity);
        HibernateUtil.getSessionFactory().getCurrentSession().save(productPropsEntity);
        commitTransaction(Common.TRAN_COMMENT.SUCCEED.toString());*/


    }

    public void approve(Long productAudId) throws Exception {
        try {
            beginTransaction(Common.TRAN_ACTION.APPROVE.toString());

            ProductAudEntity productAudEntity =  findById(productAudId);

            //Manipulate PRODUCT
            if (productAudEntity.getAuditOperation() == EnumUtil.AuditOperation.DELETE.toString()) {
                delete(productAudEntity.getProductId());
            } else {
                save(productAudEntity.getProductAudId());
            }

            //Update PRODUCT AUDIT: set Approval Status to APPROVED
            productAudEntity.setApprovalStatus(EnumUtil.ApprovalStatus.APPROVED.toString());
            HibernateUtil.getSessionFactory().getCurrentSession().save(productAudEntity);

            commitTransaction(Common.TRAN_COMMENT.SUCCEED.toString(), productAudEntity.getSysTranId());
        } catch (Exception ex) {
            rollbackTransaction(Common.TRAN_COMMENT.ERROR.toString());
            throw ex;
        }
    }

    public void delete(String productId) {
        ProductEntity productEntity = (ProductEntity) findById(productId);
        HibernateUtil.getSessionFactory().getCurrentSession().delete(productEntity);
    }

    public ProductEntity findById(String id) {
        //return (ProductEntity) HibernateUtil.getSessionFactory().byId(ProductEntity.class).load(id);
        return null;
    }
    public ProductAudEntity findById(Long productAudId) {
        try {
            Query query = HibernateUtil.getSessionFactory().getCurrentSession().createQuery("from ProductAudEntity where productAudId = :id");
            query.setLong("id", productAudId);
            return (ProductAudEntity) query.uniqueResult();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return null;
        }
    }
}
