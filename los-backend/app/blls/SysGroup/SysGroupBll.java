package blls.SysGroup;

import blls.base.BllBase;
import models.entities.SysGroup;
import models.viewModels.SysGroupModel;
import repositories.SysGroupRepository;
import utilities.CommonUtility;

import javax.inject.Named;
import javax.inject.Singleton;
import java.security.cert.Extension;
import java.util.List;

/**
 * Created by Administrator on 11/14/2014.
 */
@Named
@Singleton
public class SysGroupBll extends BllBase{
    public List<SysGroupModel> getAllGroup() throws Exception {
        // Find all group in system
        List<SysGroup> groupList = getRepositoryFactory().getRepositoryInstance(SysGroupRepository.class).findAll();

        // Copy to group model and return
        List<SysGroupModel> sysGroupModels;
        sysGroupModels = CommonUtility.copyListObjects(groupList, SysGroup.class, SysGroupModel.class);

        return sysGroupModels;
    }
}
