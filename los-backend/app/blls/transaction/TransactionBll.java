package blls.transaction;

import blls.base.BllBase;
import factory.repository.RepositoryFactory;
import javassist.NotFoundException;
import models.entities.SysTransaction;
import org.springframework.transaction.annotation.Transactional;
import repositories.SysTransactionRepository;
import utilities.EnumUtility;
import utilities.FactoryContext;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Trung on 11/3/2014.
 */
@Named
@Singleton
public class TransactionBll extends BllBase {

    public SysTransaction addTransaction() {
        SysTransaction transaction = new SysTransaction();
        transaction.setUserId("demo.approve");
        transaction.setComments("Giao dịch thành công");
        transaction.setTranStart(new Timestamp(new Date().getTime()));

        SysTransactionRepository repository = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class);

        transaction.setDbSessionId(repository.getDBSession());
        transaction = repository.save(transaction);

        return transaction;
    }

    public void commitTransaction(SysTransaction transaction) {
        transaction.setTranEnd(new Timestamp(new Date().getTime()));
        closePreviousTransaction(transaction.getPrevSysTranId());
        transaction = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).save(transaction);
    }

    public void approveTransaction(SysTransaction transaction) throws NotFoundException {
        transaction.setTranEnd(new Timestamp(new Date().getTime()));
        approvePreviousTransaction(transaction.getPrevSysTranId());
        transaction = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).save(transaction);
    }

    public void rejectTransaction(SysTransaction transaction) throws NotFoundException {
        transaction.setTranEnd(new Timestamp(new Date().getTime()));
        rejectPreviousTransaction(transaction.getPrevSysTranId());
        transaction = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).save(transaction);
    }

    public void closeTransaction(SysTransaction transaction) {
        transaction.setTranEnd(new Timestamp(new Date().getTime()));
        transaction.setTranStatus(EnumUtility.TRAN_STATUS.CLOSE.toString());
        transaction = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).save(transaction);
    }

    public void rollbackTransaction(SysTransaction transaction) {
        transaction.setComments("Transaction error & rollbacks");
        transaction.setTranEnd(new Timestamp(new Date().getTime()));
        transaction.setTranStatus(EnumUtility.TRAN_STATUS.ERROR.toString());
        transaction = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).save(transaction);
    }

    private  void closePreviousTransaction(Long prevTranId) {
        if (prevTranId == null || !getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).exists(prevTranId))
            return;

        SysTransaction prevTran = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).findOne(prevTranId);
        prevTran.setTranStatus(EnumUtility.TRAN_STATUS.CLOSE.toString());
        getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).save(prevTran);
    }

    private  void approvePreviousTransaction(Long prevTranId) throws NotFoundException {
        if (prevTranId == null || !getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).exists(prevTranId))
            throw new NotFoundException("Could not found TRANSACTION_ID: '" + prevTranId + "' for approval");

        SysTransaction prevTran = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).findOne(prevTranId);
        prevTran.setTranStatus(EnumUtility.TRAN_STATUS.APPROVED.toString());
        getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).save(prevTran);
    }

    private  void rejectPreviousTransaction(Long prevTranId) throws NotFoundException {
        if (prevTranId == null || !getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).exists(prevTranId))
            throw new NotFoundException("Could not found TRANSACTION_ID: '" + prevTranId + "' for approval");

        SysTransaction prevTran = getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).findOne(prevTranId);
        prevTran.setTranStatus(EnumUtility.TRAN_STATUS.REJECT.toString());
        getRepositoryFactory().getRepositoryInstance(SysTransactionRepository.class).save(prevTran);
    }
}
