package blls.sysUser;

import blls.base.BllBase;
import models.entities.SysGroupMember;
import models.entities.SysRight;
import models.entities.SysUser;
import models.viewModels.SysUserModel;
import repositories.SysGroupMemberRepository;
import repositories.SysRightRepository;
import repositories.SysUserRepository;
import utilities.CommonUtility;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Administrator on 10/29/2014.
 */
@Named
@Singleton
public class SysUserBll extends BllBase {

    public String getACLByUserIdAndPID(String userId, Long pid) {
        SysUser sysUser = getRepositoryFactory().getRepositoryInstance(SysUserRepository.class).findOne(userId);
        //List sid of user and group where user belong to.
        List<Long> sidList = getRepositoryFactory().getRepositoryInstance(SysGroupMemberRepository.class).findGroupSidByMemberSid(sysUser.sid);
        sidList.add(sysUser.sid);

        //Get ACL of user for pid
        StringBuilder acl = new StringBuilder();
        for (Long sid : sidList) {
            List<String> aclList = getRepositoryFactory().getRepositoryInstance(SysRightRepository.class).findAclBySidAndPid(sid, pid);
            if(!aclList.isEmpty())
                acl.append(aclList.get(0));
        }

        return acl.toString();
    }

    public List<SysUserModel> getUserListForAddRight() throws Exception {
        // Find all user active in system.
        List<SysUser> userList = getRepositoryFactory().getRepositoryInstance(SysUserRepository.class).findByStatus("A");

        // Copy user to user model and return to client
        List<SysUserModel> resultList;
        resultList = CommonUtility.copyListObjects(userList, SysUser.class, SysUserModel.class);

        return resultList;
    }
}
