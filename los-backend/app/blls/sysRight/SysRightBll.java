package blls.sysRight;

import blls.base.BllBase;
import models.entities.SysRightAud;
import models.viewModels.SysRightAudModel;
import repositories.SysRightAudRepository;
import utilities.ObjectUtility;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.ArrayList;

/**
 * Created by Trung on 11/11/2014.
 */
@Named
@Singleton
public class SysRightBll extends BllBase {
    public void addSysRightAud(SysRightAudModel sysRightAudModel) throws Exception {
        SysRightAud sysRightAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(sysRightAudModel, SysRightAudModel.class, null, SysRightAud.class);

        addSysRightAudEntity(sysRightAud);
    }

    public void addSysRightAuds(ArrayList<SysRightAudModel> sysRightAudModels) throws Exception {
        for (SysRightAudModel sysRightAudModel: sysRightAudModels) {
            addSysRightAud(sysRightAudModel);
        }
    }

    private void addSysRightAudEntity(SysRightAud sysRightAud) {
        try {
            // todo kiem tra

            getRepositoryFactory().getRepositoryInstance(SysRightAudRepository.class).save(sysRightAud);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
}
