package blls.product;

import blls.CommonBll;
import blls.base.BllBase;
import blls.sysRight.SysRightBll;
import blls.sysUser.SysUserBll;
import models.entities.*;
import models.viewModels.*;
import models.viewModels.commonViewModels.RightDetailModel;
import models.viewModels.commonViewModels.TreeModel;
import repositories.*;
import utilities.CommonUtility;
import utilities.EnumUtility;
import utilities.ObjectUtility;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/29/2014.
 */
@Named
@Singleton
public class ProductBll extends BllBase {
    @Inject
    private CommonBll commonBll;
    @Inject
    private SysUserBll sysUserBll;
    @Inject
    private SysRightBll sysRightBll;

    /**
     * Get product right by user for display and hide button bar
     *
     * @param menuId
     * @param productId
     * @param userId
     * @return
     */
    public String getProductRight(Long menuId, String productId, String userId) {
        String acl = null;

        SysMenu menu = getRepositoryFactory().getRepositoryInstance(SysMenuRepository.class).findOne(menuId);
        if (menu != null) {
            String menuACL = sysUserBll.getACLByUserIdAndPID(userId, menu.pid);
            List<Product> productList = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findByProductId(productId);
            if (!productList.isEmpty()) {
                acl = getACLOfProduct(productList.get(0), menuACL, userId);
            }
        }

        return acl;
    }

    /**
     * Recursive method to get ACL of Product by User
     *
     * @param product
     * @param menuAcl
     * @param userId
     * @return
     */
    public String getACLOfProduct(Product product, String menuAcl, String userId) {
        String acl = null;
        if (product != null) {
            if (product.inheritMenuAcl == 1) {
                // Inherit Right from Product menu
                acl = menuAcl;
            } else if (product.inheritParentAcl == 1) {
                // Inherit Right from parent product
                if (product.parentId != null && !product.parentId.isEmpty()) {
                    List<Product> parentProductList = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findByProductId(product.parentId);
                    if (!parentProductList.isEmpty())
                        acl = getACLOfProduct(parentProductList.get(0), menuAcl, userId);
                }
            } else {
                // Get Right of product
                acl = sysUserBll.getACLByUserIdAndPID(userId, product.pid);
            }
        }
        return acl;
    }

    public List<ProductModel> getProductListForAddParent() throws Exception{
        List<ProductModel> resultList;

        List<Product> productList = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findByAllowSub((long) 1);

        resultList = CommonUtility.copyListObjects(productList, Product.class, ProductModel.class);
        return resultList;
    }

    /**
     * Get tree by user's right
     *
     * @param menuId
     * @param userId
     * @return
     */
    public List<TreeModel> getProductTreeByUserId(Long menuId, String userId) {
        List<TreeModel> productTree = new ArrayList();
        // Get pid and heritable of menu for display tree
        SysMenu menu = getRepositoryFactory().getRepositoryInstance(SysMenuRepository.class).findOne(menuId);
        if (menu != null) {
            String menuACL = sysUserBll.getACLByUserIdAndPID(userId, menu.pid);
            if (menu.heritableAcl == 1) {
                // When heritable == 1, all product record must follow "Product menu" right.
                if (menuACL.contains(EnumUtility.SYS_RIGHT_DEF.VIEW.toString())) {
                    // If menu have "VIEW right", return all product
                    List<Product> productList = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findAll();
                    for (Product product : productList) {
                        productTree.add(new TreeModel(product));
                    }
                }
            } else {
                // When heritable == 0, follow record's right
                // When we have small table (<300 records), load all and use java to manipulate
                // When we have large table (>300 records), don't load all, get each part from database
                productTree = getProductTreeByRight(getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findAll(), null, menuACL, null, userId);
            }
        }

        return productTree;
    }

    /**
     * Recursive method to get product list by right of user.
     *
     * @param list
     * @param parentId
     * @param menuAcl
     * @param parentAcl
     * @param userId
     * @return
     */
    private List<TreeModel> getProductTreeByRight(List<Product> list, final String parentId, String menuAcl, String parentAcl, String userId) {
        List<TreeModel> productTree = new ArrayList();

        if (parentId == null) {
            // Get Root node
            Product rootNode = list.stream().filter(product -> product.parentId == null || product.parentId.isEmpty()).findFirst().get();
            // Check if Root node can be viewed by User
            String rootAcl = sysUserBll.getACLByUserIdAndPID(userId, rootNode.pid);

            if ((rootNode.inheritMenuAcl == 1 && menuAcl.contains(EnumUtility.SYS_RIGHT_DEF.VIEW.toString()))
                    || (rootNode.inheritMenuAcl == 0 && rootAcl.contains(EnumUtility.SYS_RIGHT_DEF.VIEW.toString()))) {
                // Inherit from menu and have View right with Product Menu
                // Or not inherit from menu and have View right with Root node
                String eNodeAcl;
                if (rootNode.inheritMenuAcl == 1) {
                    eNodeAcl = menuAcl;
                } else {
                    eNodeAcl = rootAcl;
                }
                productTree.add(new TreeModel(rootNode));

                // Recursive all leaf of root node.
                List<TreeModel> leafList = getProductTreeByRight(list, rootNode.productId, menuAcl, eNodeAcl, userId);
                productTree.addAll(leafList);
            }
        } else {
            // Get Leaf node
            for (Product node : list) {
                if (node.parentId != null && node.parentId.equals(parentId)) {
                    // Check if each node can be viewed by User
                    String nodeAcl = sysUserBll.getACLByUserIdAndPID(userId, node.pid);

                    if ((node.inheritMenuAcl == 1 && menuAcl.contains(EnumUtility.SYS_RIGHT_DEF.VIEW.toString()))
                            || (node.inheritMenuAcl == 0 && node.inheritParentAcl == 1 && parentAcl.contains(EnumUtility.SYS_RIGHT_DEF.VIEW.toString()))
                            || (node.inheritMenuAcl == 0 && node.inheritParentAcl == 0 && nodeAcl.contains(EnumUtility.SYS_RIGHT_DEF.VIEW.toString()))) {
                        //Inherit from menu and have View right with Product menu
                        //Or not inherit from menu, inherit from parent and have View right with parent node
                        //Or not inherit from anywhere and have View right with node
                        productTree.add(new TreeModel(node));

                        //Get exactly ACL of this node
                        String eNodeAcl;
                        if (node.getInheritMenuAcl() == 1) {
                            eNodeAcl = menuAcl;
                        } else if (node.inheritMenuAcl == 0 && node.inheritParentAcl == 1) {
                            eNodeAcl = parentAcl;
                        } else {
                            eNodeAcl = nodeAcl;
                        }

                        //Recursive all leaf of this node.
                        List<TreeModel> leafList = getProductTreeByRight(list, node.productId, menuAcl, eNodeAcl, userId);
                        productTree.addAll(leafList);
                    }
                }
            }
        }

        return productTree;
    }

    /**
     * Get list user, group with right of product
     *
     * @param productId
     * @return
     */
    public List<RightDetailModel> getProductDetailRight(String productId) {
        // Find pid from productId
        List<RightDetailModel> rightList = new ArrayList<RightDetailModel>();
        List<Long> pidList = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findPidByProductId(productId);

        // If exists, call method to get right detail list.
        if (!pidList.isEmpty())
            rightList = commonBll.getRightDetailByPid(pidList.get(0));

        return rightList;
    }

    /**
     * Get main properties of product by productId
     *
     * @param productId
     * @return
     * @throws Exception
     */
    public ProductModel getProductDetail(String productId) throws Exception {
        ProductModel productModel = new ProductModel();
        Product product = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findOne(productId);
        if (product == null) return null;
        productModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                product,
                product.getClass(),
                productModel,
                productModel.getClass());

        return productModel;
    }

    /**
     * Get detail productAud by id
     * @param productAudId
     * @return
     * @throws Exception
     */
    public ProductAudModel getProductAudDetail(Long productAudId) throws Exception {
        ProductAudModel productAudModel = new ProductAudModel();
        ProductAud productAud = getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).findOne(productAudId);
        if (productAud == null) return null;
        productAudModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                productAud,
                productAud.getClass(),
                productAudModel,
                productAudModel.getClass());

        return productAudModel;
    }

    /**
     * Get props of product by productId
     *
     * @param productId
     * @return
     * @throws Exception
     */
    public List<ProductPropModel> getProductDetailProps(String productId) throws Exception {
        List<ProductPropModel> resultList = new ArrayList<>();
        if (productId != null && !productId.isEmpty()) {
            // In case view detail product, get all props of product by productId
            List<ProductProp> list = getRepositoryFactory().getRepositoryInstance(ProductPropRepository.class).findByProductId(productId);
            resultList = CommonUtility.copyListObjects(list, ProductProp.class, ProductPropModel.class);
        } else {
            // In case add new product, need to get props definition of PRODUCT
            List<UdtmField> udtmFieldList = getRepositoryFactory().getRepositoryInstance(UdtmFieldRepository.class).findByFieldGroupId(EnumUtility.UDTM_TYPE.PRODUCT.toString());
            for (UdtmField udtm : udtmFieldList) {
                ProductPropModel propModel = new ProductPropModel();
                UdtmFieldModel udtmFieldModel = new UdtmFieldModel();
                udtmFieldModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                        udtm,
                        udtm.getClass(),
                        udtmFieldModel,
                        udtmFieldModel.getClass()
                );
                propModel.udtmField = udtmFieldModel;
                resultList.add(propModel);
            }
        }
        return resultList;
    }

    public List<ProductPropsAudModel> getProductAudDetailProps(Long productAudId) throws Exception {
        List<ProductPropsAudModel> resultList;
        ProductAud productAud = getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).findOne(productAudId);
        List<ProductPropsAud> productPropsAuds = getRepositoryFactory().getRepositoryInstance(ProductPropsAudRepository.class).findByProductIdAndSysTranId(productAud.productId, productAud.sysTran.sysTranId);
        resultList = CommonUtility.copyListObjects(productPropsAuds, ProductPropsAud.class, ProductPropsAudModel.class);

        return resultList;
    }

    public List<SysRightAudModel> getProductAudDetailRight(Long productAudId) throws Exception {
        List<SysRightAudModel> resultList;
        ProductAud productAud = getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).findOne(productAudId);
        List<SysRightAud> sysRightAuds = getRepositoryFactory().getRepositoryInstance(SysRightAudRepository.class).findByPidAndSysTranId(productAud.pid, productAud.sysTran.sysTranId);
        resultList = CommonUtility.copyListObjects(sysRightAuds, SysRightAud.class, SysRightAudModel.class);

        return resultList;
    }

    /**
     * Get list product audit waiting for approval
     *
     * @return
     * @throws Exception
     */
    public List<ProductAudModel> getProductAudForApproval() throws Exception {
        List<ProductAudModel> resultList;

        List<ProductAud> audList = getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).findProductAudForApproval();
        resultList = CommonUtility.copyListObjects(audList, ProductAud.class, ProductAudModel.class);

        return resultList;
    }

    /**
     * Approve product audit
     *
     * @throws Exception
     */
    public void approveProductAud(Long productAudId) throws Exception {
        ProductAud productAud = getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).findOne(productAudId);

        if (productAud != null) {
            Product product = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findOne(productAud.productId);
            // If exists newer version of productId, no approve and return
            if(product != null && product.updSeq > productAud.updSeq )
                return;

            getCurrentTransaction().setPrevSysTranId(productAud.sysTran.sysTranId);
            if (productAud.auditOperation.equals(EnumUtility.AUDIT_OPERATION.DELETE.toString())) {
                // Delete product
                deleteProductByProductAud(productAud);
            } else {
                // Add or update product
                addOrUpdateProductByProductAud(productAud);
            }

            //Create new productAud for approval
            ProductAud approveProductAud = new ProductAud();
            approveProductAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                    productAud,
                    productAud.getClass(),
                    approveProductAud,
                    approveProductAud.getClass()
            );
            approveProductAud.productAudId = null;
            approveProductAud.prevSysTranId = productAud.sysTran.sysTranId;
            approveProductAud.approvalStatus = EnumUtility.APPROVAL_STATUS.APPROVED.toString();
            getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).save(approveProductAud);
        }
    }

    private void addOrUpdateProductByProductAud(ProductAud productAud) throws Exception {
        Product product = new Product();

        product = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                productAud,
                productAud.getClass(),
                product,
                product.getClass()
        );
        product.updSeq = product.updSeq + 1;

        if (productAud.auditOperation.equals(EnumUtility.AUDIT_OPERATION.UPDATE.toString())) {
            // Update
            // Delete Product's props and right
            deleteProductProps(productAud.productId);
            deleteProductRights(productAud.pid);
        }

        // Save product to database
        product.lastSysTranId = getCurrentTransaction().sysTranId;
        getRepositoryFactory().getRepositoryInstance(ProductRepository.class).save(product);

        // Save props of product
        List<ProductPropsAud> productPropsAuds = getRepositoryFactory().getRepositoryInstance(ProductPropsAudRepository.class).findByProductIdAndSysTranId(productAud.productId, productAud.sysTran.sysTranId);
        for (ProductPropsAud productPropsAud : productPropsAuds) {
            ProductProp productProp = new ProductProp();
            productProp = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                    productPropsAud,
                    productPropsAud.getClass(),
                    productProp,
                    productProp.getClass()
            );
            productProp.lastSysTranId = getCurrentTransaction().sysTranId;
            productProp.udtmField = getRepositoryFactory().getRepositoryInstance(UdtmFieldRepository.class).findOne(productPropsAud.getUdfId());
            getRepositoryFactory().getRepositoryInstance(ProductPropRepository.class).save(productProp);
        }
        // Save right of product
        List<SysRightAud> sysRightAuds = getRepositoryFactory().getRepositoryInstance(SysRightAudRepository.class).findByPidAndSysTranId(productAud.pid, productAud.sysTran.sysTranId);
        for (SysRightAud sysRightAud : sysRightAuds) {
            SysRight sysRight = new SysRight();
            sysRight = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                    sysRightAud,
                    sysRightAud.getClass(),
                    sysRight,
                    sysRight.getClass()
            );
            getRepositoryFactory().getRepositoryInstance(SysRightRepository.class).save(sysRight);
        }
    }

    private void deleteProductByProductAud(ProductAud productAud) {
        // Delete Product's props and right
        deleteProductProps(productAud.productId);
        deleteProductRights(productAud.pid);
        // Delete Product
        getRepositoryFactory().getRepositoryInstance(ProductRepository.class).delete(productAud.productId);
    }

    private void deleteProductProps(String productId) {
        getRepositoryFactory().getRepositoryInstance(ProductPropRepository.class).deleteByProductId(productId);
    }

    private void deleteProductRights(Long pid) {
        getRepositoryFactory().getRepositoryInstance(SysRightRepository.class).deleteByPid(pid);
    }

    /**
     * Add new product. Product information will be saved in PRODUCT_AUD for further processes
     * Product information will be breaks in to 3 parts: ProductAudModel,  Set<ProductPropsAudModel> and Set<SysRightAudModel>
     *
     * @param productModel
     * @throws Exception
     */
    public void addProductAud(ProductModel productModel) throws Exception {
        ProductAudModel productAudModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(productModel, ProductModel.class, null, ProductAudModel.class);
        ArrayList<ProductPropsAudModel> productPropsAudModels = new ArrayList<ProductPropsAudModel>();
        ArrayList<SysRightAudModel> sysRightAudModels = new ArrayList<SysRightAudModel>();

        productAudModel.sysTran = getCurrentTransaction();
        productAudModel.approvalStatus = EnumUtility.APPROVAL_STATUS.INPUT.getValue();
        if (getRepositoryFactory().getRepositoryInstance(ProductRepository.class).exists(productModel.productId)) {
            Product oldProduct = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findOne(productModel.productId);

            ProductAud oldProductAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                    oldProduct
                    , Product.class
                    , null
                    , ProductAud.class);

            productAudModel.updSeq = oldProductAud.updSeq;
            productAudModel.auditOperation = EnumUtility.AUDIT_OPERATION.UPDATE.getValue();
            productAudModel.pid = oldProductAud.pid;

            productAudModel.auditFields = ObjectUtility.SingleClassUtility.compareAnnotationMethodsToString(productModel, oldProductAud);
        }
        else {
            productAudModel.updSeq = Long.valueOf(0);
            productAudModel.auditOperation = EnumUtility.AUDIT_OPERATION.INSERT.getValue();
            productAudModel.pid = commonBll.getNextPId();
        }

        insertProductForInput(productAudModel);

        if (productModel.productProps != null) {
            ProductPropsAudModel productPropsAudModel;

            for (ProductPropModel productPropModel : productModel.productProps) {
                productPropsAudModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(productPropModel, ProductPropModel.class, null, ProductPropsAudModel.class);
                productPropsAudModel.productId = productAudModel.productId;
                productPropsAudModel.sysTranId = productAudModel.sysTran.sysTranId;
                productPropsAudModel.updSeq = productAudModel.updSeq;
                productPropsAudModel.auditOperation = productAudModel.auditOperation;
                productPropsAudModel.approvalStatus = productAudModel.approvalStatus;
                productPropsAudModels.add(productPropsAudModel);
            }
            insertProductPropertiesForInput(productPropsAudModels);
        }

        if (productModel.productRights != null) {
            SysRightAudModel sysRightAudModel;

            for (SysRightModel sysRightModel : productModel.productRights) {
                sysRightAudModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(sysRightModel, SysRightModel.class, null, SysRightAudModel.class);
                sysRightAudModel.pid = productAudModel.pid;
                sysRightAudModel.sysTranId = productAudModel.sysTran.sysTranId;
                sysRightAudModel.updSeq = productAudModel.updSeq;
                sysRightAudModel.auditOperation = productAudModel.auditOperation;
                sysRightAudModel.approvalStatus = productAudModel.approvalStatus;
                sysRightAudModels.add(sysRightAudModel);
            }
            sysRightBll.addSysRightAuds(sysRightAudModels);
        }
    }

    /**
     * Insert a new product
     * tranAction : Input
     *
     * @param productAudModel
     * @throws Exception
     */
    public void insertProductForInput(ProductAudModel productAudModel) throws Exception {
        ProductAud productAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(productAudModel
                , ProductAudModel.class
                , null
                , ProductAud.class);

        // Todo remove prevSysTranId

        addProductAudEntity(productAud);
    }

    public void insertProductPropertiesForInput(ArrayList<ProductPropsAudModel> productPropsAudModels) throws Exception {
        for (ProductPropsAudModel productPropsAudModel : productPropsAudModels) {
            ProductPropsAud productPropAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(productPropsAudModel, ProductPropsAudModel.class, null, ProductPropsAud.class);

            addProductPropsAudEntity(productPropAud);
        }
    }

    /**
     * Insert a new product
     * tranAction : EDIT_DRAFT
     *
     * @param productAudModel
     * @throws Exception
     */
    public void insertProductForEditDraft(ProductAudModel productAudModel) throws Exception {

        ProductAud oldProductAud = getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).findOne(productAudModel.productAudId);
        // todo upate old transaction -> InActive

        ProductAud productAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(productAudModel
                , ProductAudModel.class
                , null
                , ProductAud.class);

        productAud.approvalStatus = EnumUtility.APPROVAL_STATUS.INPUT.getValue();
        // Get all annotations
        productAud.auditFields = null;
        productAud.auditOperation = EnumUtility.AUDIT_OPERATION.INSERT.getValue();
        productAud.pid = oldProductAud.pid;
//        productAud.prevSysTranId = oldProductAud.sysTranId;
        productAud.updSeq = new Long(0);

        addProductAudEntity(productAud);

        // update current transaction : tranAction = EDIT_DRAFT
        getCurrentTransaction().tranAction = EnumUtility.TRAN_ACTION.EDIT_DRAFT.toString();

    }

    /**
     * Insert a new product
     * tranAction : APPROVE
     *
     * @param productAudId
     * @throws Exception
     */
    public void insertProductForApprove(Long productAudId) throws Exception {

        ProductAud productAud = getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).findOne(productAudId);
        // todo upate old transaction -> Close

        productAud.approvalStatus = EnumUtility.APPROVAL_STATUS.APPROVED.getValue();
        // Get all annotations
        productAud.auditFields = null;
        productAud.auditOperation = EnumUtility.AUDIT_OPERATION.INSERT.getValue();
//        productAud.prevSysTranId = productAud.sysTranId;
        productAud.updSeq = new Long(0);

        addProductAudEntity(productAud);

        // update current transaction : tranAction = EDIT_DRAFT
        getCurrentTransaction().tranAction = EnumUtility.TRAN_ACTION.APPROVE.toString();

        Product product = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                productAud
                , productAud.getClass()
                , null
                , Product.class
                , ObjectUtility.ACCEPT_DIFFERENT_TYPE_ENUM.SKIP_DIFFERENT_TYPE
        );
        addProductEntity(product);
    }

    /**
     * Update a product
     *
     * @param productAudModel
     * @throws Exception
     */
    public void updateProduct(ProductAudModel productAudModel) throws Exception {
        ProductAud productAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                productAudModel
                , ProductAudModel.class
                , null
                , ProductAud.class);

        Product oldProduct = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findOne(productAud.productId);

        ProductAud oldProductAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                oldProduct
                , Product.class
                , null
                , ProductAud.class);
        if (oldProductAud == null)
            throw new Exception("Product " + productAud.productName + " not exists");

        productAud.approvalStatus = EnumUtility.APPROVAL_STATUS.INPUT.getValue();
        productAud.auditFields = ObjectUtility.SingleClassUtility.compareAnnotationMethodsToString(productAud, oldProductAud);
        productAud.auditOperation = EnumUtility.AUDIT_OPERATION.UPDATE.getValue();
        productAud.pid = oldProductAud.pid;
//        productAud.prevSysTranId = oldProductAud.sysTranId;
        productAud.updSeq = oldProductAud.updSeq;

        addProductAudEntity(productAud);
    }

    /**
     * Delete a product
     *
     * @param productId
     * @throws Exception
     */
    public void deleteProduct(String productId) throws Exception {
        ProductAud productAud = ObjectUtility.MultiClassesUtility.copyObjectProperties(getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findOne(productId)
                , Product.class
                , null
                , ProductAud.class);

        productAud.approvalStatus = EnumUtility.APPROVAL_STATUS.INPUT.getValue();
        productAud.auditFields = ObjectUtility.SingleClassUtility.getAllAnnotationNameToString(ProductAud.class);
        productAud.auditOperation = EnumUtility.AUDIT_OPERATION.DELETE.getValue();

        productAud.setSysTran(getCurrentTransaction());

        addProductAudEntity(productAud);
    }

    private void addProductPropsAudEntity(ProductPropsAud productPropsAud) {
        try {
            // todo kiem tra

            getRepositoryFactory().getRepositoryInstance(ProductPropsAudRepository.class).save(productPropsAud);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * Add entity : productAud
     *
     * @param productAud
     * @return
     */
    private void addProductAudEntity(ProductAud productAud) throws Exception {
        try {
//            if (!isBoolean(productAud.allowCont))
//                throw new Exception("AllowCont Format Error");
//
//            if (!isBoolean(productAud.allowSub))
//                throw new Exception("AllowSub Format Error");

            productAud.auditDate = new Timestamp(System.currentTimeMillis());

            // Todo check expiry > effective

//            if (!isBoolean(productAud.inheritCondition))
//                throw new Exception("InheritCondition Format Error");
//
//            if (!isBoolean(productAud.inheritDocument))
//                throw new Exception("inheritDocument Format Error");
//
//            if (!isBoolean(productAud.inheritMenuAcl))
//                throw new Exception("inheritMenuAcl Format Error");
//
//            if (!isBoolean(productAud.inheritParentAcl))
//                throw new Exception("inheritParentAcl Format Error");

            if (productAud.parentId != null && !getRepositoryFactory().getRepositoryInstance(ProductRepository.class).exists(productAud.parentId))
                throw new Exception("parentId not exists");

            if (productAud.productId == null)
                throw new Exception("productId is null");

//            if (getRepositoryFactory().getRepositoryInstance(ProductRepository.class).exists(productAud.productId))
//                throw new Exception("productId exists");

            if (productAud.productName == null)
                throw new Exception("productName is null");

            // Todo check Owner

//            productAud.sysTranId = getCurrentTransaction().sysTranId;

            getRepositoryFactory().getRepositoryInstance(ProductAudRepository.class).save(productAud);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private void addProductEntity(Product product) {
        try {
            // todo kiem tra

            getRepositoryFactory().getRepositoryInstance(ProductRepository.class).save(product);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    /**
     * Kiem tra mien {0,1}
     *
     * @param value
     * @return
     */
    private boolean isBoolean(Long value) {
        return value != new Long(0) && value != new Long(1) ? false : true;
    }

    /**
     * Demo: Using copy object
     *
     * @return
     * @throws Exception
     */
    public ProductModel useCopyObject() throws Exception {
        ProductModel productModel = new ProductModel();
        Product product = getRepositoryFactory().getRepositoryInstance(ProductRepository.class).findOne("TD");

        // use copyObjectProperties
        productModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                product,
                product.getClass(),
                productModel,
                productModel.getClass(),
                ObjectUtility.ARRAY_COLECTION_ENUM.ARRAY_TYPE_SET, // ObjectUtility.ARRAY_COLECTION_ENUM.ARRAY_TYPE_LIST
                ObjectUtility.ACCEPT_DIFFERENT_TYPE_ENUM.ACCEPT_DIFFERENT_TYPE);

        // use SKIP_DIFFERENT_TYPE
//        productModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
//                product,
//                product.getClass(),
//                productModel,
//                productModel.getClass(),
//                ObjectUtility.ARRAY_COLECTION_ENUM.ARRAY_TYPE_SET,
//                ObjectUtility.ACCEPT_DIFFERENT_TYPE_ENUM.SKIP_DIFFERENT_TYPE);

//        Default SKIP_DIFFERENT_TYPE.ACCEPT_DIFFERENT_TYPE
//        productModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
//                product,
//                product.getClass(),
//                productModel,
//                productModel.getClass(),
//                ObjectUtility.ARRAY_COLECTION_ENUM.ARRAY_TYPE_SET);

        //        use SKIP_DIFFERENT_TYPE.SKIP_ARRAY_TYPE
//        productModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
//                product,
//                product.getClass(),
//                productModel,
//                productModel.getClass(),
//                ObjectUtility.ARRAY_COLECTION_ENUM.SKIP_ARRAY_TYPE);

        // Default ARRAY_TYPE_SET
//        productModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
//                product,
//                product.getClass(),
//                productModel,
//                productModel.getClass(),
//                ObjectUtility.ACCEPT_DIFFERENT_TYPE_ENUM.SKIP_DIFFERENT_TYPE);

        // Default ARRAY_TYPE_SET ,  DIFFERENT_TYPE_ENUM.ACCEPT_DIFFERENT_TYPE
//        productModel = ObjectUtility.MultiClassesUtility.copyObjectProperties(
//                product,
//                product.getClass(),
//                productModel,
//                productModel.getClass());

        return productModel;
    }
}
