package blls.base;

import factory.business.BusinessLayerFactory;
import factory.repository.RepositoryFactory;
import models.entities.SysTransaction;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Trung on 10/30/2014.
 */
@Named
@Singleton
public abstract class BllBase {
    private SysTransaction currentTransaction;

    public RepositoryFactory getRepositoryFactory() {
        return repositoryFactory;
    }

    @Inject
    public void setRepositoryFactory(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    private RepositoryFactory repositoryFactory;

    public SysTransaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(SysTransaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }
}