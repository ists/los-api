package blls;

import blls.base.BllBase;
import models.entities.SysGroup;
import models.entities.SysRight;
import models.entities.SysUser;
import models.viewModels.commonViewModels.RightDetailModel;
import repositories.SysGroupRepository;
import repositories.SysRightRepository;
import repositories.SysUserRepository;
import repositories.common.CommonRepository;
import utilities.EnumUtility;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/29/2014.
 */
@Named
@Singleton
public class CommonBll extends BllBase {

    /**
     * Method to get list of right for pid. It use for all table have pid.
     *
     * @param pid
     * @return List<RightDetailModel>
     */
    public List<RightDetailModel> getRightDetailByPid(Long pid) {
        List<RightDetailModel> rightList = new ArrayList<RightDetailModel>();

        if (pid != null) {
            List<SysRight> sysRightList = getRepositoryFactory().getRepositoryInstance(SysRightRepository.class).findByPid(pid);

            for (SysRight right : sysRightList) {
                Long sid = right.sid;
                String acl = right.acl;
                List<SysUser> userList = getRepositoryFactory().getRepositoryInstance(SysUserRepository.class).findBySid(sid);
                if (!userList.isEmpty())
                    rightList.add(new RightDetailModel(sid, userList.get(0).userName, EnumUtility.SID_TYPE.USER.getValue(), acl));
                else {
                    List<SysGroup> groupList = getRepositoryFactory().getRepositoryInstance(SysGroupRepository.class).findBySid(sid);
                    if (!groupList.isEmpty())
                        rightList.add(new RightDetailModel(sid, groupList.get(0).groupName, EnumUtility.SID_TYPE.GROUP.getValue(), acl));
                }
            }
        }

        return rightList;
    }

    /**
     * Get next PId Sequence
     *
     * @return Long : nextPId
     */
    public Long getNextPId() {
        BigDecimal list = getRepositoryFactory().getRepositoryInstance(CommonRepository.class).getNextPId();
        if (list == null) return new Long(0);
        return list.longValue();
    }
}
