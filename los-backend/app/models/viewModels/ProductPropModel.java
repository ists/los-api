package models.viewModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import models.baseModel.BaseModelInterface;

/**
 * Created by Administrator on 10/29/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductPropModel implements BaseModelInterface {

    public String thisType = "ProductPropModel";
    public Long productPropsId;
    public Long lastSysTranId;
    public String productId;
    public Long udfId;
    public String udfValue;
    public Long updSeq;
    public UdtmFieldModel udtmField;


    public ProductPropModel() {
    }

    public ProductPropModel(long productPropsId, Long lastSysTranId, String productId, Long udfId, String udfValue, Long updSeq, UdtmFieldModel udtmField) {
        this.productPropsId = productPropsId;
        this.lastSysTranId = lastSysTranId;
        this.productId = productId;
        this.udfId = udfId;
        this.udfValue = udfValue;
        this.updSeq = updSeq;
        this.udtmField = udtmField;
    }
}
