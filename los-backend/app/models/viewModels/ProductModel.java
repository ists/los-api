package models.viewModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import models.baseModel.BaseModelInterface;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by Administrator on 10/29/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductModel implements BaseModelInterface {

    public String thisType = "ProductModel";

    public String productId;
    public Long allowCont;
    public Long allowSub;
    public Timestamp effective;
    public Timestamp expiry;
    public Long inheritCondition;
    public Long inheritDocument;
    public Long inheritMenuAcl;
    public Long inheritParentAcl;
    public Long lastSysTranId;
    public String linkCbs;
    public Long pid;
    public String productName;
    public Long productOwner;
    public String productSlogan;
    public Long updSeq;
    public String parentId;
    public Set<ProductModel> products;
    public Set<ProductPropModel> productProps;
    public Set<SysRightModel> productRights;

    public ProductModel() {
    }

    public ProductModel(String productId, Long allowCont, Long allowSub, Timestamp effective, Timestamp expiry, Long inheritCondition, Long inheritDocument, Long inheritMenuAcl, Long inheritParentAcl, Long lastSysTranId, String linkCbs, Long pid, String productName, Long productOwner, String productSlogan, Long updSeq, String parentId, Set<ProductModel> products, Set<ProductPropModel> productProps,Set<SysRightModel> productRights) {
        this.productId = productId;
        this.allowCont = allowCont;
        this.allowSub = allowSub;
        this.effective = effective;
        this.expiry = expiry;
        this.inheritCondition = inheritCondition;
        this.inheritDocument = inheritDocument;
        this.inheritMenuAcl = inheritMenuAcl;
        this.inheritParentAcl = inheritParentAcl;
        this.lastSysTranId = lastSysTranId;
        this.linkCbs = linkCbs;
        this.pid = pid;
        this.productName = productName;
        this.productOwner = productOwner;
        this.productSlogan = productSlogan;
        this.updSeq = updSeq;
        this.parentId = parentId;
        this.products = products;
        this.productProps = productProps;
        this.productRights = productRights;
    }
}
