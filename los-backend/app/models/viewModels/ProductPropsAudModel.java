package models.viewModels;

import models.baseModel.BaseModelInterface;

import javax.persistence.Column;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/29/2014.
 */
public class ProductPropsAudModel implements BaseModelInterface {

    public String thisType = "ProductPropsAudModel";

    public Long productPropsAudId;
    public String approvalStatus;
    public Timestamp auditDate;
    public String auditFields;
    public String auditOperation;
    public Long prevSysTranId;
    public String productId;
    public Long sysTranId;
    public Long udfId;
    public String udfValue;
    public Long updSeq;

    public ProductPropsAudModel() {
    }

    public ProductPropsAudModel(long productPropsAudId, String approvalStatus, Timestamp auditDate, String auditFields, String auditOperation, Long prevSysTranId, String productId, Long sysTranId, Long udfId, String udfValue, Long updSeq) {
        this.productPropsAudId = productPropsAudId;
        this.approvalStatus = approvalStatus;
        this.auditDate = auditDate;
        this.auditFields = auditFields;
        this.auditOperation = auditOperation;
        this.prevSysTranId = prevSysTranId;
        this.productId = productId;
        this.sysTranId = sysTranId;
        this.udfId = udfId;
        this.udfValue = udfValue;
        this.updSeq = updSeq;
    }
}
