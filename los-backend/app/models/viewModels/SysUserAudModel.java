package models.viewModels;

import models.baseModel.BaseModelInterface;

import java.sql.Timestamp;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysUserAudModel implements BaseModelInterface {

    public String thisType = "SysUserAudModel";

    public Long sysUserAudId;
    public String acl;
    public String approvalStatus;
    public Timestamp auditDate;
    public String auditFields;
    public String auditOperation;
    public Long autoAuthorized;
    public String deptId;
    public Timestamp effective;
    public Timestamp expiry;
    public Timestamp lastLogin;
    public String ldapId;
    public Long maxConnections;
    public String nodeId;
    public String passCode;
    public Long prevSysTranId;
    public Long sid;
    public String status;
    public Long sysTranId;
    public Long tryCount;
    public Long updSeq;
    public String userId;
    public Long userLevel;
    public String userName;

    public SysUserAudModel() {
    }

    public SysUserAudModel(long sysUserAudId, String acl, String approvalStatus, Timestamp auditDate, String auditFields, String auditOperation, Long autoAuthorized, String deptId, Timestamp effective, Timestamp expiry, Timestamp lastLogin, String ldapId, Long maxConnections, String nodeId, String passCode, Long prevSysTranId, Long sid, String status, Long sysTranId, Long tryCount, Long updSeq, String userId, Long userLevel, String userName) {
        this.sysUserAudId = sysUserAudId;
        this.acl = acl;
        this.approvalStatus = approvalStatus;
        this.auditDate = auditDate;
        this.auditFields = auditFields;
        this.auditOperation = auditOperation;
        this.autoAuthorized = autoAuthorized;
        this.deptId = deptId;
        this.effective = effective;
        this.expiry = expiry;
        this.lastLogin = lastLogin;
        this.ldapId = ldapId;
        this.maxConnections = maxConnections;
        this.nodeId = nodeId;
        this.passCode = passCode;
        this.prevSysTranId = prevSysTranId;
        this.sid = sid;
        this.status = status;
        this.sysTranId = sysTranId;
        this.tryCount = tryCount;
        this.updSeq = updSeq;
        this.userId = userId;
        this.userLevel = userLevel;
        this.userName = userName;
    }
}
