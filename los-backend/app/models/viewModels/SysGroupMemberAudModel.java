package models.viewModels;

import models.baseModel.BaseModelInterface;

import java.sql.Timestamp;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysGroupMemberAudModel implements BaseModelInterface {

    public String thisType = "SysGroupMemberAudModel";

    public Long sysGroupMemberAudId;
    public String acl;
    public String approvalStatus;
    public Timestamp auditDate;
    public String auditFields;
    public String auditOperation;
    public Long groupSid;
    public Long memberSid;
    public Long prevSysTranId;
    public Long sysTranId;
    public Long updSeq;

    public SysGroupMemberAudModel() {
    }

    public SysGroupMemberAudModel(long sysGroupMemberAudId, String acl, String approvalStatus, Timestamp auditDate, String auditFields, String auditOperation, Long groupSid, Long memberSid, Long prevSysTranId, Long sysTranId, Long updSeq) {
        this.sysGroupMemberAudId = sysGroupMemberAudId;
        this.acl = acl;
        this.approvalStatus = approvalStatus;
        this.auditDate = auditDate;
        this.auditFields = auditFields;
        this.auditOperation = auditOperation;
        this.groupSid = groupSid;
        this.memberSid = memberSid;
        this.prevSysTranId = prevSysTranId;
        this.sysTranId = sysTranId;
        this.updSeq = updSeq;
    }
}
