package models.viewModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import models.baseModel.BaseModelInterface;

import javax.persistence.Column;

/**
 * Created by Administrator on 10/29/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UdtmFieldModel implements BaseModelInterface {

    public String thisType = "UdtmFieldModel";

    public Long udfId;
    public String acl;
    public String authStat;
    public String defaultValue;
    public String drvRule;
    public String drvRuleType;
    public String entityDescription;
    public String entityName;
    public String entityTable;
    public String entityWhereClause;
    public String fieldDescription;
    public String fieldGroupId;
    public String fieldName;
    public String fieldType;
    public String incExcFlag;
    public Long lastSysTranId;
    public String mandatory;
    public String mask;
    public Long maxLength;
    public Long maxVal;
    public Long minLength;
    public Long minVal;
    public String recordStat;
    public String uniqueField;
    public Long updSeq;
    public String updateAllowed;
    public String valRule;
    public String valRuleType;
    public String valType;

    public UdtmFieldModel() {
    }

    public UdtmFieldModel(long udfId, String acl, String authStat, String defaultValue, String drvRule, String drvRuleType, String entityDescription, String entityName, String entityTable, String entityWhereClause, String fieldDescription, String fieldGroupId, String fieldName, String fieldType, String incExcFlag, Long lastSysTranId, String mandatory, String mask, Long maxLength, Long maxVal, Long minLength, Long minVal, String recordStat, String uniqueField, Long updSeq, String updateAllowed, String valRule, String valRuleType, String valType) {
        this.udfId = udfId;
        this.acl = acl;
        this.authStat = authStat;
        this.defaultValue = defaultValue;
        this.drvRule = drvRule;
        this.drvRuleType = drvRuleType;
        this.entityDescription = entityDescription;
        this.entityName = entityName;
        this.entityTable = entityTable;
        this.entityWhereClause = entityWhereClause;
        this.fieldDescription = fieldDescription;
        this.fieldGroupId = fieldGroupId;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.incExcFlag = incExcFlag;
        this.lastSysTranId = lastSysTranId;
        this.mandatory = mandatory;
        this.mask = mask;
        this.maxLength = maxLength;
        this.maxVal = maxVal;
        this.minLength = minLength;
        this.minVal = minVal;
        this.recordStat = recordStat;
        this.uniqueField = uniqueField;
        this.updSeq = updSeq;
        this.updateAllowed = updateAllowed;
        this.valRule = valRule;
        this.valRuleType = valRuleType;
        this.valType = valType;
    }
}
