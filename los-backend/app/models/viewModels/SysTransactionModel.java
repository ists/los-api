package models.viewModels;

import models.baseModel.BaseModelInterface;

import java.sql.Timestamp;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysTransactionModel implements BaseModelInterface {

    public String thisType = "SysTransactionModel";

    public Long sysTranId;
    public String comments;
    public String dbSessionId;
    public String errCode;
    public String errDesc;
    public Long prevSysTranId;
    public String tranAction;
    public String tranCode;
    public Timestamp tranEnd;
    public Timestamp tranStart;
    public String tranStatus;
    public String userId;
    public SysUserSessionModel sysUserSession;

    public SysTransactionModel() {
    }

    public SysTransactionModel(long sysTranId, String comments, String dbSessionId, String errCode, String errDesc, Long prevSysTranId, String tranAction, String tranCode, Timestamp tranEnd, Timestamp tranStart, String tranStatus, String userId, SysUserSessionModel sysUserSession) {
        this.sysTranId = sysTranId;
        this.comments = comments;
        this.dbSessionId = dbSessionId;
        this.errCode = errCode;
        this.errDesc = errDesc;
        this.prevSysTranId = prevSysTranId;
        this.tranAction = tranAction;
        this.tranCode = tranCode;
        this.tranEnd = tranEnd;
        this.tranStart = tranStart;
        this.tranStatus = tranStatus;
        this.userId = userId;
        this.sysUserSession = sysUserSession;
    }
}
