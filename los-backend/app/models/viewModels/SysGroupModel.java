package models.viewModels;

import models.baseModel.BaseModelInterface;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysGroupModel implements BaseModelInterface {

    public String thisType = "SysGroupModel";

    public String groupId;
    public String acl;
    public String groupName;
    public Long lastSysTranId;
    public String ldapId;
    public Long sid;
    public Long updSeq;

    public SysGroupModel() {
    }

    public SysGroupModel(String groupId, String acl, String groupName, Long lastSysTranId, String ldapId, Long sid, Long updSeq) {
        this.groupId = groupId;
        this.acl = acl;
        this.groupName = groupName;
        this.lastSysTranId = lastSysTranId;
        this.ldapId = ldapId;
        this.sid = sid;
        this.updSeq = updSeq;
    }
}
