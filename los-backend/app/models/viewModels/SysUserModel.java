package models.viewModels;

import models.baseModel.BaseModelInterface;

import java.sql.Timestamp;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysUserModel implements BaseModelInterface {

    public String thisType = "SysUserModel";

    public String userId;
    public String acl;
    public Long autoAuthorized;
    public String deptId;
    public Timestamp effective;
    public Timestamp expiry;
    public Timestamp lastLogin;
    public Long lastSysTranId;
    public String ldapId;
    public Long maxConnections;
    public String nodeId;
    public String passCode;
    public Long sid;
    public String status;
    public Long tryCount;
    public Long updSeq;
    public Long userLevel;
    public String userName;

    public SysUserModel() {
    }

    public SysUserModel(String userId, String acl, Long autoAuthorized, String deptId, Timestamp effective, Timestamp expiry, Timestamp lastLogin, Long lastSysTranId, String ldapId, Long maxConnections, String nodeId, String passCode, Long sid, String status, Long tryCount, Long updSeq, Long userLevel, String userName) {
        this.userId = userId;
        this.acl = acl;
        this.autoAuthorized = autoAuthorized;
        this.deptId = deptId;
        this.effective = effective;
        this.expiry = expiry;
        this.lastLogin = lastLogin;
        this.lastSysTranId = lastSysTranId;
        this.ldapId = ldapId;
        this.maxConnections = maxConnections;
        this.nodeId = nodeId;
        this.passCode = passCode;
        this.sid = sid;
        this.status = status;
        this.tryCount = tryCount;
        this.updSeq = updSeq;
        this.userLevel = userLevel;
        this.userName = userName;
    }
}
