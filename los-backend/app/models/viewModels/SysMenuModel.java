package models.viewModels;

import models.baseModel.BaseModelInterface;

import java.util.Set;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysMenuModel implements BaseModelInterface {

    public String thisType = "SysMenuModel";

    public Long menuId;
    public Long heritableAcl;
    public Long lastSysTranId;
    public String menuName;
    public String menuRight;
    public Long pid;
    public Long updSeq;
    public String url;
    public Long parentId;
    public Set<SysMenuModel> sysMenus;

    public SysMenuModel() {
    }

    public SysMenuModel(long menuId, Long heritableAcl, Long lastSysTranId, String menuName, String menuRight, Long pid, Long updSeq, String url, Long parentId, Set<SysMenuModel> sysMenus) {
        this.menuId = menuId;
        this.heritableAcl = heritableAcl;
        this.lastSysTranId = lastSysTranId;
        this.menuName = menuName;
        this.menuRight = menuRight;
        this.pid = pid;
        this.updSeq = updSeq;
        this.url = url;
        this.parentId = parentId;
        this.sysMenus = sysMenus;
    }
}
