package models.viewModels;

import models.baseModel.BaseModelInterface;
import java.sql.Timestamp;

/**
 * Created by Administrator on 10/29/2014.
 */
public class UdtmFieldsAudModel implements BaseModelInterface {

    public String thisType = "UdtmFieldsAudModel";

    public Long udtmFieldsAudId;
    public String acl;
    public String approvalStatus;
    public Timestamp auditDate;
    public String auditFields;
    public String auditOperation;
    public String authStat;
    public String defaultValue;
    public String drvRule;
    public String drvRuleType;
    public String entityDescription;
    public String entityName;
    public String entityTable;
    public String entityWhereClause;
    public String fieldDescription;
    public String fieldGroupId;
    public String fieldName;
    public String fieldType;
    public String incExcFlag;
    public String mandatory;
    public String mask;
    public Long maxLength;
    public Long maxVal;
    public Long minLength;
    public Long minVal;
    public Long prevSysTranId;
    public String recordStat;
    public Long sysTranId;
    public Long udfId;
    public String uniqueField;
    public Long updSeq;
    public String updateAllowed;
    public String valRule;
    public String valRuleType;
    public String valType;

    public UdtmFieldsAudModel() {
    }

    public UdtmFieldsAudModel(long udtmFieldsAudId, String acl, String approvalStatus, Timestamp auditDate, String auditFields, String auditOperation, String authStat, String defaultValue, String drvRule, String drvRuleType, String entityDescription, String entityName, String entityTable, String entityWhereClause, String fieldDescription, String fieldGroupId, String fieldName, String fieldType, String incExcFlag, String mandatory, String mask, Long maxLength, Long maxVal, Long minLength, Long minVal, Long prevSysTranId, String recordStat, Long sysTranId, Long udfId, String uniqueField, Long updSeq, String updateAllowed, String valRule, String valRuleType, String valType) {
        this.udtmFieldsAudId = udtmFieldsAudId;
        this.acl = acl;
        this.approvalStatus = approvalStatus;
        this.auditDate = auditDate;
        this.auditFields = auditFields;
        this.auditOperation = auditOperation;
        this.authStat = authStat;
        this.defaultValue = defaultValue;
        this.drvRule = drvRule;
        this.drvRuleType = drvRuleType;
        this.entityDescription = entityDescription;
        this.entityName = entityName;
        this.entityTable = entityTable;
        this.entityWhereClause = entityWhereClause;
        this.fieldDescription = fieldDescription;
        this.fieldGroupId = fieldGroupId;
        this.fieldName = fieldName;
        this.fieldType = fieldType;
        this.incExcFlag = incExcFlag;
        this.mandatory = mandatory;
        this.mask = mask;
        this.maxLength = maxLength;
        this.maxVal = maxVal;
        this.minLength = minLength;
        this.minVal = minVal;
        this.prevSysTranId = prevSysTranId;
        this.recordStat = recordStat;
        this.sysTranId = sysTranId;
        this.udfId = udfId;
        this.uniqueField = uniqueField;
        this.updSeq = updSeq;
        this.updateAllowed = updateAllowed;
        this.valRule = valRule;
        this.valRuleType = valRuleType;
        this.valType = valType;
    }
}
