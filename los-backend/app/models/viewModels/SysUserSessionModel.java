package models.viewModels;

import models.baseModel.BaseModelInterface;

import java.sql.Timestamp;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysUserSessionModel implements BaseModelInterface {

    public String thisType = "SysUserSessionModel";

    public Long userSessionId;
    public String langId;
    public Timestamp logoffTime;
    public Timestamp logonTime;
    public String sessionKey;
    public String status;
    public String userId;

    public SysUserSessionModel() {
    }

    public SysUserSessionModel(long userSessionId, String langId, Timestamp logoffTime, Timestamp logonTime, String sessionKey, String status, String userId) {
        this.userSessionId = userSessionId;
        this.langId = langId;
        this.logoffTime = logoffTime;
        this.logonTime = logonTime;
        this.sessionKey = sessionKey;
        this.status = status;
        this.userId = userId;
    }
}
