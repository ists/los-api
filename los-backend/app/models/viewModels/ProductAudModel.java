package models.viewModels;

import models.baseModel.BaseModelInterface;
import models.entities.SysTransaction;

import java.sql.Timestamp;

/**
 * Created by Administrator on 10/29/2014.
 */
public class ProductAudModel implements BaseModelInterface {

    public String thisType = "ProductAudModel";

    public Long productAudId;
    public Long allowCont;
    public Long allowSub;
    public String approvalStatus;
    public Timestamp auditDate;
    public String auditFields;
    public String auditOperation;
    public Timestamp effective;
    public Timestamp expiry;
    public Long inheritCondition;
    public Long inheritDocument;
    public Long inheritMenuAcl;
    public Long inheritParentAcl;
    public String linkCbs;
    public String parentId;
    public Long pid;
    public Long prevSysTranId;
    public String productId;
    public String productName;
    public Long productOwner;
    public String productSlogan;
    public SysTransaction sysTran;
    public Long updSeq;

    public ProductAudModel(){
    }

    public ProductAudModel(long productAudId, Long allowCont, Long allowSub, String approvalStatus, Timestamp auditDate, String auditFields, String auditOperation, Timestamp effective, Timestamp expiry, Long inheritCondition, Long inheritDocument, Long inheritMenuAcl, Long inheritParentAcl, String linkCbs, String parentId, Long pid, Long prevSysTranId, String productId, String productName, Long productOwner, String productSlogan, SysTransaction sysTran, Long updSeq) {
        this.productAudId = productAudId;
        this.allowCont = allowCont;
        this.allowSub = allowSub;
        this.approvalStatus = approvalStatus;
        this.auditDate = auditDate;
        this.auditFields = auditFields;
        this.auditOperation = auditOperation;
        this.effective = effective;
        this.expiry = expiry;
        this.inheritCondition = inheritCondition;
        this.inheritDocument = inheritDocument;
        this.inheritMenuAcl = inheritMenuAcl;
        this.inheritParentAcl = inheritParentAcl;
        this.linkCbs = linkCbs;
        this.parentId = parentId;
        this.pid = pid;
        this.prevSysTranId = prevSysTranId;
        this.productId = productId;
        this.productName = productName;
        this.productOwner = productOwner;
        this.productSlogan = productSlogan;
        this.sysTran = sysTran;
        this.updSeq = updSeq;
    }
}
