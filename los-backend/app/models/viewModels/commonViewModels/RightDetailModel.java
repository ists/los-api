/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.viewModels.commonViewModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import models.baseModel.BaseModelInterface;

/**
 * @author Administrator
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RightDetailModel implements BaseModelInterface {

    public Long sid;
    public String name;
    public int type;
    public String acl;

    public RightDetailModel() {}

    public RightDetailModel(Long sid, String name, int type, String acl) {
        this.sid = sid;
        this.name = name;
        this.type = type;
        this.acl = acl;
    }
}
