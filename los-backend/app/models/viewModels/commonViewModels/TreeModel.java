package models.viewModels.commonViewModels;

import models.baseModel.BaseModelInterface;
import models.entities.Product;

/**
 * Created by Administrator on 10/29/2014.
 */
public class TreeModel implements BaseModelInterface {

    public String id;
    public String name;
    public String parentId;

    public TreeModel() {
    }

    public TreeModel(String id, String name, String parentId) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
    }

    public TreeModel(Long id, String name, Long parentId) {
        this.id = id.toString();
        this.name = name;
        this.parentId = parentId.toString();
    }

    public TreeModel(Product product) {
        this.id = product.productId;
        this.name = product.productName;
        this.parentId = product.parentId;
    }
}
