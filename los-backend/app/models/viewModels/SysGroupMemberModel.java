package models.viewModels;

import models.baseModel.BaseModelInterface;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysGroupMemberModel implements BaseModelInterface {

    public String thisType = "SysGroupMemberModel";

    public Long sysGroupMemberId;
    public String acl;
    public Long groupSid;
    public Long lastSysTranId;
    public Long memberSid;
    public Long updSeq;

    public SysGroupMemberModel() {
    }

    public SysGroupMemberModel(long sysGroupMemberId, String acl, Long groupSid, Long lastSysTranId, Long memberSid, Long updSeq) {
        this.sysGroupMemberId = sysGroupMemberId;
        this.acl = acl;
        this.groupSid = groupSid;
        this.lastSysTranId = lastSysTranId;
        this.memberSid = memberSid;
        this.updSeq = updSeq;
    }
}
