package models.viewModels;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import models.baseModel.BaseModelInterface;

/**
 * Created by Administrator on 11/3/2014.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SysRightModel implements BaseModelInterface {

    public String thisType = "SysRightModel";

    public Long rightId;
    public String acl;
    public Long pid;
    public Long sid;

    public SysRightModel() {
    }

    public SysRightModel(long rightId, String acl, Long pid, Long sid) {
        this.rightId = rightId;
        this.acl = acl;
        this.pid = pid;
        this.sid = sid;
    }
}
