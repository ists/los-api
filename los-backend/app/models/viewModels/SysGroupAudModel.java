package models.viewModels;

import models.baseModel.BaseModelInterface;

import java.sql.Timestamp;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysGroupAudModel implements BaseModelInterface {

    public String thisType = "SysGroupAudModel";

    public Long sysGroupAudId;
    public String acl;
    public String approvalStatus;
    public Timestamp auditDate;
    public String auditFields;
    public String auditOperation;
    public String groupId;
    public String groupName;
    public String ldapId;
    public Long prevSysTranId;
    public Long sid;
    public Long sysTranId;
    public Long updSeq;

    public SysGroupAudModel() {
    }

    public SysGroupAudModel(long sysGroupAudId, String acl, String approvalStatus, Timestamp auditDate, String auditFields, String auditOperation, String groupId, String groupName, String ldapId, Long prevSysTranId, Long sid, Long sysTranId, Long updSeq) {
        this.sysGroupAudId = sysGroupAudId;
        this.acl = acl;
        this.approvalStatus = approvalStatus;
        this.auditDate = auditDate;
        this.auditFields = auditFields;
        this.auditOperation = auditOperation;
        this.groupId = groupId;
        this.groupName = groupName;
        this.ldapId = ldapId;
        this.prevSysTranId = prevSysTranId;
        this.sid = sid;
        this.sysTranId = sysTranId;
        this.updSeq = updSeq;
    }
}
