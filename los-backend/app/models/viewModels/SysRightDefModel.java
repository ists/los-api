package models.viewModels;

import models.baseModel.BaseModelInterface;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysRightDefModel implements BaseModelInterface {

    public String rightCode;
    public String rightDesc;

    public SysRightDefModel() {
    }

    public SysRightDefModel(String rightCode, String rightDesc) {
        this.rightCode = rightCode;
        this.rightDesc = rightDesc;
    }
}
