package models.viewModels;

import models.baseModel.BaseModelInterface;

import java.sql.Timestamp;

/**
 * Created by Administrator on 11/11/2014.
 */
public class SysRightAudModel implements BaseModelInterface {
    public String thisType = "SysRightAudModel";
    public Long sysRightAudId;
    public Long sysTranId;
    public Timestamp auditDate;
    public String auditFields;
    public String auditOperation;
    public String approvalStatus;
    public Long prevSysTranId;
    public Long updSeq;
    public Long rightId;
    public Long sid;
    public Long pid;
    public String acl;

    public SysRightAudModel(Long sysRightAudId, Long sysTranId, Timestamp auditDate, String auditFields, String auditOperation, String approvalStatus, Long prevSysTranId, Long updSeq, Long rightId, Long sid, Long pid, String acl) {
        this.sysRightAudId = sysRightAudId;
        this.sysTranId = sysTranId;
        this.auditDate = auditDate;
        this.auditFields = auditFields;
        this.auditOperation = auditOperation;
        this.approvalStatus = approvalStatus;
        this.prevSysTranId = prevSysTranId;
        this.updSeq = updSeq;
        this.rightId = rightId;
        this.sid = sid;
        this.pid = pid;
        this.acl = acl;
    }

    public SysRightAudModel() {
    }
}
