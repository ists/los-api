package models.viewModels;

import models.baseModel.BaseModelInterface;

/**
 * Created by Administrator on 11/3/2014.
 */
public class SysTranCodeModel implements BaseModelInterface {

    public String thisType = "SysTranCodeModel";

    public String tranCode;
    public Long menuId;
    public String tranDesc;

    public SysTranCodeModel() {
    }

    public SysTranCodeModel(String tranCode, Long menuId, String tranDesc) {
        this.tranCode = tranCode;
        this.menuId = menuId;
        this.tranDesc = tranDesc;
    }
}
