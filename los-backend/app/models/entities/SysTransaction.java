package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the SYS_TRANSACTION database table.
 */
@Entity
@Table(name = "SYS_TRANSACTION")
public class SysTransaction implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "SYS_TRANSACTION_SYSTRANID_GENERATOR", sequenceName = "SQ_SYS_TRAN_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYS_TRANSACTION_SYSTRANID_GENERATOR")
    @Column(name = "SYS_TRAN_ID", unique = true, nullable = false, precision = 20)
    public Long sysTranId;

    @Column(name = "COMMENTS", length = 256)
    public String comments;

    @Column(name = "DB_SESSION_ID", length = 20)
    public String dbSessionId;

    @Column(name = "ERR_CODE", length = 10)
    public String errCode;

    @Column(name = "ERR_DESC", length = 4000)
    public String errDesc;

    @Column(name = "PREV_SYS_TRAN_ID")
    public Long prevSysTranId;

    @Column(name = "TRAN_ACTION", length = 1)
    public String tranAction;

    @Column(name = "TRAN_CODE", length = 30)
    public String tranCode;

    @Column(name = "TRAN_END")
    public Timestamp tranEnd;

    @Column(name = "TRAN_START")
    public Timestamp tranStart;

    @Column(name = "TRAN_STATUS", length = 1)
    public String tranStatus;

    @Column(name = "USER_ID", nullable = false, length = 20)
    public String userId;

    //bi-directional many-to-one association to SysUserSession
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_SESSION_ID")
    public SysUserSession sysUserSession;

    public SysTransaction() {
    }

    public Long getSysTranId() {
        return this.sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDbSessionId() {
        return this.dbSessionId;
    }

    public void setDbSessionId(String dbSessionId) {
        this.dbSessionId = dbSessionId;
    }

    public String getErrCode() {
        return this.errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrDesc() {
        return this.errDesc;
    }

    public void setErrDesc(String errDesc) {
        this.errDesc = errDesc;
    }

    public Long getPrevSysTranId() {
        return this.prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    public String getTranAction() {
        return this.tranAction;
    }

    public void setTranAction(String tranAction) {
        this.tranAction = tranAction;
    }

    public String getTranCode() {
        return this.tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    public Timestamp getTranEnd() {
        return this.tranEnd;
    }

    public void setTranEnd(Timestamp tranEnd) {
        this.tranEnd = tranEnd;
    }

    public Timestamp getTranStart() {
        return this.tranStart;
    }

    public void setTranStart(Timestamp tranStart) {
        this.tranStart = tranStart;
    }

    public String getTranStatus() {
        return this.tranStatus;
    }

    public void setTranStatus(String tranStatus) {
        this.tranStatus = tranStatus;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public SysUserSession getSysUserSession() {
        return this.sysUserSession;
    }

    public void setSysUserSession(SysUserSession sysUserSession) {
        this.sysUserSession = sysUserSession;
    }

}