package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;


/**
 * The persistent class for the PRODUCT_PROPS database table.
 *
 */
@Entity
@Table(name="PRODUCT_PROPS")
public class ProductProp implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="PRODUCT_PROPS_PRODUCTPROPSID_GENERATOR", sequenceName="SQ_PRODUCT_PROPS_ID", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PRODUCT_PROPS_PRODUCTPROPSID_GENERATOR")
    @Column(name="PRODUCT_PROPS_ID", unique=true, nullable=false)
    public Long productPropsId;

    @Column(name="LAST_SYS_TRAN_ID", precision=20)
    public Long lastSysTranId;

    @Column(name="PRODUCT_ID", length=20)
    public String productId;

    @Column(name="UDF_VALUE", length=100)
    public String udfValue;

    @Column(name="UPD_SEQ")
    public Long updSeq;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="UDF_ID")
    public UdtmField udtmField;

    public ProductProp() {
    }

    public Long getProductPropsId() {
        return this.productPropsId;
    }

    public void setProductPropsId(Long productPropsId) {
        this.productPropsId = productPropsId;
    }

    public Long getLastSysTranId() {
        return this.lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUdfValue() {
        return this.udfValue;
    }

    public void setUdfValue(String udfValue) {
        this.udfValue = udfValue;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    public UdtmField getUdtmField() {
        return this.udtmField;
    }

    public void setUdtmField(UdtmField udtmField) {
        this.udtmField = udtmField;
    }

}