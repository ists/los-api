package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the SYS_GROUP_MEMBER_AUD database table.
 */
@Entity
@Table(name = "SYS_GROUP_MEMBER_AUD")
public class SysGroupMemberAud implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "SYS_GROUP_MEMBER_AUD_SYSGROUPMEMBERAUDID_GENERATOR", sequenceName = "SQ_SYS_GROUP_MEMBER_AUD_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYS_GROUP_MEMBER_AUD_SYSGROUPMEMBERAUDID_GENERATOR")
    @Column(name = "SYS_GROUP_MEMBER_AUD_ID", unique = true, nullable = false)
    public Long sysGroupMemberAudId;

    @Column(name = "ACL", length = 200)
    public String acl;

    @Column(name = "APPROVAL_STATUS", length = 1)
    public String approvalStatus;

    @Column(name = "AUDIT_DATE")
    public Timestamp auditDate;

    @Column(name = "AUDIT_FIELDS", length = 25)
    public String auditFields;

    @Column(name = "AUDIT_OPERATION", length = 1)
    public String auditOperation;

    @Column(name = "GROUP_SID")
    public Long groupSid;

    @Column(name = "MEMBER_SID")
    public Long memberSid;

    @Column(name = "PREV_SYS_TRAN_ID")
    public Long prevSysTranId;

    @Column(name = "SYS_TRAN_ID")
    public Long sysTranId;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    public SysGroupMemberAud() {
    }

    public Long getSysGroupMemberAudId() {
        return this.sysGroupMemberAudId;
    }

    public void setSysGroupMemberAudId(Long sysGroupMemberAudId) {
        this.sysGroupMemberAudId = sysGroupMemberAudId;
    }

    public String getAcl() {
        return this.acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public String getApprovalStatus() {
        return this.approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Timestamp getAuditDate() {
        return this.auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditFields() {
        return this.auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    public String getAuditOperation() {
        return this.auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    public Long getGroupSid() {
        return this.groupSid;
    }

    public void setGroupSid(Long groupSid) {
        this.groupSid = groupSid;
    }

    public Long getMemberSid() {
        return this.memberSid;
    }

    public void setMemberSid(Long memberSid) {
        this.memberSid = memberSid;
    }

    public Long getPrevSysTranId() {
        return this.prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    public Long getSysTranId() {
        return this.sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

}