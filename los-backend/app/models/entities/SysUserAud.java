package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the SYS_USER_AUD database table.
 */
@Entity
@Table(name = "SYS_USER_AUD")
public class SysUserAud implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "SYS_USER_AUD_SYSUSERAUDID_GENERATOR", sequenceName = "SQ_SYS_USER_AUD_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYS_USER_AUD_SYSUSERAUDID_GENERATOR")
    @Column(name = "SYS_USER_AUD_ID", unique = true, nullable = false)
    public Long sysUserAudId;

    @Column(name = "ACL", length = 50)
    public String acl;

    @Column(name = "APPROVAL_STATUS", length = 1)
    public String approvalStatus;

    @Column(name = "AUDIT_DATE")
    public Timestamp auditDate;

    @Column(name = "AUDIT_FIELDS", length = 148)
    public String auditFields;

    @Column(name = "AUDIT_OPERATION", length = 1)
    public String auditOperation;

    @Column(name = "AUTO_AUTHORIZED", precision = 1)
    public Long autoAuthorized;

    @Column(name = "DEPT_ID", length = 5)
    public String deptId;

    @Column(name = "EFFECTIVE")
    public Timestamp effective;

    @Column(name = "EXPIRY")
    public Timestamp expiry;

    @Column(name = "LAST_LOGIN")
    public Timestamp lastLogin;

    @Column(name = "LDAP_ID", length = 400)
    public String ldapId;

    @Column(name = "MAX_CONNECTIONS")
    public Long maxConnections;

    @Column(name = "NODE_ID", length = 20)
    public String nodeId;

    @Column(name = "PASS_CODE", length = 50)
    public String passCode;

    @Column(name = "PREV_SYS_TRAN_ID")
    public Long prevSysTranId;

    @Column(name = "SID", precision = 10)
    public Long sid;

    @Column(name = "STATUS", length = 1)
    public String status;

    @Column(name = "SYS_TRAN_ID")
    public Long sysTranId;

    @Column(name = "TRY_COUNT", precision = 4)
    public Long tryCount;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    @Column(name = "USER_ID", length = 20)
    public String userId;

    @Column(name = "USER_LEVEL", precision = 2)
    public Long userLevel;

    @Column(name = "USER_NAME", length = 200)
    public String userName;

    public SysUserAud() {
    }

    public Long getSysUserAudId() {
        return this.sysUserAudId;
    }

    public void setSysUserAudId(Long sysUserAudId) {
        this.sysUserAudId = sysUserAudId;
    }

    public String getAcl() {
        return this.acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public String getApprovalStatus() {
        return this.approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Timestamp getAuditDate() {
        return this.auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditFields() {
        return this.auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    public String getAuditOperation() {
        return this.auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    public Long getAutoAuthorized() {
        return this.autoAuthorized;
    }

    public void setAutoAuthorized(Long autoAuthorized) {
        this.autoAuthorized = autoAuthorized;
    }

    public String getDeptId() {
        return this.deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public Timestamp getEffective() {
        return this.effective;
    }

    public void setEffective(Timestamp effective) {
        this.effective = effective;
    }

    public Timestamp getExpiry() {
        return this.expiry;
    }

    public void setExpiry(Timestamp expiry) {
        this.expiry = expiry;
    }

    public Timestamp getLastLogin() {
        return this.lastLogin;
    }

    public void setLastLogin(Timestamp lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getLdapId() {
        return this.ldapId;
    }

    public void setLdapId(String ldapId) {
        this.ldapId = ldapId;
    }

    public Long getMaxConnections() {
        return this.maxConnections;
    }

    public void setMaxConnections(Long maxConnections) {
        this.maxConnections = maxConnections;
    }

    public String getNodeId() {
        return this.nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getPassCode() {
        return this.passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    public Long getPrevSysTranId() {
        return this.prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    public Long getSid() {
        return this.sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getSysTranId() {
        return this.sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    public Long getTryCount() {
        return this.tryCount;
    }

    public void setTryCount(Long tryCount) {
        this.tryCount = tryCount;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getUserLevel() {
        return this.userLevel;
    }

    public void setUserLevel(Long userLevel) {
        this.userLevel = userLevel;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}