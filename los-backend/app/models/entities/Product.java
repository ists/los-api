package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;


/**
 * The persistent class for the PRODUCT database table.
 */
@Entity
@Table(name = "PRODUCT")
public class Product implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "PRODUCT_ID", unique = true, nullable = false, length = 20)
    public String productId;

    @Column(name = "ALLOW_CONT")
    public Long allowCont;

    @Column(name = "ALLOW_SUB")
    public Long allowSub;

    @Column(name = "EFFECTIVE")
    public Timestamp effective;

    @Column(name = "EXPIRY")
    public Timestamp expiry;

    @Column(name = "INHERIT_CONDITION")
    public Long inheritCondition;

    @Column(name = "INHERIT_DOCUMENT")
    public Long inheritDocument;

    @Column(name = "INHERIT_MENU_ACL")
    public Long inheritMenuAcl;

    @Column(name = "INHERIT_PARENT_ACL")
    public Long inheritParentAcl;

    @Column(name = "LAST_SYS_TRAN_ID", precision = 20)
    public Long lastSysTranId;

    @Column(name = "LINK_CBS", length = 30)
    public String linkCbs;

    @Column(name = "PID")
    public Long pid;

    @Column(name = "PRODUCT_NAME", length = 50)
    public String productName;

    @Column(name = "PRODUCT_OWNER", precision = 5)
    public Long productOwner;

    @Column(name = "PRODUCT_SLOGAN", length = 100)
    public String productSlogan;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

//    //bi-directional many-to-one association to Product
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "PARENT_ID")
//    public Product product;

    @Column(name = "PARENT_ID")
    public String parentId;

    //bi-directional many-to-one association to Product
//    @OneToMany(mappedBy = "product")
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "PRODUCT_ID")
    public Set<Product> products;

    //bi-directional many-to-one association to ProductProp
//    @OneToMany(mappedBy = "product")
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "PRODUCT_ID")
    public Set<ProductProp> productProps;

    public Product() {
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getAllowCont() {
        return this.allowCont;
    }

    public void setAllowCont(Long allowCont) {
        this.allowCont = allowCont;
    }

    public Long getAllowSub() {
        return this.allowSub;
    }

    public void setAllowSub(Long allowSub) {
        this.allowSub = allowSub;
    }

    public Timestamp getEffective() {
        return this.effective;
    }

    public void setEffective(Timestamp effective) {
        this.effective = effective;
    }

    public Timestamp getExpiry() {
        return this.expiry;
    }

    public void setExpiry(Timestamp expiry) {
        this.expiry = expiry;
    }

    public Long getInheritCondition() {
        return this.inheritCondition;
    }

    public void setInheritCondition(Long inheritCondition) {
        this.inheritCondition = inheritCondition;
    }

    public Long getInheritDocument() {
        return this.inheritDocument;
    }

    public void setInheritDocument(Long inheritDocument) {
        this.inheritDocument = inheritDocument;
    }

    public Long getInheritMenuAcl() {
        return this.inheritMenuAcl;
    }

    public void setInheritMenuAcl(Long inheritMenuAcl) {
        this.inheritMenuAcl = inheritMenuAcl;
    }

    public Long getInheritParentAcl() {
        return this.inheritParentAcl;
    }

    public void setInheritParentAcl(Long inheritParentAcl) {
        this.inheritParentAcl = inheritParentAcl;
    }

    public Long getLastSysTranId() {
        return this.lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    public String getLinkCbs() {
        return this.linkCbs;
    }

    public void setLinkCbs(String linkCbs) {
        this.linkCbs = linkCbs;
    }

    public Long getPid() {
        return this.pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getProductOwner() {
        return this.productOwner;
    }

    public void setProductOwner(Long productOwner) {
        this.productOwner = productOwner;
    }

    public String getProductSlogan() {
        return this.productSlogan;
    }

    public void setProductSlogan(String productSlogan) {
        this.productSlogan = productSlogan;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

//    public Product getProduct() {
//        return this.product;
//    }
//
//    public void setProduct(Product product) {
//        this.product = product;
//    }

    public Set<Product> getProducts() {
        return this.products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

//    public Product addProduct(Product product) {
//        getProducts().add(product);
//        product.setProduct(this);
//
//        return product;
//    }
//
//    public Product removeProduct(Product product) {
//        getProducts().remove(product);
//        product.setProduct(null);
//
//        return product;
//    }

    public Set<ProductProp> getProductProps() {
        return this.productProps;
    }

    public void setProductProps(Set<ProductProp> productProps) {
        this.productProps = productProps;
    }

//    public ProductProp addProductProp(ProductProp productProp) {
//        getProductProps().add(productProp);
//        productProp.setProduct(this);
//
//        return productProp;
//    }
//
//    public ProductProp removeProductProp(ProductProp productProp) {
//        getProductProps().remove(productProp);
//        productProp.setProduct(null);
//
//        return productProp;
//    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}