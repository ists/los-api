package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;


/**
 * The persistent class for the UDTM_FIELDS database table.
 */
@Entity
@Table(name = "UDTM_FIELDS")
public class UdtmField implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "UDTM_FIELDS_UDFID_GENERATOR", sequenceName = "SQ_UDF_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UDTM_FIELDS_UDFID_GENERATOR")
    @Column(name = "UDF_ID", unique = true, nullable = false)
    public Long udfId;

    @Column(name = "ACL", length = 50)
    public String acl;

    @Column(name = "AUTH_STAT", length = 1)
    public String authStat;

    @Column(name = "DEFAULT_VALUE", length = 150)
    public String defaultValue;

    @Column(name = "DRV_RULE", length = 4000)
    public String drvRule;

    @Column(name = "DRV_RULE_TYPE", length = 4)
    public String drvRuleType;

    @Column(name = "ENTITY_DESCRIPTION", length = 30)
    public String entityDescription;

    @Column(name = "ENTITY_NAME", length = 30)
    public String entityName;

    @Column(name = "ENTITY_TABLE", length = 30)
    public String entityTable;

    @Column(name = "ENTITY_WHERE_CLAUSE", length = 1000)
    public String entityWhereClause;

    @Column(name = "FIELD_DESCRIPTION", length = 500)
    public String fieldDescription;

    @Column(name = "FIELD_GROUP_ID", length = 105)
    public String fieldGroupId;

    @Column(name = "FIELD_NAME", length = 105)
    public String fieldName;

    @Column(name = "FIELD_TYPE", length = 1)
    public String fieldType;

    @Column(name = "INC_EXC_FLAG", length = 1)
    public String incExcFlag;

    @Column(name = "LAST_SYS_TRAN_ID", precision = 20)
    public Long lastSysTranId;

    @Column(name = "MANDATORY", length = 1)
    public String mandatory;

    @Column(name = "MASK", length = 25)
    public String mask;

    @Column(name = "MAX_LENGTH", precision = 4)
    public Long maxLength;

    @Column(name = "MAX_VAL", precision = 20, scale = 3)
    public Long maxVal;

    @Column(name = "MIN_LENGTH", precision = 4)
    public Long minLength;

    @Column(name = "MIN_VAL", precision = 20, scale = 3)
    public Long minVal;

    @Column(name = "RECORD_STAT", length = 1)
    public String recordStat;

    @Column(name = "UNIQUE_FIELD", length = 1)
    public String uniqueField;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    @Column(name = "UPDATE_ALLOWED", length = 1)
    public String updateAllowed;

    @Column(name = "VAL_RULE", length = 4000)
    public String valRule;

    @Column(name = "VAL_RULE_TYPE", length = 1)
    public String valRuleType;

    @Column(name = "VAL_TYPE", length = 3)
    public String valType;

    //bi-directional many-to-one association to ProductProp
//    @OneToMany(mappedBy = "udtmField")
//    public Set<ProductProp> productProps;

    public UdtmField() {
    }

    public Long getUdfId() {
        return this.udfId;
    }

    public void setUdfId(Long udfId) {
        this.udfId = udfId;
    }

    public String getAcl() {
        return this.acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public String getAuthStat() {
        return this.authStat;
    }

    public void setAuthStat(String authStat) {
        this.authStat = authStat;
    }

    public String getDefaultValue() {
        return this.defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDrvRule() {
        return this.drvRule;
    }

    public void setDrvRule(String drvRule) {
        this.drvRule = drvRule;
    }

    public String getDrvRuleType() {
        return this.drvRuleType;
    }

    public void setDrvRuleType(String drvRuleType) {
        this.drvRuleType = drvRuleType;
    }

    public String getEntityDescription() {
        return this.entityDescription;
    }

    public void setEntityDescription(String entityDescription) {
        this.entityDescription = entityDescription;
    }

    public String getEntityName() {
        return this.entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityTable() {
        return this.entityTable;
    }

    public void setEntityTable(String entityTable) {
        this.entityTable = entityTable;
    }

    public String getEntityWhereClause() {
        return this.entityWhereClause;
    }

    public void setEntityWhereClause(String entityWhereClause) {
        this.entityWhereClause = entityWhereClause;
    }

    public String getFieldDescription() {
        return this.fieldDescription;
    }

    public void setFieldDescription(String fieldDescription) {
        this.fieldDescription = fieldDescription;
    }

    public String getFieldGroupId() {
        return this.fieldGroupId;
    }

    public void setFieldGroupId(String fieldGroupId) {
        this.fieldGroupId = fieldGroupId;
    }

    public String getFieldName() {
        return this.fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return this.fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getIncExcFlag() {
        return this.incExcFlag;
    }

    public void setIncExcFlag(String incExcFlag) {
        this.incExcFlag = incExcFlag;
    }

    public Long getLastSysTranId() {
        return this.lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    public String getMandatory() {
        return this.mandatory;
    }

    public void setMandatory(String mandatory) {
        this.mandatory = mandatory;
    }

    public String getMask() {
        return this.mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public Long getMaxLength() {
        return this.maxLength;
    }

    public void setMaxLength(Long maxLength) {
        this.maxLength = maxLength;
    }

    public Long getMaxVal() {
        return this.maxVal;
    }

    public void setMaxVal(Long maxVal) {
        this.maxVal = maxVal;
    }

    public Long getMinLength() {
        return this.minLength;
    }

    public void setMinLength(Long minLength) {
        this.minLength = minLength;
    }

    public Long getMinVal() {
        return this.minVal;
    }

    public void setMinVal(Long minVal) {
        this.minVal = minVal;
    }

    public String getRecordStat() {
        return this.recordStat;
    }

    public void setRecordStat(String recordStat) {
        this.recordStat = recordStat;
    }

    public String getUniqueField() {
        return this.uniqueField;
    }

    public void setUniqueField(String uniqueField) {
        this.uniqueField = uniqueField;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    public String getUpdateAllowed() {
        return this.updateAllowed;
    }

    public void setUpdateAllowed(String updateAllowed) {
        this.updateAllowed = updateAllowed;
    }

    public String getValRule() {
        return this.valRule;
    }

    public void setValRule(String valRule) {
        this.valRule = valRule;
    }

    public String getValRuleType() {
        return this.valRuleType;
    }

    public void setValRuleType(String valRuleType) {
        this.valRuleType = valRuleType;
    }

    public String getValType() {
        return this.valType;
    }

    public void setValType(String valType) {
        this.valType = valType;
    }

//    public Set<ProductProp> getProductProps() {
//        return this.productProps;
//    }
//
//    public void setProductProps(Set<ProductProp> productProps) {
//        this.productProps = productProps;
//    }
//
//    public ProductProp addProductProp(ProductProp productProp) {
//        getProductProps().add(productProp);
//        productProp.setUdtmField(this);
//
//        return productProp;
//    }
//
//    public ProductProp removeProductProp(ProductProp productProp) {
//        getProductProps().remove(productProp);
//        productProp.setUdtmField(null);
//
//        return productProp;
//    }

}