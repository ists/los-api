package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;


/**
 * The persistent class for the SYS_USER_SESSION database table.
 */
@Entity
@Table(name = "SYS_USER_SESSION")
public class SysUserSession implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "USER_SESSION_ID", unique = true, nullable = false)
    public Long userSessionId;

    @Column(name = "LANG_ID", length = 5)
    public String langId;

    
    @Column(name = "LOGOFF_TIME")
    public Timestamp logoffTime;

    
    @Column(name = "LOGON_TIME")
    public Timestamp logonTime;

    @Column(name = "SESSION_KEY", length = 50)
    public String sessionKey;

    @Column(name = "STATUS", length = 1)
    public String status;

    @Column(name = "USER_ID", length = 20)
    public String userId;

//    //bi-directional many-to-one association to SysTransaction
//    @OneToMany(mappedBy = "sysUserSession")
//    public Set<SysTransaction> sysTransactions;

    public SysUserSession() {
    }

    public Long getUserSessionId() {
        return this.userSessionId;
    }

    public void setUserSessionId(Long userSessionId) {
        this.userSessionId = userSessionId;
    }

    public String getLangId() {
        return this.langId;
    }

    public void setLangId(String langId) {
        this.langId = langId;
    }

    public Timestamp getLogoffTime() {
        return this.logoffTime;
    }

    public void setLogoffTime(Timestamp logoffTime) {
        this.logoffTime = logoffTime;
    }

    public Timestamp getLogonTime() {
        return this.logonTime;
    }

    public void setLogonTime(Timestamp logonTime) {
        this.logonTime = logonTime;
    }

    public String getSessionKey() {
        return this.sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

//    public Set<SysTransaction> getSysTransactions() {
//        return this.sysTransactions;
//    }
//
//    public void setSysTransactions(Set<SysTransaction> sysTransactions) {
//        this.sysTransactions = sysTransactions;
//    }
//
//    public SysTransaction addSysTransaction(SysTransaction sysTransaction) {
//        getSysTransactions().add(sysTransaction);
//        sysTransaction.setSysUserSession(this);
//
//        return sysTransaction;
//    }
//
//    public SysTransaction removeSysTransaction(SysTransaction sysTransaction) {
//        getSysTransactions().remove(sysTransaction);
//        sysTransaction.setSysUserSession(null);
//
//        return sysTransaction;
//    }

}