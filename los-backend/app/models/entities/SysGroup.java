package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the SYS_GROUP database table.
 */
@Entity
@Table(name = "SYS_GROUP")
public class SysGroup implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "GROUP_ID", unique = true, nullable = false, length = 20)
    public String groupId;

    @Column(name = "ACL", length = 50)
    public String acl;

    @Column(name = "GROUP_NAME", length = 100)
    public String groupName;

    @Column(name = "LAST_SYS_TRAN_ID", precision = 20)
    public Long lastSysTranId;

    @Column(name = "LDAP_ID", length = 100)
    public String ldapId;

    @Column(name = "SID", precision = 10)
    public Long sid;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    public SysGroup() {
    }

    public String getGroupId() {
        return this.groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getAcl() {
        return this.acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getLastSysTranId() {
        return this.lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    public String getLdapId() {
        return this.ldapId;
    }

    public void setLdapId(String ldapId) {
        this.ldapId = ldapId;
    }

    public Long getSid() {
        return this.sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

}