package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;


/**
 * The persistent class for the SYS_USER database table.
 */
@Entity
@Table(name = "SYS_USER")
public class SysUser implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "USER_ID", unique = true, nullable = false, length = 20)
    public String userId;

    @Column(length = 50)
    public String acl;

    @Column(name = "AUTO_AUTHORIZED", precision = 1)
    public Long autoAuthorized;

    @Column(name = "DEPT_ID", length = 5)
    public String deptId;

    @Column(name = "EFFECTIVE")
    public Timestamp effective;

    @Column(name = "EXPIRY")
    public Timestamp expiry;

    @Column(name = "LAST_LOGIN")
    public Timestamp lastLogin;

    @Column(name = "LAST_SYS_TRAN_ID", precision = 20)
    public Long lastSysTranId;

    @Column(name = "LDAP_ID", length = 100)
    public String ldapId;

    @Column(name = "MAX_CONNECTIONS")
    public Long maxConnections;

    @Column(name = "NODE_ID", length = 20)
    public String nodeId;

    @Column(name = "PASS_CODE", length = 50)
    public String passCode;

    @Column(name = "SID")
    public Long sid;

    @Column(name = "STATUS", length = 1)
    public String status;

    @Column(name = "TRY_COUNT", precision = 4)
    public Long tryCount;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    @Column(name = "USER_LEVEL", precision = 2)
    public Long userLevel;

    @Column(name = "USER_NAME", length = 50)
    public String userName;

    public SysUser() {
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAcl() {
        return this.acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public Long getAutoAuthorized() {
        return this.autoAuthorized;
    }

    public void setAutoAuthorized(Long autoAuthorized) {
        this.autoAuthorized = autoAuthorized;
    }

    public String getDeptId() {
        return this.deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public Timestamp getEffective() {
        return this.effective;
    }

    public void setEffective(Timestamp effective) {
        this.effective = effective;
    }

    public Timestamp getExpiry() {
        return this.expiry;
    }

    public void setExpiry(Timestamp expiry) {
        this.expiry = expiry;
    }

    public Timestamp getLastLogin() {
        return this.lastLogin;
    }

    public void setLastLogin(Timestamp lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Long getLastSysTranId() {
        return this.lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    public String getLdapId() {
        return this.ldapId;
    }

    public void setLdapId(String ldapId) {
        this.ldapId = ldapId;
    }

    public Long getMaxConnections() {
        return this.maxConnections;
    }

    public void setMaxConnections(Long maxConnections) {
        this.maxConnections = maxConnections;
    }

    public String getNodeId() {
        return this.nodeId;
    }

    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    public String getPassCode() {
        return this.passCode;
    }

    public void setPassCode(String passCode) {
        this.passCode = passCode;
    }

    public Long getSid() {
        return this.sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTryCount() {
        return this.tryCount;
    }

    public void setTryCount(Long tryCount) {
        this.tryCount = tryCount;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    public Long getUserLevel() {
        return this.userLevel;
    }

    public void setUserLevel(Long userLevel) {
        this.userLevel = userLevel;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}