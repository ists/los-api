package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;


/**
 * The persistent class for the SYS_RIGHT database table.
 */
@Entity
@Table(name = "SYS_RIGHT")
public class SysRight implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "SYS_RIGHT_RIGHTID_GENERATOR", sequenceName = "SQ_SYS_RIGHT_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYS_RIGHT_RIGHTID_GENERATOR")
    @Column(name = "RIGHT_ID", unique = true, nullable = false)
    public Long rightId;

    @Column(name = "ACL", length = 50)
    public String acl;

    @Column(name = "PID")
    public Long pid;

    @Column(name = "SID")
    public Long sid;

    public SysRight() {
    }

    public Long getRightId() {
        return this.rightId;
    }

    public void setRightId(Long rightId) {
        this.rightId = rightId;
    }

    public String getAcl() {
        return this.acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public Long getPid() {
        return this.pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getSid() {
        return this.sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

}