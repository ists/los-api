package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the SYS_GROUP_AUD database table.
 */
@Entity
@Table(name = "SYS_GROUP_AUD")
public class SysGroupAud implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "SYS_GROUP_AUD_SYSGROUPAUDID_GENERATOR", sequenceName = "SQ_SYS_GROUP_AUD_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYS_GROUP_AUD_SYSGROUPAUDID_GENERATOR")
    @Column(name = "SYS_GROUP_AUD_ID", unique = true, nullable = false)
    public Long sysGroupAudId;

    @Column(name = "ACL", length = 50)
    public String acl;

    @Column(name = "APPROVAL_STATUS", length = 1)
    public String approvalStatus;

    @Column(name = "AUDIT_DATE")
    public Timestamp auditDate;

    @Column(name = "AUDIT_FIELDS", length = 36)
    public String auditFields;

    @Column(name = "AUDIT_OPERATION", length = 1)
    public String auditOperation;

    @Column(name = "GROUP_ID", length = 20)
    public String groupId;

    @Column(name = "GROUP_NAME", length = 400)
    public String groupName;

    @Column(name = "LDAP_ID", length = 400)
    public String ldapId;

    @Column(name = "PREV_SYS_TRAN_ID")
    public Long prevSysTranId;

    @Column(name = "SID", precision = 10)
    public Long sid;

    @Column(name = "SYS_TRAN_ID")
    public Long sysTranId;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    public SysGroupAud() {
    }

    public Long getSysGroupAudId() {
        return this.sysGroupAudId;
    }

    public void setSysGroupAudId(Long sysGroupAudId) {
        this.sysGroupAudId = sysGroupAudId;
    }

    public String getAcl() {
        return this.acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public String getApprovalStatus() {
        return this.approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Timestamp getAuditDate() {
        return this.auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditFields() {
        return this.auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    public String getAuditOperation() {
        return this.auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    public String getGroupId() {
        return this.groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getLdapId() {
        return this.ldapId;
    }

    public void setLdapId(String ldapId) {
        this.ldapId = ldapId;
    }

    public Long getPrevSysTranId() {
        return this.prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    public Long getSid() {
        return this.sid;
    }

    public void setSid(Long sid) {
        this.sid = sid;
    }

    public Long getSysTranId() {
        return this.sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

}