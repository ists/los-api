package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the PRODUCT_PROPS_AUD database table.
 */
@Entity
@Table(name = "PRODUCT_PROPS_AUD")
public class ProductPropsAud implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "PRODUCT_PROPS_AUD_PRODUCTPROPSAUDID_GENERATOR", sequenceName = "SQ_PRODUCT_PROPS_AUD_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCT_PROPS_AUD_PRODUCTPROPSAUDID_GENERATOR")
    @Column(name = "PRODUCT_PROPS_AUD_ID", unique = true, nullable = false)
    public Long productPropsAudId;

    @Column(name = "APPROVAL_STATUS", length = 1)
    public String approvalStatus;

    @Column(name = "AUDIT_DATE")
    public Timestamp auditDate;

    @Column(name = "AUDIT_FIELDS", length = 28)
    public String auditFields;

    @Column(name = "AUDIT_OPERATION", length = 1)
    public String auditOperation;

    @Column(name = "PREV_SYS_TRAN_ID")
    public Long prevSysTranId;

    @Column(name = "PRODUCT_ID", length = 20)
    public String productId;

    @Column(name = "SYS_TRAN_ID")
    public Long sysTranId;

    @Column(name = "UDF_ID", precision = 6)
    public Long udfId;

    @Column(name = "UDF_VALUE", length = 400)
    public String udfValue;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    public ProductPropsAud() {
    }

    public Long getProductPropsAudId() {
        return this.productPropsAudId;
    }

    public void setProductPropsAudId(Long productPropsAudId) {
        this.productPropsAudId = productPropsAudId;
    }

    public String getApprovalStatus() {
        return this.approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Timestamp getAuditDate() {
        return this.auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditFields() {
        return this.auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    public String getAuditOperation() {
        return this.auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    public Long getPrevSysTranId() {
        return this.prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getSysTranId() {
        return this.sysTranId;
    }

    public void setSysTranId(Long sysTranId) {
        this.sysTranId = sysTranId;
    }

    public Long getUdfId() {
        return this.udfId;
    }

    public void setUdfId(Long udfId) {
        this.udfId = udfId;
    }

    public String getUdfValue() {
        return this.udfValue;
    }

    public void setUdfValue(String udfValue) {
        this.udfValue = udfValue;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

}