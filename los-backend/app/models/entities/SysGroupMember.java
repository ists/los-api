package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;


/**
 * The persistent class for the SYS_GROUP_MEMBER database table.
 *
 */
@Entity
@Table(name="SYS_GROUP_MEMBER")
public class SysGroupMember implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="SYS_GROUP_MEMBER_SYSGROUPMEMBERID_GENERATOR", sequenceName="SQ_SYS_GROUP_MEMBER_ID", allocationSize = 1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SYS_GROUP_MEMBER_SYSGROUPMEMBERID_GENERATOR")
    @Column(name="SYS_GROUP_MEMBER_ID", unique=true, nullable=false)
    public Long sysGroupMemberId;

    @Column(name = "ACL", length=50)
    public String acl;

    @Column(name="GROUP_SID")
    public Long groupSid;

    @Column(name="LAST_SYS_TRAN_ID", precision=20)
    public Long lastSysTranId;

    @Column(name="MEMBER_SID")
    public Long memberSid;

    @Column(name="UPD_SEQ", precision=8)
    public Long updSeq;

    public SysGroupMember() {
    }

    public Long getSysGroupMemberId() {
        return this.sysGroupMemberId;
    }

    public void setSysGroupMemberId(Long sysGroupMemberId) {
        this.sysGroupMemberId = sysGroupMemberId;
    }

    public String getAcl() {
        return this.acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public Long getGroupSid() {
        return this.groupSid;
    }

    public void setGroupSid(Long groupSid) {
        this.groupSid = groupSid;
    }

    public Long getLastSysTranId() {
        return this.lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    public Long getMemberSid() {
        return this.memberSid;
    }

    public void setMemberSid(Long memberSid) {
        this.memberSid = memberSid;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

}