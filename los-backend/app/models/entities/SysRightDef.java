package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the SYS_RIGHT_DEF database table.
 */
@Entity
@Table(name = "SYS_RIGHT_DEF")
public class SysRightDef implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "RIGHT_CODE", unique = true, nullable = false, length = 1)
    public String rightCode;

    @Column(name = "RIGHT_DESC", length = 50)
    public String rightDesc;

    public SysRightDef() {
    }

    public String getRightCode() {
        return this.rightCode;
    }

    public void setRightCode(String rightCode) {
        this.rightCode = rightCode;
    }

    public String getRightDesc() {
        return this.rightDesc;
    }

    public void setRightDesc(String rightDesc) {
        this.rightDesc = rightDesc;
    }

}