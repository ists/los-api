package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the SYS_TRAN_CODE database table.
 */
@Entity
@Table(name = "SYS_TRAN_CODE")
public class SysTranCode implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "TRAN_CODE", unique = true, nullable = false, length = 30)
    public String tranCode;

    @Column(name = "MENU_ID")
    public Long menuId;

    @Column(name = "TRAN_DESC", length = 100)
    public String tranDesc;

    public SysTranCode() {
    }

    public String getTranCode() {
        return this.tranCode;
    }

    public void setTranCode(String tranCode) {
        this.tranCode = tranCode;
    }

    public Long getMenuId() {
        return this.menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getTranDesc() {
        return this.tranDesc;
    }

    public void setTranDesc(String tranDesc) {
        this.tranDesc = tranDesc;
    }

}