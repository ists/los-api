package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the SYS_MENU database table.
 */
@Entity
@Table(name = "SYS_MENU")
public class SysMenu implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "SYS_MENU_MENUID_GENERATOR", sequenceName = "SQ_MENU_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SYS_MENU_MENUID_GENERATOR")
    @Column(name = "MENU_ID", unique = true, nullable = false)
    public Long menuId;

    @Column(name = "HERITABLE_ACL")
    public Long heritableAcl;

    @Column(name = "LAST_SYS_TRAN_ID", precision = 20)
    public Long lastSysTranId;

    @Column(name = "MENU_NAME", length = 50)
    public String menuName;

    @Column(name = "MENU_RIGHT", length = 50)
    public String menuRight;

    @Column(name = "PID")
    public Long pid;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    @Column(name = "URL", length = 255)
    public String url;

    @Column(name = "PARENT_ID")
    public Long parentId;

//    //bi-directional many-to-one association to SysMenu
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "PARENT_ID")
//    public SysMenu sysMenu;

    //bi-directional many-to-one association to SysMenu
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "MENU_ID")
    public Set<SysMenu> sysMenus;

    public SysMenu() {
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getMenuId() {
        return this.menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public Long getHeritableAcl() {
        return this.heritableAcl;
    }

    public void setHeritableAcl(Long heritableAcl) {
        this.heritableAcl = heritableAcl;
    }

    public Long getLastSysTranId() {
        return this.lastSysTranId;
    }

    public void setLastSysTranId(Long lastSysTranId) {
        this.lastSysTranId = lastSysTranId;
    }

    public String getMenuName() {
        return this.menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuRight() {
        return this.menuRight;
    }

    public void setMenuRight(String menuRight) {
        this.menuRight = menuRight;
    }

    public Long getPid() {
        return this.pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

//    public SysMenu getSysMenu() {
//        return this.sysMenu;
//    }
//
//    public void setSysMenu(SysMenu sysMenu) {
//        this.sysMenu = sysMenu;
//    }

    public Set<SysMenu> getSysMenus() {
        return this.sysMenus;
    }

    public void setSysMenus(Set<SysMenu> sysMenus) {
        this.sysMenus = sysMenus;
    }

//    public SysMenu addSysMenus(SysMenu sysMenus) {
//        getSysMenus().add(sysMenus);
//        sysMenus.setSysMenu(this);
//
//        return sysMenus;
//    }
//
//    public SysMenu removeSysMenus(SysMenu sysMenus) {
//        getSysMenus().remove(sysMenus);
//        sysMenus.setSysMenu(null);
//
//        return sysMenus;
//    }

}