package models.entities;

import models.baseModel.BaseModelInterface;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the PRODUCT_AUD database table.
 */
@Entity
@Table(name = "PRODUCT_AUD")
public class ProductAud implements BaseModelInterface {
    public static final Long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "PRODUCT_AUD_PRODUCTAUDID_GENERATOR", sequenceName = "SQ_PRODUCT_AUD_ID", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PRODUCT_AUD_PRODUCTAUDID_GENERATOR")
    @Column(name = "PRODUCT_AUD_ID", unique = true, nullable = false)
    public Long productAudId;

    @Column(name = "ALLOW_CONT")
    public Long allowCont;

    @Column(name = "ALLOW_SUB")
    public Long allowSub;

    @Column(name = "APPROVAL_STATUS", length = 1)
    public String approvalStatus;

    @Column(name = "AUDIT_DATE")
    public Timestamp auditDate;

    @Column(name = "AUDIT_FIELDS", length = 1024)
    public String auditFields;

    @Column(name = "AUDIT_OPERATION", length = 1)
    public String auditOperation;

    @Column(name = "EFFECTIVE")
    public Timestamp effective;

    @Column(name = "EXPIRY")
    public Timestamp expiry;

    @Column(name = "INHERIT_CONDITION")
    public Long inheritCondition;

    @Column(name = "INHERIT_DOCUMENT")
    public Long inheritDocument;

    @Column(name = "INHERIT_MENU_ACL")
    public Long inheritMenuAcl;

    @Column(name = "INHERIT_PARENT_ACL")
    public Long inheritParentAcl;

    @Column(name = "LINK_CBS", length = 120)
    public String linkCbs;

    @Column(name = "PARENT_ID", length = 20)
    public String parentId;

    @Column(name = "PID")
    public Long pid;

    @Column(name = "PREV_SYS_TRAN_ID")
    public Long prevSysTranId;

    @Column(name = "PRODUCT_ID", length = 20)
    public String productId;

    @Column(name = "PRODUCT_NAME", length = 200)
    public String productName;

    @Column(name = "PRODUCT_OWNER", precision = 5)
    public Long productOwner;

    @Column(name = "PRODUCT_SLOGAN", length = 400)
    public String productSlogan;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SYS_TRAN_ID")
    public SysTransaction sysTran;

//    @Column(name = "SYS_TRAN_ID")
//    public Long sysTranId;

    @Column(name = "UPD_SEQ", precision = 8)
    public Long updSeq;

    public ProductAud() {
    }

    public Long getProductAudId() {
        return this.productAudId;
    }

    public void setProductAudId(Long productAudId) {
        this.productAudId = productAudId;
    }

    public Long getAllowCont() {
        return this.allowCont;
    }

    public void setAllowCont(Long allowCont) {
        this.allowCont = allowCont;
    }

    public Long getAllowSub() {
        return this.allowSub;
    }

    public void setAllowSub(Long allowSub) {
        this.allowSub = allowSub;
    }

    public String getApprovalStatus() {
        return this.approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Timestamp getAuditDate() {
        return this.auditDate;
    }

    public void setAuditDate(Timestamp auditDate) {
        this.auditDate = auditDate;
    }

    public String getAuditFields() {
        return this.auditFields;
    }

    public void setAuditFields(String auditFields) {
        this.auditFields = auditFields;
    }

    public String getAuditOperation() {
        return this.auditOperation;
    }

    public void setAuditOperation(String auditOperation) {
        this.auditOperation = auditOperation;
    }

    public Timestamp getEffective() {
        return this.effective;
    }

    public void setEffective(Timestamp effective) {
        this.effective = effective;
    }

    public Timestamp getExpiry() {
        return this.expiry;
    }

    public void setExpiry(Timestamp expiry) {
        this.expiry = expiry;
    }

    public Long getInheritCondition() {
        return this.inheritCondition;
    }

    public void setInheritCondition(Long inheritCondition) {
        this.inheritCondition = inheritCondition;
    }

    public Long getInheritDocument() {
        return this.inheritDocument;
    }

    public void setInheritDocument(Long inheritDocument) {
        this.inheritDocument = inheritDocument;
    }

    public Long getInheritMenuAcl() {
        return this.inheritMenuAcl;
    }

    public void setInheritMenuAcl(Long inheritMenuAcl) {
        this.inheritMenuAcl = inheritMenuAcl;
    }

    public Long getInheritParentAcl() {
        return this.inheritParentAcl;
    }

    public void setInheritParentAcl(Long inheritParentAcl) {
        this.inheritParentAcl = inheritParentAcl;
    }

    public String getLinkCbs() {
        return this.linkCbs;
    }

    public void setLinkCbs(String linkCbs) {
        this.linkCbs = linkCbs;
    }

    public String getParentId() {
        return this.parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Long getPid() {
        return this.pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getPrevSysTranId() {
        return this.prevSysTranId;
    }

    public void setPrevSysTranId(Long prevSysTranId) {
        this.prevSysTranId = prevSysTranId;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getProductOwner() {
        return this.productOwner;
    }

    public void setProductOwner(Long productOwner) {
        this.productOwner = productOwner;
    }

    public String getProductSlogan() {
        return this.productSlogan;
    }

    public void setProductSlogan(String productSlogan) {
        this.productSlogan = productSlogan;
    }

    public SysTransaction getSysTran() {
        return this.sysTran;
    }

    public void setSysTran(SysTransaction sysTran) {
        this.sysTran = sysTran;
    }

//    public Long getSysTranId() {
//        return this.sysTranId;
//    }
//
//    public void setSysTran(Long sysTranId) {
//        this.sysTranId = sysTranId;
//    }

    public Long getUpdSeq() {
        return this.updSeq;
    }

    public void setUpdSeq(Long updSeq) {
        this.updSeq = updSeq;
    }

}