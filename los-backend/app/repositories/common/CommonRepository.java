package repositories.common;

import models.entities.SysTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Administrator on 10/30/2014.
 */
public interface CommonRepository extends Repository<SysTransaction, Long> {

    @Query(value = "SELECT SQ_PID.NEXTVAL from Dual", nativeQuery = true)
    public BigDecimal getNextPId();
}
