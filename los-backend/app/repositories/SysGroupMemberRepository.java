package repositories;

import models.entities.SysGroupMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Administrator on 10/29/2014.
 */
public interface SysGroupMemberRepository extends JpaRepository<SysGroupMember, Long> {
    List<SysGroupMember> findByMemberSid(Long memberSid);

    @Query(value = "select g.groupSid from SysGroupMember g where g.memberSid = :memberSid")
    List<Long> findGroupSidByMemberSid(@Param("memberSid") Long memberSid);
}
