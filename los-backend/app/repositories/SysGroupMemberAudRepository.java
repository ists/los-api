package repositories;

import models.entities.SysGroupMemberAud;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface SysGroupMemberAudRepository extends JpaRepository<SysGroupMemberAud, Long> {
}
