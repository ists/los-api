package repositories;

import models.entities.SysTranCode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface SysTranCodeRepository extends JpaRepository<SysTranCode, String> {
}
