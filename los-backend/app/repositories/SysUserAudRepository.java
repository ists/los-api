package repositories;

import models.entities.SysUserAud;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface SysUserAudRepository extends JpaRepository<SysUserAud, Long> {
}
