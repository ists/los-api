package repositories;

import models.entities.UdtmField;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface UdtmFieldRepository extends JpaRepository<UdtmField, Long> {
    List<UdtmField> findByFieldGroupId(String fieldGroupId);
}
