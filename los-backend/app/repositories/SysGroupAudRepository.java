package repositories;

import models.entities.SysGroupAud;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface SysGroupAudRepository extends JpaRepository<SysGroupAud, Long> {
}
