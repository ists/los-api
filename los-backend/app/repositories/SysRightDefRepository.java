package repositories;

import models.entities.SysRightDef;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface SysRightDefRepository extends JpaRepository<SysRightDef, String> {
}
