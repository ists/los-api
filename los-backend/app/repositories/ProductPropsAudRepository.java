package repositories;

import models.entities.ProductPropsAud;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface ProductPropsAudRepository extends JpaRepository<ProductPropsAud, Long> {
    List<ProductPropsAud> findByProductIdAndSysTranId(String productId, Long sysTranId);
}
