package repositories;

import models.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Administrator on 10/29/2014.
 */
public interface ProductRepository extends JpaRepository<Product, String> {
    @Query(value = "select p.pid from Product p where p.productId = :productId")
    List<Long> findPidByProductId(@Param("productId") String productId);

    List<Product> findByProductId(String productId);

    List<Product> findByAllowSub(Long allowSub);
}
