package repositories;

import models.entities.ProductProp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface ProductPropRepository extends JpaRepository<ProductProp, Long> {
    List<ProductProp> findByProductId(String productId);

//    @Modifying
//    @Query(value = "delete from ProductProp where productId = :productId")
    @Transactional
    Long deleteByProductId(String productId);
}
