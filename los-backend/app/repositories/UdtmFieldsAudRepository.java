package repositories;

import models.entities.UdtmFieldsAud;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface UdtmFieldsAudRepository extends JpaRepository<UdtmFieldsAud, Long> {
}
