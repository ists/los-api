package repositories;

import models.entities.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 10/29/2014.
 */
public interface SysUserRepository extends JpaRepository<SysUser, String> {
    List<SysUser> findBySid(Long sid);

    List<SysUser> findByStatus(String status);
}
