package repositories;

import models.entities.SysMenu;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 10/30/2014.
 */
public interface SysMenuRepository extends JpaRepository<SysMenu, Long> {

}
