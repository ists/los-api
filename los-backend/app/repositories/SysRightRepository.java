package repositories;

        import models.entities.SysRight;
        import org.springframework.data.jpa.repository.JpaRepository;
        import org.springframework.data.jpa.repository.Query;
        import org.springframework.data.repository.query.Param;
        import org.springframework.transaction.annotation.Transactional;

        import java.util.List;

/**
 * Created by Administrator on 10/29/2014.
 */
public interface SysRightRepository extends JpaRepository<SysRight, Long> {
    List<SysRight> findBySidAndPid(Long sid, Long pid);

    @Query(value = "select r.acl from SysRight r where r.sid = :sid and r.pid = :pid")
    List<String> findAclBySidAndPid(@Param("sid") Long sid, @Param("pid") Long pid);

    @Query(value = "select r.sid from SysRight r where r.pid = :pid")
    List<Long> findSidByPid(@Param("pid") Long pid);

    List<SysRight> findByPid(Long pid);

    @Transactional
    Long deleteByPid(Long pid);
}
