package repositories;

import models.entities.SysUserSession;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 11/6/2014.
 */
public interface SysUserSessionRepository extends JpaRepository<SysUserSession, Long> {
}
