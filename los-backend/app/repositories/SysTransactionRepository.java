package repositories;

import models.entities.SysTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 *
 */
@Named
@Singleton
public interface SysTransactionRepository  extends JpaRepository<SysTransaction, Long> {
    @Query(value = "select sys_context('USERENV','SID') AS SID from dual", nativeQuery = true)
    String getDBSession();
}
