package repositories;

import models.entities.SysGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 10/29/2014.
 */
public interface SysGroupRepository extends JpaRepository<SysGroup, String> {
    List<SysGroup> findBySid(Long sid);
}
