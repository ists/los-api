package repositories;

import models.entities.ProductAud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Administrator on 11/3/2014.
 */
public interface ProductAudRepository extends JpaRepository<ProductAud, Long> {
    @Query(value = "select p from ProductAud p inner join p.sysTran t where t.tranStatus = 'A' order by p.auditDate desc ")
    List<ProductAud> findProductAudForApproval();
}
