package repositories;

import models.entities.SysRightAud;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 11/12/2014.
 */
public interface SysRightAudRepository extends JpaRepository<SysRightAud, Long> {
    List<SysRightAud> findByPidAndSysTranId(Long pid, Long sysTranId);
}
