package utilities;

/**
 * Created by Administrator on 10/29/2014.
 */
public class EnumUtility {

    public static enum TRAN_ACTION {
        INPUT("I"),
        EDIT_DRAFT("E"),
        REVIEW("R"),
        APPROVE("A"),
        REJECT("J"),
        INQUIRY("Q");

        private String value;

        private TRAN_ACTION(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static enum TRAN_STATUS {
        ACTIVE("A"),
        CLOSE("C"),
        ERROR("E"),
        REJECT("J"),
        APPROVED("P");

        private String value;

        private TRAN_STATUS(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static enum SYS_RIGHT_DEF {
        VIEW("V"),
        INPUT("I"),
        UPDATE("U"),
        DELETE("D"),
        CHANGE_PARENT("E"),
        APPROVE("A");

        private String value;

        private SYS_RIGHT_DEF(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static enum BOOLEAN_CHAR {
        YES("Y"),
        NO("N");

        private String value;

        private BOOLEAN_CHAR(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }

    public static enum BOOLEAN_NUMBER {
        YES(1),
        NO(0);

        private int value;

        private BOOLEAN_NUMBER(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public static enum SID_TYPE {
        USER(1),
        GROUP(2);

        private final int value;

        private SID_TYPE(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public static enum AUDIT_OPERATION {
        INSERT,UPDATE,DELETE;

        public String toString() {
            switch (this) {
                case INSERT:
                    return "I";
                case UPDATE:
                    return "U";
                case DELETE:
                    return "D";
            }
            return super.toString();
        }

        public String getValue() {
            return toString();
        }
    }

    public static enum APPROVAL_STATUS {
        INPUT,REVIEW,APPROVED,ERROR,REJECT;

        public String toString() {
            switch (this) {
                case INPUT:
                    return "I";
                case REVIEW:
                    return "U";
                case APPROVED:
                    return "A";
                case ERROR:
                    return "E";
                case REJECT:
                    return "J";
            }
            return super.toString();
        }

        public String getValue() {
            return toString();
        }
    }

    public static enum ACL_ENUM {
        VIEW,EDIT,DELETE,APPROVE,CREATE,SUB_TREE,USE;

        public String toString() {
            switch (this) {
                case VIEW:
                    return "V";
                case EDIT:
                    return "E";
                case DELETE:
                    return "D";
                case APPROVE:
                    return "A";
                case CREATE:
                    return "C";
                case SUB_TREE:
                    return "S";
                case USE:
                    return "U";
            }
            return super.toString();
        }

        public String getValue() {
            return toString();
        }
    }

    public static enum UDTM_TYPE {
        PRODUCT("PRODUCT");

        private String value;

        private UDTM_TYPE(String value) {
            this.value = value;
        }

        public String toString() {
            return this.value;
        }
    }
}
