package utilities;

import javax.management.modelmbean.InvalidTargetObjectTypeException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10/24/2014.
 */
public class CommonUtility {
    public static <T1 extends Object, T2 extends Object> List<T2> copyListObjects(
            List<T1> sourceList
            , Class<? extends T1> sourceClass
            , Class<? extends T2> targetClass) throws Exception {
        List<T2> resultList = new ArrayList<>();
        for (T1 sourceObj : sourceList) {
            T2 targetObj = (T2) targetClass.newInstance();
            targetObj = ObjectUtility.MultiClassesUtility.copyObjectProperties(
                    sourceObj,
                    sourceClass,
                    targetObj,
                    targetClass
            );

            resultList.add(targetObj);
        }
        return resultList;
    }
}
