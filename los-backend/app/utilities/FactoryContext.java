package utilities;

import blls.base.BllBase;
import factory.business.BusinessLayerFactory;
import factory.repository.RepositoryFactory;
import factory.transaction.TransactionFactory;
import org.reflections.Reflections;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;
import transactions.base.TransactionBase;

import java.lang.reflect.ParameterizedType;
import java.util.Set;

/**
 * Created by Trung on 11/5/2014.
 */
public class FactoryContext {
    private String[] transactionFactoryPackages;
    private String[] repositoryFactoryPackages;
    private String[] businessFactoryPackages;
    public static AnnotationConfigApplicationContext ctx;

    public void setBusinessFactoryPackages(String... businessFactoryPackages) {
        this.businessFactoryPackages = businessFactoryPackages;
    }

    public void setRepositoryFactoryPackages(String... repositoryFactoryPackages) {
        this.repositoryFactoryPackages = repositoryFactoryPackages;
    }

    public void setTransactionFactoryPackages(String... transactionFactoryPackages) {
        this.transactionFactoryPackages = transactionFactoryPackages;
    }

    public void populateFactories(AnnotationConfigApplicationContext ctx) {
        this.ctx = ctx;
//        Reflections reflections = new Reflections(transactionFactoryPackages[0]);
//        TransactionFactory.transactionClasses = reflections.getSubTypesOf(TransactionBase.class);
//
//        reflections = new Reflections(businessFactoryPackages[0]);
//        BusinessLayerFactory.businessClasses = reflections.getSubTypesOf(BllBase.class);

//        reflections = new Reflections(repositoryFactoryPackages[0]);
//        RepositoryFactory.repositoryClasses = reflections.getSubTypesOf(JpaRepository.class);
    }
}
