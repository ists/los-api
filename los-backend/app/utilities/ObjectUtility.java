package utilities;

import javax.persistence.Column;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
public class ObjectUtility {

    private static boolean isSet(Object obj) {
        try {
            Set target = (Set) obj;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static <S1 extends Collection> boolean isType(Object targetObj, Class<S1> clazz) {
        try {
            S1 tmp = (S1) targetObj;
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static enum ARRAY_COLECTION_ENUM {
        SKIP_ARRAY_TYPE,ARRAY_TYPE_SET,ARRAY_TYPE_LIST;

        public <T extends Collection> Class<T> getValue() {
            switch (this) {
                case ARRAY_TYPE_SET:
                    return (Class<T>) HashSet.class;
                case ARRAY_TYPE_LIST:
                    return (Class<T>) ArrayList.class;
            }
            return null;
        }
    }

    public static enum ACCEPT_DIFFERENT_TYPE_ENUM {
        ACCEPT_DIFFERENT_TYPE(true),
        SKIP_DIFFERENT_TYPE(false);

        private boolean value;

        private ACCEPT_DIFFERENT_TYPE_ENUM(boolean value) {
            this.value = value;
        }

        public boolean getValue() {
            return value;
        }

    }

    public static class MultiClassesUtility {

        /**
         * Copy sourceObject's components to targetObject's components
         * Default:
         * ACCEPT_DIFFERENT_TYPE_ENUM = ACCEPT_DIFFERENT_TYPE_ENUM.ACCEPT_DIFFERENT_TYPE;
         * typeArrayClass = ARRAY_COLECTION_ENUM.ARRAY_TYPE_SET(interface: SET)
         *
         * @param sourceObject
         * @param sourceClass
         * @param targetObject
         * @param targetClass
         * @param <T1>
         * @param <T2>
         * @param <S1>
         * @return
         * @throws Exception
         */
        public static <T1 extends Object, T2 extends Object, S1 extends Collection> T2 copyObjectProperties(
                T1 sourceObject,
                Class<? extends T1> sourceClass,
                T2 targetObject,
                Class<? extends T2> targetClass) throws Exception {
            return copyObjectProperties(
                    sourceObject,
                    sourceClass,
                    targetObject,
                    targetClass,
                    ARRAY_COLECTION_ENUM.ARRAY_TYPE_SET.getValue(),
                    ACCEPT_DIFFERENT_TYPE_ENUM.ACCEPT_DIFFERENT_TYPE.getValue());
        }

        /**
         * Copy sourceObject's components to targetObject's components
         * Default:
         * typeArrayClass = ARRAY_COLECTION_ENUM.ARRAY_TYPE_SET
         *
         * @param sourceObject
         * @param sourceClass
         * @param targetObject
         * @param targetClass
         * @param acceptDifferentType
         * @param <T1>
         * @param <T2>
         * @param <S1>
         * @return targetObject with new value of properties
         * @throws Exception
         */
        public static <T1 extends Object, T2 extends Object, S1 extends Collection> T2 copyObjectProperties(
                T1 sourceObject,
                Class<? extends T1> sourceClass,
                T2 targetObject,
                Class<? extends T2> targetClass,
                ACCEPT_DIFFERENT_TYPE_ENUM acceptDifferentType) throws Exception {
            return copyObjectProperties(
                    sourceObject,
                    sourceClass,
                    targetObject,
                    targetClass,
                    ARRAY_COLECTION_ENUM.ARRAY_TYPE_SET.getValue(),
                    acceptDifferentType.getValue());
        }

        /**
         * Copy sourceObject's components to targetObject's components
         *
         * @param sourceObject
         * @param sourceClass
         * @param targetObject
         * @param targetClass
         * @param typeArrayClass
         * @param <T1>
         * @param <T2>
         * @param <S1>
         * @return targetObject with new value of properties
         * @throws Exception
         */
        public static <T1 extends Object, T2 extends Object, S1 extends Collection> T2 copyObjectProperties(
                T1 sourceObject,
                Class<? extends T1> sourceClass,
                T2 targetObject,
                Class<? extends T2> targetClass,
                ARRAY_COLECTION_ENUM typeArrayClass) throws Exception {
            return copyObjectProperties(
                    sourceObject,
                    sourceClass,
                    targetObject,
                    targetClass,
                    typeArrayClass.getValue(),
                    ACCEPT_DIFFERENT_TYPE_ENUM.ACCEPT_DIFFERENT_TYPE.getValue());
        }

        /**
         * Copy sourceObject's components to targetObject's components
         *
         * @param sourceObject
         * @param sourceClass
         * @param targetObject
         * @param targetClass
         * @param typeArrayClass
         * @param acceptDifferentType
         * @param <T1>
         * @param <T2>
         * @param <S1>
         * @return targetObject with new value of properties
         * @throws Exception
         */
        public static <T1 extends Object, T2 extends Object, S1 extends Collection> T2 copyObjectProperties(
                T1 sourceObject,
                Class<? extends T1> sourceClass,
                T2 targetObject,
                Class<? extends T2> targetClass,
                ARRAY_COLECTION_ENUM typeArrayClass,
                ACCEPT_DIFFERENT_TYPE_ENUM acceptDifferentType) throws Exception {
            return copyObjectProperties(
                    sourceObject,
                    sourceClass,
                    targetObject,
                    targetClass,
                    typeArrayClass.getValue(),
                    acceptDifferentType.getValue());
        }

        /**
         * Copy sourceObject's components to targetObject's components
         *
         * @param sourceObject
         * @param sourceClass
         * @param targetObject
         * @param targetClass
         * @param typeArrayClass
         * @param acceptDifferentType
         * ObjectUtility.MultiClassesUtility.ACCEPT_DIFFERENT_TYPE
         * ObjectUtility.MultiClassesUtility.SKIP_DIFFERENT_TYPE
         * @param <T1>
         * @param <T2>
         * @param <S1>
         * @return targetObject with new value of properties
         * @throws Exception
         */
        private static <T1 extends Object, T2 extends Object, S1 extends Collection> T2 copyObjectProperties(
                T1 sourceObject,
                Class<? extends T1> sourceClass,
                T2 targetObject,
                Class<? extends T2> targetClass,
                Class<? extends S1> typeArrayClass,
                boolean acceptDifferentType) throws Exception {

            if (sourceObject == null) {
                return targetObject;
            }
            try {
                targetObject = (targetObject != null) ? targetObject : (T2) targetClass.newInstance();
            } catch (Exception ex) {
                throw new Exception("The class " + targetClass.getName() + " must has Constructor() method");
            }

            try {
                Field[] sourceObjectFields = (sourceObject != null) ? sourceObject.getClass().getDeclaredFields() : new Field[0];
                Field[] targetObjectFields = (targetObject != null) ? targetObject.getClass().getDeclaredFields() : new Field[0];

                for (Field targetField : targetObjectFields) {
                    if (Modifier.isFinal(targetField.getModifiers()))
                        continue;

                    for (Field sourceField : sourceObjectFields) {
                        // khong cung ten -> continue
                        if (!targetField.getName().equalsIgnoreCase(sourceField.getName())) {
                            continue;
                        }
                        // Set accessible cho cac field de truy cap tranh loi private
                        targetField.setAccessible(true);
                        sourceField.setAccessible(true);

                        // neu khac kieu du lieu
                        if (!sourceField.getType().getName().equalsIgnoreCase(targetField.getType().getName())) {
                            if (acceptDifferentType) {
                                targetField.set(targetObject, copyObjectProperties(
                                        sourceField.get(sourceObject),
                                        sourceField.getType(),
                                        targetField.get(targetObject),
                                        targetField.getType(),
                                        typeArrayClass,
                                        acceptDifferentType));
                            }
                            break;
                        }

                        Object objSourceField = sourceField.get(sourceObject);
                        if (objSourceField == null) {
                            break;
                        }

                        // Neu la Iterable (List, Set, Collection, ...)
                        if (isType(objSourceField, typeArrayClass)) {
                            if (typeArrayClass == null) break;

                            if (typeArrayClass.isInterface()) {
                                throw new Exception("The type of array class: " + typeArrayClass.getName() + " is an interface (Use ArrayList, Vector or HashSet ...)\n");
                            }
                            Class sourceFieldGenericClass = null;
                            Class targetFieldGenericClass = null;
                            try {
                                sourceFieldGenericClass = (Class) ((ParameterizedType) sourceField.getGenericType()).getActualTypeArguments()[0];
                                targetFieldGenericClass = (Class) ((ParameterizedType) targetField.getGenericType()).getActualTypeArguments()[0];
                            } catch (Exception e) {
                                break; // Neu 1 trong 2 ko Generic -> break
                            }

                            // Lay List trong target ra hoac tao moi neu null
                            S1 objTargetField = (targetField.get(targetObject) != null) ? (S1) targetField.get(targetObject)
                                    : typeArrayClass.newInstance();

                            S1 objSourceSet = (S1) objSourceField;
                            for (Object tmpSourceObject : objSourceSet) {
                                try {
                                    Object tmpTargetObject = targetFieldGenericClass.newInstance();
                                    tmpTargetObject = copyObjectProperties(
                                            tmpSourceObject,
                                            sourceFieldGenericClass,
                                            tmpTargetObject,
                                            targetFieldGenericClass,
                                            typeArrayClass,
                                            acceptDifferentType);
                                    objTargetField.add(tmpTargetObject);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw new Exception("The class " + targetFieldGenericClass.getName() + " must has Constructor() method \n" + e.toString());
                                }
                            }
                            targetField.set(targetObject, objTargetField);
                            break;
                        }

                        targetField.set(targetObject, objSourceField);
                        break;

                    }
                }

            } catch (Exception ex) {
                throw ex;
            }
            return targetObject;
        }
    }

    public static class SingleClassUtility {

        public static <T extends Object> List<String> compareAnnotationMethods(T obj1, T obj2) throws Exception {
            List<String> list = new ArrayList<String>();
            try {
                Method[] methods = obj1.getClass().getMethods();

                for (Method method : methods) {
                    method.setAccessible(true);

                    Column column = method.getAnnotation(Column.class);
                    if (column == null) {
                        continue;
                    }

                    if (!method.invoke(obj1, null).equals(method.invoke(obj2, null))) {
                        list.add(column.name());
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }
            return list;
        }

        public static <T extends Object> String compareAnnotationMethodsToString(T obj1, T obj2) throws Exception {
            String list = "";
            try {
                Method[] methods = obj1.getClass().getMethods();

                for (Method method : methods) {
                    method.setAccessible(true);

                    Column column = method.getAnnotation(Column.class);
                    if (column == null) {
                        continue;
                    }

                    if (!method.invoke(obj1, null).equals(method.invoke(obj2, null))) {
                        list += column.name() + " ";
                    }
                }
            } catch (Exception ex) {
                throw ex;
            }
            return list.trim().replace(" ", ";");
        }

        public static <T extends Object> List<String> getAllAnnotationName(Class<T> tClass) throws Exception {
            List<String> list = new ArrayList<String>();

            try {
                Method[] methods = tClass.getMethods();

                for (Method method : methods) {
                    method.setAccessible(true);
                    Column column = method.getAnnotation(Column.class);
                    if (column == null) {
                        continue;
                    }
                    list.add(column.name());
                }

            } catch (Exception ex) {
                throw ex;
            }
            return list;
        }

        public static <T extends Object> String getAllAnnotationNameToString(Class<T> tClass) throws Exception {
            String list = "";

            try {
                Method[] methods = tClass.getMethods();

                for (Method method : methods) {
                    method.setAccessible(true);
                    Column column = method.getAnnotation(Column.class);
                    if (column == null) {
                        continue;
                    }
                    list += column.name() + " ";
                }

            } catch (Exception e) {
                throw e;
            }

            return list.trim().replace(" ", ";");
        }
    }

}
