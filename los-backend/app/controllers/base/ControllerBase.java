package controllers.base;

import factory.transaction.TransactionFactory;
import play.mvc.Controller;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Trung on 10/30/2014.
 */
@Named
@Singleton
public abstract class ControllerBase extends Controller {
    public TransactionFactory getTransactionFactory() {
        return transactionFactory;
    }

    @Inject
    public void setTransactionFactory(TransactionFactory transactionFactory) {
        this.transactionFactory = transactionFactory;
    }

    private TransactionFactory transactionFactory;
}