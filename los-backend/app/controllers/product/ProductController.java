package controllers.product;

import blls.product.ProductBll;
import com.fasterxml.jackson.databind.JsonNode;
import controllers.base.ControllerBase;
import models.viewModels.ProductModel;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Result;
import play.mvc.Results;
import transactions.product.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Administrator on 10/29/2014.
 */
@Named
@Singleton
public class ProductController extends ControllerBase {
    private final ProductBll productBll;

    @Inject
    public ProductController(final ProductBll productBll) {
        this.productBll = productBll;
    }

    /**
     * Get product right by user for display and hide button bar
     *
     * @param menuId
     * @param productId
     * @param userId
     * @return
     */
    public Result getProductRight(Long menuId, String productId, String userId) {
        try {
            GetProductRightTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductRightTransaction.class);
            transaction.addParameter("menuId", menuId);
            transaction.addParameter("userId", userId);
            transaction.addParameter("productId", productId);
            transaction.execute();

            return ok(Json.toJson(transaction.getEnquiryData()));
        } catch (Exception e) {
            e.printStackTrace();
            return ok("not really ok");
        }
    }

    /**
     * Get tree by user's right
     *
     * @param menuId
     * @param userId
     * @return
     */
    public Result getProductTreeByUserId(Long menuId, String userId) {
        try {
            GetProductTreeTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductTreeTransaction.class);
            transaction.addParameter("menuId", menuId);
            transaction.addParameter("userId", userId);
            transaction.execute();

            return ok(Json.toJson(transaction.getEnquiryData()));
        } catch (Exception e) {
            e.printStackTrace();
            return ok("not really ok");
        }
//        return ok(Json.toJson(productBll.getProductTreeByUserId(menuId, userId)));
    }

    /**
     * Get list user, group with right of product
     *
     * @param productId
     * @return
     */
    public Result getProductDetailRight(String productId) {
        try {
            GetProductDetailRightTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductDetailRightTransaction.class);
            transaction.addParameter("productId", productId);
            transaction.execute();

            return ok(Json.toJson(transaction.getEnquiryData()));
        } catch (Exception e) {
            e.printStackTrace();
            return ok("not really ok");
        }
    }

    public Result getProductDetail(String productId) {
        try {
            GetProductDetailTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductDetailTransaction.class);
            transaction.addParameter("productId", productId);
            transaction.execute();

            if (transaction.getEnquiryData() != null)
                return ok(Json.toJson(transaction.getEnquiryData()));
            else
                return notFound();
        } catch (Exception e) {
            e.printStackTrace();
            return notFound();
        }
    }

    public Result getProductAudDetail(Long productAudId) {
        try {
            GetProductAudDetailTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductAudDetailTransaction.class);
            transaction.addParameter("productAudId", productAudId);
            transaction.execute();

            if (transaction.getEnquiryData() != null)
                return ok(Json.toJson(transaction.getEnquiryData()));
            else
                return notFound();
        } catch (Exception e) {
            e.printStackTrace();
            return notFound();
        }
    }

    public Result getProductDetailProps(String productId) {
        try {
            GetProductDetailPropsTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductDetailPropsTransaction.class);
            transaction.addParameter("productId", productId);
            transaction.execute();

            if (transaction.getEnquiryData() != null)
                return ok(Json.toJson(transaction.getEnquiryData()));
            else
                return notFound();
        } catch (Exception e) {
            e.printStackTrace();
            return notFound();
        }
    }

    public Result getProductDetailDefaultProps() {
        try {
            GetProductDetailPropsTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductDetailPropsTransaction.class);
            transaction.addParameter("productId", "");
            transaction.execute();

            if (transaction.getEnquiryData() != null)
                return ok(Json.toJson(transaction.getEnquiryData()));
            else
                return notFound();
        } catch (Exception e) {
            e.printStackTrace();
            return notFound();
        }
    }

    public Result getProductAudDetailRight(Long productAudId) {
        try {
            GetProductAudDetailRightTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductAudDetailRightTransaction.class);
            transaction.addParameter("productAudId", productAudId);
            transaction.execute();

            if (transaction.getEnquiryData() != null)
                return ok(Json.toJson(transaction.getEnquiryData()));
            else
                return notFound();
        } catch (Exception e) {
            e.printStackTrace();
            return notFound();
        }
    }

    public Result getProductAudDetailProps(Long productAudId) {
        try {
            GetProductAudDetailPropsTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductAudDetailPropsTransaction.class);
            transaction.addParameter("productAudId", productAudId);
            transaction.execute();

            if (transaction.getEnquiryData() != null)
                return ok(Json.toJson(transaction.getEnquiryData()));
            else
                return notFound();
        } catch (Exception e) {
            e.printStackTrace();
            return notFound();
        }
    }

    public Result getProductAudForApproval() {
        try {
            GetProductAudForApprovalTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductAudForApprovalTransaction.class);
            transaction.execute();

            if (transaction.getEnquiryData() != null)
                return ok(Json.toJson(transaction.getEnquiryData()));
            else
                return noContent();
        } catch (Exception e) {
            e.printStackTrace();
            return Results.noContent();
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result approveProductAud() {
        try {
            JsonNode jsonNode = request().body().asJson();
            Long productAudId = Long.valueOf(jsonNode.findPath("productAudId").asText());
            ApproveProductAudTransaction transaction = getTransactionFactory().getTransactionInstance(ApproveProductAudTransaction.class);

            transaction.addParameter("productAudId", productAudId);
            transaction.execute();

            return ok("Approval is ok!");
        } catch (Exception e) {
            e.printStackTrace();
            return Results.ok("Can not approve!");
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result addProduct() {
        try {
            JsonNode json = request().body().asJson();
            ProductModel model = Json.fromJson(json, ProductModel.class);

            AddProductAudTransaction transaction = getTransactionFactory().getTransactionInstance(AddProductAudTransaction.class);
            transaction.addParameter("productModel", model);
            transaction.execute();

            return ok();
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
    }

    public Result deleteProduct(String productId) {
        try {
            DeleteProductTransaction transaction = getTransactionFactory().getTransactionInstance(DeleteProductTransaction.class);
            transaction.addParameter("productId", productId);

            transaction.execute();
            return ok();
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
    }

    public Result getProductListForAddParent() {
        try {
            GetProductListForAddParentTransaction transaction = getTransactionFactory().getTransactionInstance(GetProductListForAddParentTransaction.class);

            transaction.execute();

            return ok(Json.toJson(transaction.getEnquiryData()));
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }
    }

    public Result test() throws Exception {
        return ok(Json.toJson(productBll.useCopyObject()));
    }
}