package controllers.sysUser;

import blls.sysUser.SysUserBll;
import controllers.base.ControllerBase;
import play.libs.Json;
import play.mvc.Result;
import transactions.sysUser.GetACLByUserIdAndPIDTransaction;
import transactions.sysUser.GetUserListForAddRightTransaction;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.inject.Named;

import static play.mvc.Results.ok;

/**
 * Created by Administrator on 10/29/2014.
 */
@Named
@Singleton
public class SysUserController extends ControllerBase {

    public Result getACLByUserIdAndPID(String userId, Long pid) {
        try {
            GetACLByUserIdAndPIDTransaction transaction = getTransactionFactory().getTransactionInstance(GetACLByUserIdAndPIDTransaction.class);
            transaction.addParameter("userId", userId);
            transaction.addParameter("pid", pid);
            transaction.execute();

            return ok(Json.toJson(transaction.getEnquiryData()));
        } catch (Exception e) {
            e.printStackTrace();
            return ok("not really ok");
        }
    }

    public Result getUserListForAddRight() {
        try {
            GetUserListForAddRightTransaction transaction = getTransactionFactory().getTransactionInstance(GetUserListForAddRightTransaction.class);
            transaction.execute();

            return ok(Json.toJson(transaction.getEnquiryData()));
        } catch (Exception e) {
            e.printStackTrace();
            return notFound();
        }
    }
}
