package controllers.sysGroup;

import controllers.base.ControllerBase;
import play.libs.Json;
import play.mvc.Result;
import transactions.sysGroup.GetGroupListForAddRightTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Administrator on 11/14/2014.
 */
@Named
@Singleton
public class SysGroupController extends ControllerBase {
    public Result getGroupListForAddRight() {
        try {
            GetGroupListForAddRightTransaction transaction = getTransactionFactory().getTransactionInstance(GetGroupListForAddRightTransaction.class);
            transaction.execute();

            return ok(Json.toJson(transaction.getEnquiryData()));
        } catch (Exception e) {
            e.printStackTrace();
            return notFound();
        }
    }
}
