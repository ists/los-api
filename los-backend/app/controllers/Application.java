package controllers;

import controllers.base.ControllerBase;
import models.entities.SysTransaction;
import play.mvc.*;
import repositories.SysTransactionRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 * The main set of web services.
 */
@Named
@Singleton
public class Application extends ControllerBase {
    @Inject
    private SysTransactionRepository sysTransactionRepository;

    public Result index() {
        return ok(views.html.index.render("Welcome!"));
    }

    public Result test() {
        SysTransaction sysTransaction = new SysTransaction();
        sysTransaction.userId = "demo.approve";
        sysTransaction.errCode = "Test";

        sysTransactionRepository.save(sysTransaction);

        return ok();
    }
}
