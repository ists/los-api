package transactions.product;

import blls.product.ProductBll;
import models.viewModels.ProductModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Administrator on 11/10/2014.
 */
@Named
@Singleton
public class GetProductDetailTransaction extends EnquiryTransaction<ProductModel> {
    @Override
    public void processTransaction() throws Exception {
        String productId = String.valueOf(getParameter("productId"));

        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductDetail(productId));
    }
}
