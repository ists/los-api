package transactions.product;

import blls.product.ProductBll;
import models.viewModels.ProductAudModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Administrator on 11/12/2014.
 */
@Named
@Singleton
public class GetProductAudDetailTransaction extends EnquiryTransaction<ProductAudModel> {
    @Override
    protected void processTransaction() throws Exception {
        Long productAudId = Long.valueOf(String.valueOf(getParameter("productAudId")));

        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductAudDetail(productAudId));
    }
}
