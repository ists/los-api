package transactions.product;

import blls.product.ProductBll;
import transactions.type.DeleteTransaction;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Trung on 11/12/2014.
 */
@Named
@Singleton
public class DeleteProductTransaction extends DeleteTransaction {
    @Override
    protected void processTransaction() throws Exception {
        String productId = getParameter("productId").toString();
        getBusinessLayerFactory().getBllInstance(ProductBll.class).deleteProduct(productId);
    }
}