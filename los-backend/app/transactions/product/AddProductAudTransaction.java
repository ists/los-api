package transactions.product;

import blls.product.ProductBll;
import models.viewModels.ProductModel;
import transactions.type.InputTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Trung on 11/11/2014.
 */
@Named
@Singleton
public class AddProductAudTransaction extends InputTransaction {
    @Override
    protected void processTransaction() throws Exception {
            ProductModel productModel = (ProductModel) getParameter("productModel");
            getBusinessLayerFactory().getBllInstance(ProductBll.class).addProductAud(productModel);
    }
}
