package transactions.product;

import blls.product.ProductBll;
import transactions.type.ApproveTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Administrator on 11/11/2014.
 */
@Named
@Singleton
public class ApproveProductAudTransaction extends ApproveTransaction {

    @Override
    protected void processTransaction() throws Exception {
        Long productAudId = (Long) getParameter("productAudId");

        getBusinessLayerFactory().getBllInstance(ProductBll.class).approveProductAud(productAudId);
    }
}
