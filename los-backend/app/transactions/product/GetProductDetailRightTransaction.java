package transactions.product;

import blls.product.ProductBll;
import models.viewModels.commonViewModels.RightDetailModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Administrator on 11/10/2014.
 */
@Named
@Singleton
public class GetProductDetailRightTransaction extends EnquiryTransaction<List<RightDetailModel>> {
    @Override
    public void processTransaction() {
        String productId = String.valueOf(getParameter("productId"));

        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductDetailRight(productId));
    }
}