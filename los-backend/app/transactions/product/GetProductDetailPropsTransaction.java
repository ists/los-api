package transactions.product;

import blls.product.ProductBll;
import models.viewModels.ProductPropModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Administrator on 11/11/2014.
 */
@Named
@Singleton
public class GetProductDetailPropsTransaction extends EnquiryTransaction<List<ProductPropModel>> {
    @Override
    public void processTransaction() throws Exception {
        String productId = String.valueOf(getParameter("productId"));

        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductDetailProps(productId));
    }
}
