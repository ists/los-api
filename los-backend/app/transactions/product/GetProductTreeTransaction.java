package transactions.product;

import blls.product.ProductBll;
import models.viewModels.commonViewModels.TreeModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Trung on 11/3/2014.
 */
@Named
@Singleton
public class GetProductTreeTransaction extends EnquiryTransaction<List<TreeModel>> {
    @Override
    public void processTransaction() throws Exception{
        Long menuId = Long.valueOf(String.valueOf(getParameter("menuId")));
        String userId = String.valueOf(getParameter("userId"));

        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductTreeByUserId(menuId, userId));
    }
}