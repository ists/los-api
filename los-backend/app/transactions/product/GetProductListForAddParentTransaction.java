package transactions.product;

import blls.product.ProductBll;
import models.viewModels.ProductModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Administrator on 11/14/2014.
 */
@Named
@Singleton
public class GetProductListForAddParentTransaction extends EnquiryTransaction<List<ProductModel>> {
    @Override
    protected void processTransaction() throws Exception {
        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductListForAddParent());
    }
}
