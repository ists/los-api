package transactions.product;

import models.entities.Product;
import transactions.base.EnquiryTransactionBase;
import transactions.base.TransactionBase;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Trung on 11/3/2014.
 */
@Named
@Singleton
public class GetProductEntityTransaction extends EnquiryTransaction<Product> {
    @Override
    public void processTransaction() {

    }
}
