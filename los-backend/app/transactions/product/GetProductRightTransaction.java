package transactions.product;

import blls.product.ProductBll;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Administrator on 11/10/2014.
 */
@Named
@Singleton
public class GetProductRightTransaction extends EnquiryTransaction<String> {
    @Override
    public void processTransaction() throws Exception{
        Long menuId = Long.valueOf(String.valueOf(getParameter("menuId")));
        String userId = String.valueOf(getParameter("userId"));
        String productId = String.valueOf(getParameter("productId"));

        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductRight(menuId, productId, userId));
    }
}
