package transactions.product;

import blls.product.ProductBll;
import models.viewModels.SysRightAudModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Administrator on 11/12/2014.
 */
@Named
@Singleton
public class GetProductAudDetailRightTransaction extends EnquiryTransaction<List<SysRightAudModel>> {
    @Override
    protected void processTransaction() throws Exception {
        Long productAudId = Long.valueOf((String) getParameter("productAudId"));

        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductAudDetailRight(productAudId));
    }
}
