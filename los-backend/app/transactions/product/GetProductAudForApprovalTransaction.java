package transactions.product;

import blls.product.ProductBll;
import models.viewModels.ProductAudModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Administrator on 11/11/2014.
 */
@Named
@Singleton
public class GetProductAudForApprovalTransaction extends EnquiryTransaction<List<ProductAudModel>> {
    @Override
    public void processTransaction() throws Exception {
        setEnquiryData(getBusinessLayerFactory().getBllInstance(ProductBll.class).getProductAudForApproval());
    }
}
