package transactions.base;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Trung on 10/29/2014.
 */
@Named
@Singleton
public abstract class NonEnquiryTransactionBase extends TransactionBase {

}
