package transactions.base;

import blls.transaction.TransactionBll;
import factory.business.BusinessLayerFactory;
import models.entities.SysTransaction;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trung on 10/28/2014.
 */
@Named
@Singleton
public abstract class TransactionBase {
    @Inject
    private BusinessLayerFactory businessLayerFactory;
    private List<TransactionParameter> parameters;
    private SysTransaction transaction;

    public SysTransaction getTransaction() {
        if (transaction == null)
            transaction = getBusinessLayerFactory().getBllInstance(TransactionBll.class).addTransaction();

        return transaction;
    }

    public BusinessLayerFactory getBusinessLayerFactory() {
        return businessLayerFactory;
    }

    public void implementTransactionManagement() {
        businessLayerFactory.setCurrentTransaction(getTransaction());
        setTransactionProperty();
    }

    protected abstract void setTransactionProperty();

    protected abstract void processTransaction() throws Exception;

    @Transactional(rollbackFor = Exception.class)
    public final void execute() throws Exception {
        implementTransactionManagement();
        processTransaction();
        concludeTransaction();
//        throw new Exception();
    }

    protected void concludeTransaction() {
        getBusinessLayerFactory().getBllInstance(TransactionBll.class).commitTransaction(getTransaction());
    }

    private void rollbackTransaction() {
        System.out.println("exception throwed");
        getTransaction().setComments("exception throwed");
        getBusinessLayerFactory().getBllInstance(TransactionBll.class).rollbackTransaction(getTransaction());
    }

    public List<TransactionParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<TransactionParameter> parameters) {
        this.parameters = parameters;
    }

    public Object getParameter(String name) {

        for (TransactionParameter param : parameters) {
            if (param.getName().equals(name))
                return param.getValue();
        }

        return null;
//        throw new NullPointerException(new StringBuilder("Khong tim thay parameter voi ten: ").append(name).toString());
    }

    public void clearParameters() {
        if (this.parameters != null)
            this.parameters.clear();
    }

    public void addParameter(TransactionParameter parameter) {
        this.parameters.add(parameter);
    }

    public void addParameter(String name, Object value) {
        TransactionParameter parameter = new TransactionParameter();
        parameter.setName(name);
        parameter.setValue(value);

        if (parameters == null)
            parameters = new ArrayList<TransactionParameter>();

        parameters.add(parameter);
    }
}