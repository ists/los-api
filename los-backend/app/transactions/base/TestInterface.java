package transactions.base;

/**
 * Created by Trung on 11/11/2014.
 */
public interface TestInterface<T> {
    public T getEnquiryData();

    public void setEnquiryData(T data);
}
