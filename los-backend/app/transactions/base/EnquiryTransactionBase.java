package transactions.base;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * Created by Trung on 10/29/2014.
 */
@Named
@Singleton
public abstract class EnquiryTransactionBase<T> extends TransactionBase {
    private T data;

    public T getEnquiryData() {
        return data;
    }

    public void setEnquiryData(T data) {
        this.data = data;
    }
}