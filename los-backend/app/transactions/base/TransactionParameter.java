package transactions.base;

/**
 * Created by Trung on 10/29/2014.
 */
public class TransactionParameter {
    private String Name;
    private Object Value;

    public Object getValue() {
        return Value;
    }

    public void setValue(Object value) {
        Value = value;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}