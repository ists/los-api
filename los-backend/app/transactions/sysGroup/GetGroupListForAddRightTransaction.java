package transactions.sysGroup;

import blls.SysGroup.SysGroupBll;
import blls.sysUser.SysUserBll;
import models.viewModels.SysGroupModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Administrator on 11/14/2014.
 */
@Named
@Singleton
public class GetGroupListForAddRightTransaction extends EnquiryTransaction<List<SysGroupModel>> {
    @Override
    protected void processTransaction() throws Exception {
        setEnquiryData(getBusinessLayerFactory().getBllInstance(SysGroupBll.class).getAllGroup());
    }
}
