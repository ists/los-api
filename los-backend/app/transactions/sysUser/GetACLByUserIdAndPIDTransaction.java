package transactions.sysUser;

import blls.sysUser.SysUserBll;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Administrator on 11/5/2014.
 */
@Named
@Singleton
public class GetACLByUserIdAndPIDTransaction extends EnquiryTransaction<String> {
    @Override
    public void processTransaction() {
        String userId = String.valueOf(String.valueOf(getParameter("userId")));
        Long pid = Long.valueOf(String.valueOf(getParameter("pid")));

        setEnquiryData(getBusinessLayerFactory().getBllInstance(SysUserBll.class).getACLByUserIdAndPID(userId, pid));
    }
}
