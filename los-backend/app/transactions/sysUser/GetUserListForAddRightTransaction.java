package transactions.sysUser;

import blls.sysUser.SysUserBll;
import models.viewModels.SysUserModel;
import transactions.type.EnquiryTransaction;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by Administrator on 11/14/2014.
 */
@Named
@Singleton
public class GetUserListForAddRightTransaction extends EnquiryTransaction<List<SysUserModel>> {
    @Override
    protected void processTransaction() throws Exception {
        setEnquiryData(getBusinessLayerFactory().getBllInstance(SysUserBll.class).getUserListForAddRight());
    }
}
