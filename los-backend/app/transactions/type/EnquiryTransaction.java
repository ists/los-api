package transactions.type;

import blls.transaction.TransactionBll;
import transactions.base.EnquiryTransactionBase;
import utilities.EnumUtility;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Trung on 10/30/2014.
 */
@Named
@Singleton
public abstract class EnquiryTransaction<T> extends EnquiryTransactionBase<T> {
    public void reset () {
        this.setEnquiryData(null);
    }

    @Override
    protected void setTransactionProperty() {
        reset();
        getTransaction().setComments("inquiry data");
        getTransaction().setTranAction(EnumUtility.TRAN_ACTION.INQUIRY.toString());
        getTransaction().setTranStatus(EnumUtility.TRAN_STATUS.ACTIVE.toString());
    }

    @Override
    protected void concludeTransaction() {
        getBusinessLayerFactory().getBllInstance(TransactionBll.class).closeTransaction(getTransaction());
    }
}