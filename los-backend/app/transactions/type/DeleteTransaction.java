package transactions.type;

import transactions.base.NonEnquiryTransactionBase;
import utilities.EnumUtility;

import javax.inject.Named;
import javax.inject.Singleton;

/**
 * Created by Trung on 11/12/2014.
 */
@Named
@Singleton
public abstract class DeleteTransaction extends NonEnquiryTransactionBase {
    @Override
    protected void setTransactionProperty() {
        getTransaction().setComments("delete data");
        getTransaction().setTranAction(EnumUtility.TRAN_ACTION.INPUT.toString());
        getTransaction().setTranStatus(EnumUtility.TRAN_STATUS.CLOSE.toString());
    }
}
