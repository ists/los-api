package transactions.type;

import blls.transaction.TransactionBll;
import javassist.NotFoundException;
import transactions.base.EnquiryTransactionBase;
import transactions.base.NonEnquiryTransactionBase;
import utilities.EnumUtility;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.Serializable;

/**
 * Created by Trung on 10/30/2014.
 */
@Named
@Singleton
public abstract class ApproveTransaction extends NonEnquiryTransactionBase {
    @Override
    protected void setTransactionProperty() {
        getTransaction().setComments("input data");
        getTransaction().setTranAction(EnumUtility.TRAN_ACTION.APPROVE.toString());
        getTransaction().setTranStatus(EnumUtility.TRAN_STATUS.CLOSE.toString());
    }

    @Override
    protected void concludeTransaction() {
        try {
            getBusinessLayerFactory().getBllInstance(TransactionBll.class).approveTransaction(getTransaction());
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }
}