package factory.business;

import blls.base.BllBase;
import models.entities.SysTransaction;
import utilities.FactoryContext;

import javax.inject.Named;
import javax.inject.Singleton;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

@Named
@Singleton
public class BusinessLayerFactory {
    public static Set<Class<? extends BllBase>> businessClasses;
    private SysTransaction currentTransaction;

//    public <T> T getBllInstance(Class<T> classType) {
//        try {
//            for (Class<? extends BllBase> business : businessClasses) {
//                if (business.equals(classType)) {
//                    T clazz = (T)business.newInstance();
//                    Method method = clazz.getClass().getMethod("setCurrentTransaction", SysTransaction.class);
//                    method.invoke(clazz, currentTransaction);
//                    //this.productBll.setCurrentTransaction(currentTransaction);
//                    return clazz;
//                }
//            }
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }

    public <A> A getBllInstance(Class<A> aClass) {
        try {
            A clazz = FactoryContext.ctx.getBean(aClass);
            Method method = null;
            method = clazz.getClass().getMethod("setCurrentTransaction", SysTransaction.class);
            if (method != null)
                method.invoke(clazz, currentTransaction);
        return clazz;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setCurrentTransaction(SysTransaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }
}