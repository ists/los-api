package factory.transaction;

import models.entities.SysTransaction;
import transactions.base.TransactionBase;
import utilities.FactoryContext;

import javax.inject.Named;
import javax.inject.Singleton;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

/**
 * Created by Trung on 10/30/2014.
 */
@Named
@Singleton
public class TransactionFactory {
    public static <A> A getTransactionInstance(Class<A> aClass) {
        try {
            A clazz = FactoryContext.ctx.getBean(aClass);
            Method method = null;
            method = clazz.getClass().getMethod("clearParameters");
            if (method != null)
                method.invoke(clazz);
            return clazz;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }
//    public static Set<Class<? extends TransactionBase>> transactionClasses;

//    public <T> T getTransactionInstance(Class<T> classType) {
//        try {
//            for (Class<? extends TransactionBase> transcation : transactionClasses) {
//                if (transcation.equals(classType))
//                    return (T) transcation.newInstance();
//            }
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
}