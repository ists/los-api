package factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.Repository;
import utilities.FactoryContext;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Set;

/**
 * Created by Trung on 10/30/2014.
 */
@Named
@Singleton
public class RepositoryFactory {
    public <A> A getRepositoryInstance(Class<A> aClass) {
        return FactoryContext.ctx.getBean(aClass);
    }
//    public static Set<Class<? extends JpaRepository>> repositoryClasses;
//
//    public <T> T getRepositoryInstance(Class<T> classType) {
//        try {
//            for (Class<? extends JpaRepository> repository : repositoryClasses) {
//                if (repository.equals(classType))
//                    return (T) repository.newInstance();
//            }
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
}
