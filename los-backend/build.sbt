name := """los-backend"""

version := "1.0-SNAPSHOT"

//playJavaSettings

lazy val root = (project in file(".")).enablePlugins(PlayJava)

libraryDependencies ++= Seq(
    javaCore,
    javaJdbc,
    javaJpa,
    "org.springframework" % "spring-context" % "4.1.1.RELEASE",
    "javax.inject" % "javax.inject" % "1",
    "org.springframework.data" % "spring-data-jpa" % "1.7.0.RELEASE",
    "org.springframework" % "spring-expression" % "4.1.1.RELEASE",
    "org.hibernate" % "hibernate-entitymanager" % "4.2.8.Final",
    //"org.hibernate" % "hibernate-entitymanager" % "4.3.6.Final",  //Comment: because play 2.3.5's JPA plugin not work with Hibernate 4.3
    //"com.oracle" % "ojdbc7" % "12.1.0.1"
    "org.mockito" % "mockito-core" % "1.9.5" % "test"
)
